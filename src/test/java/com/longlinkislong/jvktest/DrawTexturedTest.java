package com.longlinkislong.jvktest;

import com.longlinkislong.jvk.*;
import org.junit.Test;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVulkan;
import org.lwjgl.system.MemoryUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import static com.longlinkislong.jvk.Util.alignUp;


public class DrawTexturedTest {
    private static final class DrawData {
        private final Semaphore drawCompleteSemaphore;
        private final Fence drawCompleteFence;

        private DrawData(final Device device) {
            this.drawCompleteFence = device.acquireFence();
            this.drawCompleteSemaphore = device.acquireSemaphore();
        }
    }

    private static final boolean API_DEBUG = false;

    @Test
    public void run() {
        if (API_DEBUG) {
            Instance.enableLayer("VK_LAYER_LUNARG_api_dump");
        }

        Instance.enableLayer("VK_LAYER_LUNARG_standard_validation");
        Instance.enableExtension("VK_EXT_debug_report");

        GLFW.glfwInit();

        BufferStreams.of(GLFWVulkan.glfwGetRequiredInstanceExtensions())
                .mapToObj(MemoryUtil::memUTF8Safe)
                .forEach(Instance::enableExtension);

        final var instance = Instance.get();
        final var device = instance.physicalDevices.stream()
                .max(PhysicalDevice.preferDiscreteGPU())
                .orElseThrow(() -> new UnsupportedOperationException("Vulkan is not supported!"))
                .createDevice("VK_KHR_swapchain");

        GLFW.glfwDefaultWindowHints();
        GLFW.glfwWindowHint(GLFW.GLFW_CLIENT_API, GLFW.GLFW_NO_API);

        final var pWindow = GLFW.glfwCreateWindow(640, 480, "GLFW Test", 0L, 0L);
        final var surface = instance.createWindowSurface(pWindow);

        final var queueFamily = device.getQueueFamily(0);

        final var swapchain = device.createSwapchain(new Swapchain.CreateInfo()
                .withSize(640, 480)
                .withSurface(surface)
                .withQueueFamily(queueFamily));

        final var image = device.createImage(new Image.CreateInfo()
                .withUsage(ImageUsageFlag.COLOR_ATTACHMENT, ImageUsageFlag.TRANSFER_SRC)
                .withExtent(640, 480)
                .withImageType(ImageType.IMAGE_2D)
                .withFormat(Format.B8G8R8A8_UNORM), MemoryProperty.DEVICE_LOCAL);

        final var imageView = new ImageView(image);

        final var renderPass = device.createRenderPass(new RenderPass.CreateInfo()
                .withAttachments(new AttachmentDescription()
                        .withFormat(image.info.format)
                        .withInitialLayout(ImageLayout.UNDEFINED)
                        .withFinalLayout(ImageLayout.COLOR_ATTACHMENT_OPTIMAL))
                .withSubpasses(new SubpassDescription()
                        .withColorAttachments(new AttachmentReference(0, ImageLayout.COLOR_ATTACHMENT_OPTIMAL)))
                .withDependencies(
                        new SubpassDependency()
                                .withSrcSubpass(SubpassDependency.SUBPASS_EXTERNAL)
                                .withDstSubpass(0)
                                .withSrcStageMask(PipelineStageFlag.BOTTOM_OF_PIPE)
                                .withDstStageMask(PipelineStageFlag.COLOR_ATTACHMENT_OUTPUT_BIT)
                                .withDstAccessMask(AccessFlag.COLOR_ATTACHMENT_READ, AccessFlag.COLOR_ATTACHMENT_WRITE)));

        final var framebuffer = new Framebuffer(renderPass, new Framebuffer.CreateInfo(640, 480, 1), imageView);
        final var renderPassBeginInfo = framebuffer.getRenderPassBeginInfo()
                .withClearValues(new ClearValue(new Color(0.0F, 0.0F, 0.2F, 1.0F)));

        final var pipelineLayoutInfo = new PipelineLayout.CreateInfo()
                .withSetLayoutInfos(new DescriptorSetLayout.CreateInfo()
                        .withBindings(
                                new DescriptorSetLayoutBinding()
                                        .withBinding(0)
                                        .withStages(ShaderStage.VERTEX)
                                        .withDescriptorCount(1)
                                        .withDescriptorType(DescriptorType.UNIFORM_BUFFER),
                                new DescriptorSetLayoutBinding()
                                        .withBinding(1)
                                        .withStages(ShaderStage.FRAGMENT)
                                        .withDescriptorCount(1)
                                        .withDescriptorType(DescriptorType.COMBINED_IMAGE_SAMPLER)));

        final var pipeline = device.allocatePipeline(new GraphicsPipeline.CreateInfo()
                .withLayout(pipelineLayoutInfo)
                .withStages(
                        new PipelineShaderStageCreateInfo()
                                .withStage(ShaderStage.VERTEX)
                                .withModuleInfo(new ShaderModule.CreateInfo(0, Paths.get("data", "shaders", "tris2D_textured.vert.spv"))),
                        new PipelineShaderStageCreateInfo()
                                .withStage(ShaderStage.FRAGMENT)
                                .withModuleInfo(new ShaderModule.CreateInfo(0, Paths.get("data", "shaders", "tris2D_textured.frag.spv"))))
                .withVertexInputState(new PipelineVertexInputStateCreateInfo()
                        .withVertexAttributeDescriptions(
                                new VertexInputAttributeDescription(0, 0, Format.R32G32_SFLOAT, 0),
                                new VertexInputAttributeDescription(1, 0, Format.R32G32_SFLOAT, 8))
                        .withVertexBindingDescriptions(
                                new VertexInputBindingDescription(0, 16, VertexInputRate.PER_VERTEX)
                        ))
                .withInputAssemblyState(new PipelineInputAssemblyStateCreateInfo(0, PrimitiveTopology.TRIANGLE_LIST, false))
                .withColorBlendState(new PipelineColorBlendStateCreateInfo()
                        .withAttachments(new PipelineColorBlendAttachmentState()))
                .withRasterizationState(new PipelineRasterizationStateCreateInfo())
                .withMultisampleState(new PipelineMultisampleStateCreateInfo())
                .withDepthStencilState(new PipelineDepthStencilStateCreateInfo())
                .withSubpass(0), renderPass);

        final var constants = device.createBuffer(
                new Buffer.CreateInfo()
                        .withUsage(BufferUsageFlag.UNIFORM_BUFFER)
                        .withSize(16 * Float.BYTES),
                MemoryProperty.HOST_COHERENT, MemoryProperty.HOST_VISIBLE);

        constants.withMap(data -> {
            data.putFloat(1.0F).putFloat(0.0F).putFloat(0.0F).putFloat(0.0F);
            data.putFloat(0.0F).putFloat(1.0F).putFloat(0.0F).putFloat(0.0F);
            data.putFloat(0.0F).putFloat(0.0F).putFloat(1.0F).putFloat(0.0F);
            data.putFloat(0.0F).putFloat(0.0F).putFloat(0.0F).putFloat(1.0F);
        });

        final var vertices = device.createBuffer(
                new Buffer.CreateInfo()
                        .withUsage(BufferUsageFlag.VERTEX_BUFFER)
                        .withSize(3 * (2 * Float.BYTES + 2 * Float.BYTES)),
                MemoryProperty.HOST_COHERENT, MemoryProperty.HOST_VISIBLE);

        vertices.withMap(data -> {
            data.putFloat(-0.5F).putFloat(-0.5F).putFloat(0.0F).putFloat(0.0F);
            data.putFloat(0.5F).putFloat(-0.5F).putFloat(1.0F).putFloat(0.0F);
            data.putFloat(0.0F).putFloat(0.5F).putFloat(0.5F).putFloat(1.0F);
        });

        final var indices = device.createBuffer(
                new Buffer.CreateInfo()
                        .withUsage(BufferUsageFlag.INDEX_BUFFER)
                        .withSize(3 * Short.BYTES),
                MemoryProperty.HOST_COHERENT, MemoryProperty.HOST_VISIBLE);

        indices.withMap(data -> data.putShort((short) 0).putShort((short) 1).putShort((short) 2) );

        final BufferedImage bImg;
        try (var in = Files.newInputStream(Paths.get("data", "images", "environment.png"))) {
            bImg = ImageIO.read(in);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        final var tx = device.createBuffer(
                new Buffer.CreateInfo()
                        .withUsage(BufferUsageFlag.TRANSFER_SRC)
                        .withSize(alignUp(bImg.getWidth() * bImg.getHeight() * 4, 1024)),
                MemoryProperty.HOST_COHERENT, MemoryProperty.HOST_VISIBLE);

        tx.withMap(data -> {
            final var pixels = new int[bImg.getWidth() * bImg.getHeight()];

            bImg.getRGB(0, 0, bImg.getWidth(), bImg.getHeight(), pixels, 0, bImg.getWidth());

            data.asIntBuffer().put(pixels);
        });

        final var textureImage = device.createImage(new Image.CreateInfo()
                .withExtent(bImg.getWidth(), bImg.getHeight())
                .withUsage(ImageUsageFlag.TRANSFER_DST, ImageUsageFlag.SAMPLED)
                .withFormat(Format.B8G8R8A8_UNORM)
                .withImageType(ImageType.IMAGE_2D), MemoryProperty.DEVICE_LOCAL);

        final var uploadCommand = queueFamily.getCurrentCommandPool().allocate(CommandBufferLevel.PRIMARY)
                .begin(Set.of(CommandBufferUsageFlag.ONE_TIME_SUBMIT))
                .transferImage(textureImage, ImageLayout.UNDEFINED, ImageLayout.TRANSFER_DST_OPTIMAL, Set.of(PipelineStageFlag.TOP_OF_PIPE), Set.of(PipelineStageFlag.TRANSFER),  Set.of(), Set.of(AccessFlag.TRANSFER_WRITE), null, null)
                .copyBufferToImage(tx, textureImage, ImageLayout.TRANSFER_DST_OPTIMAL, 0)
                .transferImage(textureImage, ImageLayout.TRANSFER_DST_OPTIMAL, ImageLayout.SHADER_READ_ONLY, Set.of(PipelineStageFlag.TRANSFER), Set.of(PipelineStageFlag.BOTTOM_OF_PIPE), Set.of(AccessFlag.TRANSFER_WRITE), Set.of(), null, null)
                .end();

        final var queue = queueFamily.getQueue(0);

        queue.submit(List.of(uploadCommand));
        queue.waitIdle();

        uploadCommand.free();
        tx.free();

        final var textureView = new ImageView(textureImage);
        final var textureSampler = device.createSampler(new Sampler.CreateInfo());

        final var pipelineLayout = pipeline.getPipelineLayout();

        final var descriptorSet = pipelineLayout.getDescriptorSetLayout(0).allocate()
                .writeBuffer(DescriptorType.UNIFORM_BUFFER, 0, constants)
                .writeImage(DescriptorType.COMBINED_IMAGE_SAMPLER, 1, textureSampler, ImageLayout.SHADER_READ_ONLY, textureView);

        final var drawCommand = queueFamily.getCurrentCommandPool().allocate(CommandBufferLevel.PRIMARY)
                .begin(Set.of(CommandBufferUsageFlag.SIMULTANEOUS_USE))
                .beginRenderPass(renderPassBeginInfo)
                .setScissor(0, 0, 640, 480)
                .setViewport(0, 0, 640, 480)
                .bindPipeline(pipeline)
                .bindDescriptorSet(pipeline.getBindpoint(), pipelineLayout, 0, descriptorSet)
                .bindVertexBuffer(0, vertices, 0L)
                .bindIndexBuffer(indices, 0L, IndexType.UINT16)
                .drawIndexed(3, 1, 0, 0, 0)
                .endRenderPass()
                .end();

        final var drawInfos = new ArrayDeque<DrawData>();
        final Supplier<DrawData> getDrawData = () -> {
            if (!drawInfos.isEmpty()) {
                final var info = drawInfos.peek();

                if (info.drawCompleteFence.isSignaled()) {
                    info.drawCompleteFence.resetFence();

                    return drawInfos.poll();
                }
            }

            return new DrawData(device);
        };

        long time = System.currentTimeMillis();
        int frames = 0;

        try {
            while (!GLFW.glfwWindowShouldClose(pWindow)) {
                final var drawData = getDrawData.get();

                queue.submit(
                        new Queue.SubmitInfo()
                                .withSignalSemaphores(drawData.drawCompleteSemaphore)
                                .withCommandBuffers(drawCommand),
                        drawData.drawCompleteFence);

                queue.present(new Queue.PresentInfo()
                        .withImage(image)
                        .withImageAccess(AccessFlag.COLOR_ATTACHMENT_READ, AccessFlag.COLOR_ATTACHMENT_WRITE)
                        .withImageLayout(ImageLayout.COLOR_ATTACHMENT_OPTIMAL)
                        .withSrcStageMask(PipelineStageFlag.COLOR_ATTACHMENT_OUTPUT_BIT)
                        .withSwapchain(swapchain)
                        .withWaitSemaphores(drawData.drawCompleteSemaphore));

                GLFW.glfwPollEvents();
                drawInfos.offer(drawData);

                final long now = System.currentTimeMillis();
                final double dTime = (now - time) * 0.001;

                frames++;

                if (dTime > 5.0) {
                    System.out.printf("FPS: %.2f%n", (frames / dTime));
                    time = now;
                    frames = 0;
                }

                if (API_DEBUG) {
                    GLFW.glfwSetWindowShouldClose(pWindow, true);
                }
            }
        } catch (InterruptedException ex) {
            System.err.println("Display loop interrupted!");
        }

        device.waitIdle();

        queueFamily.detach();

        textureView.free();
        textureImage.free();
        textureSampler.release();

        vertices.free();
        indices.free();
        constants.free();

        pipeline.release();

        framebuffer.free();
        imageView.free();
        image.free();

        renderPass.free();

        GLFW.glfwDestroyWindow(pWindow);
        GLFW.glfwTerminate();

        swapchain.free();

        device.free();
    }
}
