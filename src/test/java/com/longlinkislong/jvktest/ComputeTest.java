package com.longlinkislong.jvktest;

import com.longlinkislong.jvk.*;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ComputeTest {
    private static final int INPUT_SIZE = 32;
    private static final int WORKGROUP_SIZE = 32;
    private static final int WORK_GROUP_COUNT = Math.max(1, INPUT_SIZE / WORKGROUP_SIZE);

    private static boolean API_DEBUG = false;

    @Test
    public void runTest() {
        if (API_DEBUG) {
            Instance.enableLayer("VK_LAYER_LUNARG_api_dump");
        }

        Instance.enableLayer("VK_LAYER_LUNARG_standard_validation");
        Instance.enableExtension("VK_EXT_debug_report");

        final var instance = Instance.get();
        final var device = instance.physicalDevices.stream()
                .max(PhysicalDevice.preferDiscreteGPU())
                .orElseThrow(() -> new RuntimeException("Vulkan is not supported!"))
                .createDevice(Set.of());

        final var inputBuffer = device.createBuffer(
                new Buffer.CreateInfo()
                        .withUsage(BufferUsageFlag.STORAGE_BUFFER)
                        .withSize(INPUT_SIZE * Float.BYTES),
                MemoryProperty.HOST_VISIBLE, MemoryProperty.HOST_COHERENT);

        final var inData = inputBuffer.map();

        System.out.println("Input:");
        for (int i = 0; i < INPUT_SIZE; i++) {
            inData.putFloat(i);
            System.out.printf("%.2f, ", (float) i);
        }
        inData.flip();

        System.out.println();

        inputBuffer.unmap();

        final var outputBuffer = device.createBuffer(
                new Buffer.CreateInfo()
                        .withUsage(BufferUsageFlag.STORAGE_BUFFER)
                        .withSize(INPUT_SIZE * Float.BYTES),
                MemoryProperty.HOST_VISIBLE, MemoryProperty.HOST_COHERENT);

        final var descriptorSetLayoutBindings = new ArrayList<DescriptorSetLayoutBinding>();
        {
            final var binding = new DescriptorSetLayoutBinding()
                    .withBinding(0)
                    .withDescriptorType(DescriptorType.STORAGE_BUFFER)
                    .withDescriptorCount(1)
                    .withStages(ShaderStage.COMPUTE);

            descriptorSetLayoutBindings.add(binding);
            descriptorSetLayoutBindings.add(binding.withBinding(1));
        }

        final var pipeline = device.allocatePipeline(new ComputePipeline.CreateInfo()
                .withStage(new PipelineShaderStageCreateInfo()
                        .withStage(ShaderStage.COMPUTE)
                        .withModuleInfo(new ShaderModule.CreateInfo()
                                .withPath(Paths.get("data", "shaders", "square.comp.spv"))))
                .withLayoutInfo(new PipelineLayout.CreateInfo()
                        .withSetLayoutInfos(new DescriptorSetLayout.CreateInfo()
                                .withBindings(descriptorSetLayoutBindings))));

        final var descriptorSet = pipeline.getPipelineLayout().getDescriptorSetLayout(0)
                .allocate()
                .writeBuffer(DescriptorType.STORAGE_BUFFER, 0, inputBuffer)
                .writeBuffer(DescriptorType.STORAGE_BUFFER, 1, outputBuffer);

        final var queueFamily = device.queueFamilies.stream()
                .filter(qf -> qf.flags.contains(QueueFlag.COMPUTE))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Compute is not supported!"));

        final var commandBuffer = queueFamily.getCurrentCommandPool()
                .allocate(CommandBufferLevel.PRIMARY)
                .begin(Set.of(CommandBufferUsageFlag.ONE_TIME_SUBMIT))
                .bindPipeline(pipeline)
                .bindDescriptorSet(pipeline.getBindpoint(), pipeline.getPipelineLayout(), 0, descriptorSet)
                .dispatch(WORK_GROUP_COUNT)
                .end();

        final var queue = queueFamily.getQueue(0);
        final var fence = device.acquireFence();

        queue.submit(List.of(commandBuffer), fence);
        fence.waitFor().release();

        final var outData = outputBuffer.map();

        System.out.println("Output:");
        for (int i = 0; i < INPUT_SIZE; i++) {
            System.out.printf("%.2f, ", outData.getFloat(i * Float.BYTES));
        }

        System.out.println();

        outputBuffer.unmap();

        queueFamily.detach();

        pipeline.release();

        outputBuffer.free();
        inputBuffer.free();

        device.free();
        instance.free();
    }
}
