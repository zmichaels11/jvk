module jvk.base {
    requires org.lwjgl;
    requires org.lwjgl.vulkan;
    requires org.lwjgl.glfw;

    exports com.longlinkislong.jvk;
    exports com.longlinkislong.jvk.memory;
}