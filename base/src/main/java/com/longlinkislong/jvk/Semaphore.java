package com.longlinkislong.jvk;

import java.lang.ref.WeakReference;
import java.util.Objects;

public final class Semaphore {
    public final long handle;
    private final WeakReference<SemaphorePool> pool;

    Semaphore(final SemaphorePool pool, final long handle) {
        this.pool = new WeakReference<>(pool);
        this.handle = handle;
    }

    public void release() {
        this.getSemaphorePool().releaseSemaphore(this);
    }

    public SemaphorePool getSemaphorePool() {
        return Objects.requireNonNull(this.pool.get(), "SemaphorePool was lost!");
    }

    public Device getDevice() {
        return this.getSemaphorePool().getDevice();
    }
}
