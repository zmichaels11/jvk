package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkAttachmentReference;
import org.lwjgl.vulkan.VkSubpassDescription;

import java.util.List;
import java.util.stream.IntStream;

public final class SubpassDescription {
    public final int flags;
    public final PipelineBindpoint pipelineBindpoint;
    public final List<AttachmentReference> inputAttachments;
    public final List<AttachmentReference> colorAttachments;
    public final List<AttachmentReference> resolveAttachments;
    public final AttachmentReference depthStencilAttachment;
    public final List<Integer> preserveAttachments;

    public SubpassDescription(
            final int flags,
            final PipelineBindpoint pipelineBindpoint,
            final List<AttachmentReference> inputAttachments,
            final List<AttachmentReference> colorAttachments,
            final List<AttachmentReference> resolveAttachments,
            final AttachmentReference depthStencilAttachment,
            final List<Integer> preserveAttachments) {

        this.flags = flags;
        this.pipelineBindpoint = pipelineBindpoint;
        this.inputAttachments = List.copyOf(inputAttachments);
        this.colorAttachments = List.copyOf(colorAttachments);
        this.resolveAttachments = List.copyOf(resolveAttachments);
        this.depthStencilAttachment = depthStencilAttachment;
        this.preserveAttachments = List.copyOf(preserveAttachments);
    }

    public SubpassDescription() {
        this(0, PipelineBindpoint.GRAPHICS, List.of(), List.of(), List.of(), null, List.of());
    }

    public SubpassDescription withFlags(final int flags) {
        return new SubpassDescription(flags, pipelineBindpoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
    }

    public SubpassDescription withPipelineBindpoint(final PipelineBindpoint pipelineBindpoint) {
        return new SubpassDescription(flags, pipelineBindpoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
    }

    public SubpassDescription withInputAttachments(final List<AttachmentReference> inputAttachments) {
        return new SubpassDescription(flags, pipelineBindpoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
    }

    public SubpassDescription withInputAttachments(final AttachmentReference inputAttachments) {
        return withInputAttachments(List.of(inputAttachments));
    }

    public SubpassDescription withColorAttachments(final List<AttachmentReference> colorAttachments) {
        return new SubpassDescription(flags, pipelineBindpoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
    }

    public SubpassDescription withColorAttachments(final AttachmentReference... colorAttachments) {
        return withColorAttachments(List.of(colorAttachments));
    }

    public SubpassDescription withResolveAttachments(final List<AttachmentReference> resolveAttachments) {
        return new SubpassDescription(flags, pipelineBindpoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
    }

    public SubpassDescription withResolveAttachments(final AttachmentReference... resolveAttachments) {
        return withResolveAttachments(List.of(resolveAttachments));
    }

    public SubpassDescription withDepthStencilAttachment(final AttachmentReference depthStencilAttachment) {
        return new SubpassDescription(flags, pipelineBindpoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
    }

    public SubpassDescription withPreserveAttachments(final List<Integer> preserveAttachments) {
        return new SubpassDescription(flags, pipelineBindpoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
    }

    public SubpassDescription withPreserveAttachments(final Integer... preserveAttachments) {
        return withPreserveAttachments(List.of(preserveAttachments));
    }

    VkSubpassDescription deserialize(final VkSubpassDescription vkobj, final MemoryStack mem) {
        vkobj.flags(flags)
                .pipelineBindPoint(pipelineBindpoint.value)
                .pInputAttachments(AttachmentReference.deserialize(this.inputAttachments, mem))
                .pColorAttachments(AttachmentReference.deserialize(this.colorAttachments, mem))
                .colorAttachmentCount(this.colorAttachments.size())
                .pResolveAttachments(AttachmentReference.deserialize(this.resolveAttachments, mem));

        if (null == this.depthStencilAttachment) {
            vkobj.pDepthStencilAttachment(null);
        } else {
            vkobj.pDepthStencilAttachment(this.depthStencilAttachment.deserialize(VkAttachmentReference.callocStack(mem)));
        }

        if (this.preserveAttachments.isEmpty()) {
            vkobj.pPreserveAttachments(null);
        } else {
            vkobj.pPreserveAttachments(preserveAttachments.stream().collect(BufferCollectors.toIntBuffer(mem)));
        }

        return vkobj;
    }

    static VkSubpassDescription.Buffer deserialize(final List<SubpassDescription> list, final MemoryStack mem) {
        final var out = VkSubpassDescription.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id), mem));

        return out;
    }
}
