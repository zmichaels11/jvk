package com.longlinkislong.jvk;

import org.lwjgl.vulkan.KHRSurface;

import java.util.Arrays;
import java.util.Optional;

public enum ColorSpace {
    SRGB_NONLINEAR(KHRSurface.VK_COLOR_SPACE_SRGB_NONLINEAR_KHR);

    final int value;

    ColorSpace(final int value) {
        this.value = value;
    }

    static Optional<ColorSpace> toColorSpace(final int value) {
        return Arrays.stream(values())
                .filter(cs -> cs.value == value)
                .findFirst();
    }
}
