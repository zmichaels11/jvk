package com.longlinkislong.jvk;

public final class SurfaceFormat {
    public final Format format;
    public final ColorSpace colorSpace;

    public SurfaceFormat(final Format format, final ColorSpace colorSpace) {
        this.format = format;
        this.colorSpace = colorSpace;
    }

    public static final SurfaceFormat DEFAULT = new SurfaceFormat(Format.B8G8R8A8_UNORM, ColorSpace.SRGB_NONLINEAR);
}
