package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkVertexInputBindingDescription;

import java.util.List;
import java.util.stream.IntStream;

public final class VertexInputBindingDescription {
    public final int binding;
    public final int stride;
    public final VertexInputRate inputRate;

    public VertexInputBindingDescription(final int binding, final int stride, final VertexInputRate inputRate) {
        this.binding = binding;
        this.stride = stride;
        this.inputRate = inputRate;
    }

    public VertexInputBindingDescription() {
        this(0, 0, VertexInputRate.PER_VERTEX);
    }

    public VertexInputBindingDescription withBinding(final int binding) {
        return new VertexInputBindingDescription(binding, stride, inputRate);
    }

    public VertexInputBindingDescription withStride(final int stride) {
        return new VertexInputBindingDescription(binding, stride, inputRate);
    }

    public VertexInputBindingDescription withInputRate(final VertexInputRate inputRate) {
        return new VertexInputBindingDescription(binding, stride, inputRate);
    }

    VkVertexInputBindingDescription deserialize(final VkVertexInputBindingDescription vkobj) {
        return vkobj.set(binding, stride, inputRate.value);
    }

    static VkVertexInputBindingDescription.Buffer deserialize(final List<VertexInputBindingDescription> list, final MemoryStack mem) {
        final var out = VkVertexInputBindingDescription.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id)));

        return out;
    }
}
