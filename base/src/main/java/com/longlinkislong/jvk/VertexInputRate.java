package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum VertexInputRate {
    PER_VERTEX(VK10.VK_VERTEX_INPUT_RATE_VERTEX),
    PER_INSTANCE(VK10.VK_VERTEX_INPUT_RATE_INSTANCE);

    final int value;

    VertexInputRate(final int value) {
        this.value = value;
    }
}
