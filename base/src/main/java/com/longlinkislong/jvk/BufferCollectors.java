package com.longlinkislong.jvk;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.List;
import java.util.stream.Collector;

public final class BufferCollectors {
    private BufferCollectors() {}

    public static Collector<Integer, List<Integer>, IntBuffer> toIntBuffer() {
        return IntBufferCollector.allocate(BufferUtils::createIntBuffer);
    }

    public static Collector<Integer, List<Integer>, IntBuffer> toIntBuffer(final MemoryStack mem) {
        return IntBufferCollector.allocate(mem::callocInt);
    }

    public static Collector<Long, List<Long>, LongBuffer> toLongBuffer() {
        return LongBufferCollector.allocate(BufferUtils::createLongBuffer);
    }

    public static Collector<Long, List<Long>, LongBuffer> toLongBuffer(final MemoryStack mem) {
        return LongBufferCollector.allocate(mem::callocLong);
    }

    public static Collector<Long, List<Long>, PointerBuffer> toPointerBuffer() {
        return PointerBufferCollector.allocate(BufferUtils::createPointerBuffer);
    }

    public static Collector<Long, List<Long>, PointerBuffer> toPointerBuffer(final MemoryStack mem) {
        return PointerBufferCollector.allocate(mem::callocPointer);
    }

    public static Collector<Float, List<Float>, FloatBuffer> toFloatBuffer() {
        return FloatBufferCollector.allocate(BufferUtils::createFloatBuffer);
    }

    public static Collector<Float, List<Float>, FloatBuffer> toFloatBuffer(final MemoryStack mem) {
        return FloatBufferCollector.allocate(mem::callocFloat);
    }
}
