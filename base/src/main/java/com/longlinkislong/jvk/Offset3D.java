package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkOffset3D;

public final class Offset3D {
    public final int x;
    public final int y;
    public final int z;

    public Offset3D(final int x, final int y, final int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    VkOffset3D deserialize(final VkOffset3D vkobj) {
        return vkobj.set(x, y, z);
    }

    public static Offset3D ZERO = new Offset3D(0, 0, 0);
}
