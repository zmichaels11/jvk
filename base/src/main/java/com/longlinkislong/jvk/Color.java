package com.longlinkislong.jvk;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public final class Color {
    public final float red;
    public final float green;
    public final float blue;
    public final float alpha;

    public Color(final float red, final float green, final float blue, final float alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    ByteBuffer deserialize(ByteBuffer buffer) {
        return buffer
                .putFloat(0, red)
                .putFloat(4, green)
                .putFloat(8, blue)
                .putFloat(12, alpha);
    }

    FloatBuffer deserialize(final FloatBuffer buffer) {
        return buffer.put(0, red)
                .put(1, green)
                .put(2, blue)
                .put(3, alpha);
    }
}
