package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum LoadOp {
    DONT_CARE(VK10.VK_ATTACHMENT_LOAD_OP_DONT_CARE),
    LOAD(VK10.VK_ATTACHMENT_LOAD_OP_LOAD),
    CLEAR(VK10.VK_ATTACHMENT_LOAD_OP_CLEAR);

    final int value;

    LoadOp(final int value) {
        this.value = value;
    }
}
