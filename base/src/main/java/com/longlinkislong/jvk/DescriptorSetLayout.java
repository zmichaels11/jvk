package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDescriptorSetLayoutCreateInfo;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class DescriptorSetLayout {
    public static final class CreateInfo {
        public final int flags;
        public final List<DescriptorSetLayoutBinding> bindings;

        public CreateInfo(final int flags, final List<DescriptorSetLayoutBinding> bindings) {
            this.flags = flags;
            this.bindings = List.copyOf(bindings);
        }

        public CreateInfo() {
            this(0, List.of());
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, bindings);
        }

        public CreateInfo withBindings(final List<DescriptorSetLayoutBinding> bindings) {
            return new CreateInfo(flags, bindings);
        }

        public CreateInfo withBindings(final DescriptorSetLayoutBinding... bindings) {
            return withBindings(List.of(bindings));
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CreateInfo that = (CreateInfo) o;
            return flags == that.flags &&
                    Objects.equals(bindings, that.bindings);
        }

        @Override
        public int hashCode() {
            return Objects.hash(flags, bindings);
        }
    }

    public final long handle;
    public final CreateInfo info;
    private final WeakReference<DescriptorSetLayoutCache> cache;
    public final DescriptorPool pool;

    DescriptorSetLayout(final DescriptorSetLayoutCache cache, final CreateInfo info) {
        this.cache = new WeakReference<>(cache);
        this.info = info;

        final var device = cache.getDevice();

        try (var mem = MemoryStack.stackPush()) {
            final var pvkDescriptorSetCI = VkDescriptorSetLayoutCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO)
                    .flags(info.flags)
                    .pBindings(DescriptorSetLayoutBinding.deserialize(info.bindings, mem));

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateDescriptorSetLayout(device.handle, pvkDescriptorSetCI, null, pHandle));

            this.handle = pHandle.get();
        }

        final var poolSizes = new HashMap<DescriptorType, Integer>();

        for (var binding : info.bindings) {
            final var count = poolSizes.getOrDefault(binding.descriptorType, 0);

            poolSizes.put(binding.descriptorType, count + binding.descriptorCount);
        }

        final var poolCI = new DescriptorPool.CreateInfo(0, 32, poolSizes.entrySet().stream()
                .map(entry -> new DescriptorPoolSize(entry.getKey().value, entry.getValue()))
                .collect(Collectors.toList()));

        this.pool = new DescriptorPool(poolCI, this);
    }

    public DescriptorSet allocate() {
        return this.pool.allocate();
    }

    public List<DescriptorSet> allocate(final int nSets) {
        return this.pool.allocate(nSets);
    }

    void free() {
        this.pool.free();
        VK10.vkDestroyDescriptorSetLayout(this.getDevice().handle, this.handle, null);
    }

    public void release() {
        this.getDescriporSetLayoutCache().releaseDescriptorSetLayout(this);
    }

    public Device getDevice() {
        return this.getDescriporSetLayoutCache().getDevice();
    }

    public DescriptorSetLayoutCache getDescriporSetLayoutCache() {
        return Objects.requireNonNull(this.cache.get(), "DescriptorSetLayoutCache was lost!");
    }
}
