package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class Fence {
    public final long handle;
    private final WeakReference<FencePool> pool;

    Fence(final FencePool pool, final long handle) {
        this.handle = handle;
        this.pool = new WeakReference<>(pool);
    }

    public FencePool getFencePool() {
        return Objects.requireNonNull(this.pool.get(), "FencePool was lost!");
    }

    public Device getDevice() {
        return this.getFencePool().getDevice();
    }

    public boolean isSignaled() {
        switch(VK10.vkGetFenceStatus(this.getDevice().handle, this.handle)) {
            case VK10.VK_SUCCESS:
                return true;
            case VK10.VK_NOT_READY:
                return false;
            case VK10.VK_ERROR_DEVICE_LOST:
                throw new RuntimeException("Device was lost!");
            default:
                throw new RuntimeException("Unknown error!");
        }
    }

    public Fence resetFence() {
        vkAssert(VK10.vkResetFences(this.getDevice().handle, this.handle));
        return this;
    }

    public Fence waitFor() {
        VK10.vkWaitForFences(this.getDevice().handle, this.handle, true, Long.MAX_VALUE);
        return this;
    }

    public Fence waitFor(final long timeout, final TimeUnit unit) throws TimeoutException {
        final int result = VK10.vkWaitForFences(this.getDevice().handle, this.handle, true, unit.toNanos(timeout));

        switch (result) {
            case VK10.VK_TIMEOUT: throw new TimeoutException();
            default: vkAssert(result);
        }

        return this;
    }

    public void release() {
        this.getFencePool().releaseFence(this);
    }
}
