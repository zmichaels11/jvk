package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.lang.ref.WeakReference;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class Queue {
    public static final class SubmitWaitInfo {
        public final Semaphore semaphore;
        public final Set<PipelineStageFlag> stageFlags;

        public SubmitWaitInfo(final Semaphore semaphore, final Set<PipelineStageFlag> stageFlags) {
            this.semaphore = semaphore;
            this.stageFlags = Set.copyOf(stageFlags);
        }

        public SubmitWaitInfo() {
            this(null, Set.of(PipelineStageFlag.TOP_OF_PIPE));
        }

        public SubmitWaitInfo withSemaphore(final Semaphore semaphore) {
            return new SubmitWaitInfo(semaphore, stageFlags);
        }

        public SubmitWaitInfo withStageFlags(final Set<PipelineStageFlag> stageFlags) {
            return new SubmitWaitInfo(semaphore, stageFlags);
        }

        public SubmitWaitInfo withStageFlags(final PipelineStageFlag... stageFlags) {
            return withStageFlags(Set.of(stageFlags));
        }
    }

    public static final class SubmitInfo {
        public final List<SubmitWaitInfo> waitInfos;
        public final List<Semaphore> signalSemaphores;
        public final List<CommandBuffer> commandBuffers;

        public SubmitInfo(final List<SubmitWaitInfo> waitInfos, final List<Semaphore> signalSemaphores, final List<CommandBuffer>commandBuffers) {
            this.waitInfos = List.copyOf(waitInfos);
            this.signalSemaphores = List.copyOf(signalSemaphores);
            this.commandBuffers = List.copyOf(commandBuffers);
        }

        public SubmitInfo() {
            this(List.of(), List.of(), List.of());
        }

        public SubmitInfo withWaitInfos(final List<SubmitWaitInfo> waitInfos) {
            return new SubmitInfo(waitInfos, signalSemaphores, commandBuffers);
        }

        public SubmitInfo withWaitInfos(final SubmitWaitInfo... waitInfos) {
            return withWaitInfos(List.of(waitInfos));
        }

        public SubmitInfo withSignalSemaphores(final List<Semaphore> signalSemaphores) {
            return new SubmitInfo(waitInfos, signalSemaphores, commandBuffers);
        }

        public SubmitInfo withSignalSemaphores(final Semaphore... signalSemaphores) {
            return withSignalSemaphores(List.of(signalSemaphores));
        }

        public SubmitInfo withCommandBuffers(final List<CommandBuffer> commandBuffers) {
            return new SubmitInfo(waitInfos, signalSemaphores, commandBuffers);
        }

        public SubmitInfo withCommandBuffers(final CommandBuffer... commandBuffers) {
            return withCommandBuffers(List.of(commandBuffers));
        }
    }

    public final VkQueue handle;
    public final int queueIndex;
    private final WeakReference<Device> device;
    private final int queueFamilyIndex;

    Queue(final Device device, final int queueFamilyIndex, final int queueIndex) {
        this.device = new WeakReference<>(device);
        this.queueFamilyIndex = queueFamilyIndex;
        this.queueIndex = queueIndex;

        try (var mem = MemoryStack.stackPush()) {
            final var pHandle = mem.callocPointer(1);

            VK10.vkGetDeviceQueue(device.handle, queueFamilyIndex, queueIndex, pHandle);

            this.handle = new VkQueue(pHandle.get(), device.handle);
        }
    }

    public QueueFamily getQueueFamily() {
        return this.getDevice().queueFamilies.get(this.queueFamilyIndex);
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public void waitIdle() {
        VK10.vkQueueWaitIdle(this.handle);
    }

    public void submit(final List<CommandBuffer> commands) {
        submit(commands, null);
    }

    public void submit(final List<CommandBuffer> commands, final Fence fence) {
        try (var mem = MemoryStack.stackPush()) {
            final var pCommandBuffers = commands.stream()
                    .mapToLong(cmdbuf -> cmdbuf.handle.address())
                    .boxed()
                    .collect(BufferCollectors.toPointerBuffer(mem));

            final var pvkSubmitInfo = VkSubmitInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SUBMIT_INFO)
                    .pCommandBuffers(pCommandBuffers);

            vkAssert(VK10.vkQueueSubmit(this.handle, pvkSubmitInfo, fence == null ? VK10.VK_NULL_HANDLE : fence.handle));
        }
    }

    public void submit(final SubmitInfo submitInfo) {
        submit(submitInfo, null);
    }

    public void submit(final SubmitInfo submitInfo, final Fence fence) {
        try (var mem = MemoryStack.stackPush()) {
            final var pCommandBuffers = submitInfo.commandBuffers.stream()
                    .mapToLong(cmdbuf -> cmdbuf.handle.address())
                    .boxed()
                    .collect(BufferCollectors.toPointerBuffer(mem));

            final var pWaitSemaphores = submitInfo.waitInfos.stream()
                    .mapToLong(waitInfo -> waitInfo.semaphore.handle)
                    .boxed()
                    .collect(BufferCollectors.toLongBuffer(mem));

            final var pWaitDstStageMask = submitInfo.waitInfos.stream()
                    .mapToInt(waitInfo -> PipelineStageFlag.toBitfield(waitInfo.stageFlags))
                    .boxed()
                    .collect(BufferCollectors.toIntBuffer(mem));

            final var pSignalSemaphores = submitInfo.signalSemaphores.stream()
                    .mapToLong(semaphore -> semaphore.handle)
                    .boxed()
                    .collect(BufferCollectors.toLongBuffer(mem));

            final var pvkSubmitInfo = VkSubmitInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SUBMIT_INFO)
                    .waitSemaphoreCount(submitInfo.waitInfos.size())
                    .pWaitDstStageMask(pWaitDstStageMask)
                    .pWaitSemaphores(pWaitSemaphores)
                    .pSignalSemaphores(pSignalSemaphores)
                    .pCommandBuffers(pCommandBuffers);

            vkAssert(VK10.vkQueueSubmit(this.handle, pvkSubmitInfo, fence == null ? VK10.VK_NULL_HANDLE : fence.handle));
        }
    }

    public static final class PresentInfo {
        public final Swapchain swapchain;
        public final List<Semaphore> waitSemaphores;
        public final Image image;
        public final ImageLayout imageLayout;
        public final Set<AccessFlag> imageAccess;
        public final QueueFamily srcQueueFamily;
        public final Set<PipelineStageFlag> srcStageMask;

        public PresentInfo(
                final Swapchain swapchain, final Image image,
                final ImageLayout imageLayout, final Set<AccessFlag> imageAccess, final Set<PipelineStageFlag> srcStageMask,
                final List<Semaphore> waitSemaphores, final QueueFamily srcQueueFamily) {

            this.srcStageMask = Set.copyOf(srcStageMask);
            this.swapchain = swapchain;
            this.waitSemaphores = List.copyOf(waitSemaphores);
            this.image = image;
            this.imageLayout = imageLayout;
            this.imageAccess = Set.copyOf(imageAccess);
            this.srcQueueFamily = srcQueueFamily;
        }

        public PresentInfo() {
            this(null, null, ImageLayout.UNDEFINED, Set.of(), Set.of(), List.of(), null);
        }

        public PresentInfo withSwapchain(final Swapchain swapchain) {
            return new PresentInfo(swapchain, image, imageLayout, imageAccess, srcStageMask, waitSemaphores, srcQueueFamily);
        }

        public PresentInfo withImage(final Image image) {
            return new PresentInfo(swapchain, image, imageLayout, imageAccess, srcStageMask, waitSemaphores, srcQueueFamily);
        }

        public PresentInfo withImageLayout(final ImageLayout imageLayout) {
            return new PresentInfo(swapchain, image, imageLayout, imageAccess, srcStageMask, waitSemaphores, srcQueueFamily);
        }

        public PresentInfo withImageAccess(final Set<AccessFlag> imageAccess) {
            return new PresentInfo(swapchain, image, imageLayout, imageAccess, srcStageMask, waitSemaphores, srcQueueFamily);
        }

        public PresentInfo withImageAccess(final AccessFlag... imageAccess) {
            return withImageAccess(Set.of(imageAccess));
        }

        public PresentInfo withWaitSemaphores(final List<Semaphore> waitSemaphores) {
            return new PresentInfo(swapchain, image, imageLayout, imageAccess, srcStageMask, waitSemaphores, srcQueueFamily);
        }

        public PresentInfo withWaitSemaphores(final Semaphore... waitSemaphores) {
            return withWaitSemaphores(List.of(waitSemaphores));
        }

        public PresentInfo withSrcQueueFamily(final QueueFamily srcQueueFamily) {
            return new PresentInfo(swapchain, image, imageLayout, imageAccess, srcStageMask, waitSemaphores, srcQueueFamily);
        }

        public PresentInfo withSrcStageMask(final Set<PipelineStageFlag> srcStageMask) {
            return new PresentInfo(swapchain, image, imageLayout, imageAccess, srcStageMask, waitSemaphores, srcQueueFamily);
        }

        public PresentInfo withSrcStageMask(final PipelineStageFlag... srcStageMask) {
            return withSrcStageMask(Set.of(srcStageMask));
        }
    }

    private final class InternalCommandBuffer {
        private final CommandBuffer commandBuffer;
        private final Fence fence;
        Semaphore imageAcquireSemaphore;
        Semaphore presentSemaphore;

        private InternalCommandBuffer() {
            this.commandBuffer = Queue.this.getQueueFamily().getCurrentCommandPool().allocate(CommandBufferLevel.PRIMARY);
            this.fence = Queue.this.getDevice().acquireFence();
        }
    }

    private final java.util.Queue<InternalCommandBuffer> commandBuffers = new ArrayDeque<>();

    private final InternalCommandBuffer acquireCommandBuffer() {
        if (!this.commandBuffers.isEmpty()) {
            final var icb = this.commandBuffers.peek();

            if (icb.fence.isSignaled()) {
                icb.fence.resetFence();
                icb.imageAcquireSemaphore.release();
                icb.presentSemaphore.release();

                return this.commandBuffers.poll();
            }
        }

        return new InternalCommandBuffer();
    }

    public void present(final PresentInfo info) throws InterruptedException {
        final var cmd = this.acquireCommandBuffer();
        final var backBuffer = info.swapchain.acquireNextImage();
        final var srcStageMask = new HashSet<>(info.srcStageMask);
        final QueueFamily dstQueueFamily = (info.srcQueueFamily == null) ? null : this.getQueueFamily();

        srcStageMask.add(PipelineStageFlag.BOTTOM_OF_PIPE);

        final var imageMemoryBarriers = List.of(
                new ImageMemoryBarrier(info.imageAccess, Set.of(AccessFlag.TRANSFER_READ), info.imageLayout, ImageLayout.TRANSFER_SRC_OPTIMAL, info.srcQueueFamily, dstQueueFamily, info.image, info.image.getFullRange()),
                new ImageMemoryBarrier(Set.of(), Set.of(AccessFlag.TRANSFER_WRITE), ImageLayout.UNDEFINED, ImageLayout.TRANSFER_DST_OPTIMAL, null, null, backBuffer.image, backBuffer.image.getFullRange()));

        cmd.imageAcquireSemaphore = backBuffer.acquireSemaphore;
        cmd.presentSemaphore = this.getDevice().acquireSemaphore();
        cmd.commandBuffer
                .begin(Set.of())
                .pipelineBarrier(srcStageMask, Set.of(PipelineStageFlag.TRANSFER), 0, List.of(), List.of(), imageMemoryBarriers)
                .copyImage(
                        info.image, ImageLayout.TRANSFER_SRC_OPTIMAL,
                        backBuffer.image, ImageLayout.TRANSFER_DST_OPTIMAL,
                        Offset3D.ZERO, Offset3D.ZERO, info.image.info.extent,
                        info.image.getSubresourceLayers(0, 0, 1),
                        backBuffer.image.getSubresourceLayers(0, 0, 1))
                .transferImage(backBuffer.image, ImageLayout.TRANSFER_DST_OPTIMAL, ImageLayout.PRESENT_SRC_KHR, Set.of(PipelineStageFlag.TRANSFER), Set.of(PipelineStageFlag.BOTTOM_OF_PIPE), Set.of(AccessFlag.TRANSFER_WRITE), Set.of(AccessFlag.MEMORY_READ), null, null)
                .end();

        commandBuffers.offer(cmd);

        final var waitInfos = Stream.concat(info.waitSemaphores.stream(), Stream.of(cmd.imageAcquireSemaphore))
                .map(semaphore -> new SubmitWaitInfo(semaphore, info.srcStageMask))
                .collect(Collectors.toList());

        this.submit(new SubmitInfo()
                .withCommandBuffers(cmd.commandBuffer)
                .withWaitInfos(waitInfos)
                .withSignalSemaphores(cmd.presentSemaphore),
                cmd.fence);

        try (var mem = MemoryStack.stackPush()) {
            final var swapchainHandle = info.swapchain.getHandle().orElseThrow(() -> new IllegalStateException("Swapchain is not valid!"));
            final var pvkPresentInfo = VkPresentInfoKHR.callocStack(mem)
                    .sType(KHRSwapchain.VK_STRUCTURE_TYPE_PRESENT_INFO_KHR)
                    .pImageIndices(mem.ints(backBuffer.index))
                    .swapchainCount(1)
                    .pSwapchains(mem.longs(swapchainHandle))
                    .pWaitSemaphores(mem.longs(cmd.presentSemaphore.handle));

            KHRSwapchain.vkQueuePresentKHR(this.handle, pvkPresentInfo);
        }
    }
}
