package com.longlinkislong.jvk;

import org.lwjgl.glfw.GLFWVulkan;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class Instance {
    private static final Set<String> DEFAULT_LAYERS;
    private static final Set<String> DEFAULT_EXTENSIONS;
    private static final Set<String> ENABLED_LAYERS;
    private static final Set<String> ENABLED_EXTENSIONS;
    private static final Object LOCK = new Object();
    private static Instance INSTANCE = null;
    private static String APPLICATION_NAME = "JVK Application";
    private static String ENGINE_NAME = "JVK";
    private static int ENGINE_VERSION = VK10.VK_MAKE_VERSION(1, 0, 0);
    private static int API_VERSION = VK10.VK_API_VERSION_1_0;
    private static int APPLICATION_VERSION = VK10.VK_MAKE_VERSION(1, 0, 0);

    static {
        DEFAULT_LAYERS = Arrays.stream(System.getProperty("Instance.DEFAULT_LAYERS", "").split(","))
                .map(String::trim)
                .filter(name -> !name.isEmpty())
                .collect(Collectors.toUnmodifiableSet());

        DEFAULT_EXTENSIONS = Arrays.stream(System.getProperty("Instance.DEFAULT_EXTENSIONS", "").split(","))
                .map(String::trim)
                .filter(name -> !name.isEmpty())
                .collect(Collectors.toUnmodifiableSet());

        ENABLED_LAYERS = new HashSet<>(DEFAULT_LAYERS);
        ENABLED_EXTENSIONS = new HashSet<>(DEFAULT_EXTENSIONS);
    }

    public static void setApplicationName(final String name) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            APPLICATION_NAME = name;
        }
    }

    public static String getApplicationName() {
        synchronized (LOCK) {
            return APPLICATION_NAME;
        }
    }

    public static void setEngineName(final String name) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            ENGINE_NAME = name;
        }
    }

    public static String getEngineName() {
        synchronized (LOCK) {
            return ENGINE_NAME;
        }
    }

    public static void setApplicationVersion(final int major, final int minor, final int patch) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            APPLICATION_VERSION = VK10.VK_MAKE_VERSION(major, minor, patch);
        }
    }

    public static int getApplicationVersion() {
        synchronized (LOCK) {
            return APPLICATION_VERSION;
        }
    }

    public static void setEngineVersion(final int major, final int minor, final int patch) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            ENGINE_VERSION = VK10.VK_MAKE_VERSION(major, minor, patch);
        }
    }

    public static int getEngineVersion() {
        synchronized (LOCK) {
            return ENGINE_VERSION;
        }
    }

    public static void setAPIVersion(final int major, final int minor, final int patch) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            API_VERSION = VK10.VK_MAKE_VERSION(major, minor, patch);
        }
    }

    public static int getAPIVersion() {
        synchronized (LOCK) {
            return API_VERSION;
        }
    }

    public static void restoreDefaultLayers() {
        synchronized (LOCK) {
            assert null == INSTANCE;

            ENABLED_LAYERS.clear();
            ENABLED_LAYERS.addAll(DEFAULT_LAYERS);
        }
    }

    public static void restoreExtensions() {
        synchronized (LOCK) {
            assert null == INSTANCE;

            ENABLED_EXTENSIONS.clear();
            ENABLED_EXTENSIONS.addAll(DEFAULT_EXTENSIONS);
        }
    }

    public static void enableLayer(final String layer) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            ENABLED_LAYERS.add(layer);
        }
    }

    public static boolean disableLayer(final String layer) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            return ENABLED_LAYERS.remove(layer);
        }
    }

    public static void enableExtension(final String extension) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            ENABLED_EXTENSIONS.add(extension);
        }
    }

    public static boolean disableExtension(final String extension) {
        synchronized (LOCK) {
            assert null == INSTANCE;

            return ENABLED_EXTENSIONS.remove(extension);
        }
    }

    public static boolean isExtensionEnabled(final String extension) {
        synchronized (LOCK) {
            return ENABLED_EXTENSIONS.contains(extension);
        }
    }

    public static boolean isLayerEnabled(final String layer) {
        synchronized (LOCK) {
            return ENABLED_LAYERS.contains(layer);
        }
    }

    public static Set<String> getEnabledLayers() {
        synchronized (LOCK) {
            return Set.copyOf(ENABLED_LAYERS);
        }
    }

    public static Set<String> getEnabledExtensions() {
        synchronized (LOCK) {
            return Set.copyOf(ENABLED_EXTENSIONS);
        }
    }

    public static Instance get() {
        synchronized (LOCK) {
            if (null == INSTANCE) {
                INSTANCE = new Instance();
            }

            return INSTANCE;
        }
    }

    public final VkInstance handle;
    public final List<PhysicalDevice> physicalDevices;

    public void free() {
        VK10.vkDestroyInstance(this.handle, null);
    }

    private Instance() {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkApplicationInfo = VkApplicationInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_APPLICATION_INFO)
                    .apiVersion(API_VERSION)
                    .applicationVersion(APPLICATION_VERSION)
                    .pApplicationName(mem.UTF8(APPLICATION_NAME))
                    .engineVersion(ENGINE_VERSION)
                    .pEngineName(mem.UTF8(ENGINE_NAME));

            final var ppEnabledLayerNames = ENABLED_LAYERS.stream()
                    .map(mem::UTF8)
                    .mapToLong(MemoryUtil::memAddress)
                    .boxed()
                    .collect(BufferCollectors.toPointerBuffer(mem));

            final var ppEnabledExtensionNames = ENABLED_EXTENSIONS.stream()
                    .map(mem::UTF8)
                    .mapToLong(MemoryUtil::memAddress)
                    .boxed()
                    .collect(BufferCollectors.toPointerBuffer(mem));

            final var pvkInstanceCI = VkInstanceCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO)
                    .pApplicationInfo(pvkApplicationInfo)
                    .ppEnabledExtensionNames(ppEnabledExtensionNames)
                    .ppEnabledLayerNames(ppEnabledLayerNames);

            final var pHandle = mem.callocPointer(1);

            vkAssert(VK10.vkCreateInstance(pvkInstanceCI, null, pHandle));

            this.handle = new VkInstance(pHandle.get(), pvkInstanceCI);

            final var pPhysicalDeviceCount = mem.callocInt(1);

            VK10.vkEnumeratePhysicalDevices(this.handle, pPhysicalDeviceCount, null);

            final var physicalDeviceCount = pPhysicalDeviceCount.get(0);
            final var pPhysicalDevices = mem.callocPointer(physicalDeviceCount);

            VK10.vkEnumeratePhysicalDevices(this.handle, pPhysicalDeviceCount, pPhysicalDevices);

            this.physicalDevices = BufferStreams.of(pPhysicalDevices)
                    .mapToObj(handle -> new VkPhysicalDevice(handle, this.handle))
                    .map(PhysicalDevice::new)
                    .collect(Collectors.toUnmodifiableList());
        }
    }

    public long createWindowSurface(final long window) {
        try (var mem = MemoryStack.stackPush()) {
            final var pHandle = mem.callocLong(1);

            vkAssert(GLFWVulkan.glfwCreateWindowSurface(this.handle, window, null, pHandle));

            return pHandle.get();
        }
    }
}
