package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineMultisampleStateCreateInfo;

import java.nio.IntBuffer;

public final class PipelineMultisampleStateCreateInfo {
    public final int flags;
    public final int rasterizationSamples;
    public final boolean sampleShadingEnable;
    public final float minSampleShading;
    public final IntBuffer pSampleMask;
    public final boolean alphaToCoverageEnable;
    public final boolean alphaToOneEnable;

    public PipelineMultisampleStateCreateInfo(
            final int flags,
            final int rasterizationSamples,
            final boolean sampleShadingEnable,
            final float minSampleShading,
            final IntBuffer pSampleMask,
            final boolean alphaToCoverageEnable, final boolean alphaToOneEnable) {

        this.flags = flags;
        this.rasterizationSamples = rasterizationSamples;
        this.sampleShadingEnable = sampleShadingEnable;
        this.minSampleShading = minSampleShading;
        this.pSampleMask = pSampleMask;
        this.alphaToCoverageEnable = alphaToCoverageEnable;
        this.alphaToOneEnable = alphaToOneEnable;
    }

    public PipelineMultisampleStateCreateInfo() {
        this(0, VK10.VK_SAMPLE_COUNT_1_BIT, false, 0.0F, null, false, false);
    }

    public PipelineMultisampleStateCreateInfo withFlags(final int flags) {
        return new PipelineMultisampleStateCreateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public PipelineMultisampleStateCreateInfo withRasterizationSamples(final int rasterizationSamples) {
        return new PipelineMultisampleStateCreateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public PipelineMultisampleStateCreateInfo withSampleShadingEnable(final boolean sampleShadingEnable) {
        return new PipelineMultisampleStateCreateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public PipelineMultisampleStateCreateInfo withMinSampleShading(final float minSampleShading) {
        return new PipelineMultisampleStateCreateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public PipelineMultisampleStateCreateInfo withSampleMask(final IntBuffer pSampleMask) {
        return new PipelineMultisampleStateCreateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public PipelineMultisampleStateCreateInfo withAlphaToOneCoverageEnable(final boolean alphaToOneCoverageEnable) {
        return new PipelineMultisampleStateCreateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public PipelineMultisampleStateCreateInfo withAlphaToOneEnable(final boolean alphaToOneEnable) {
        return new PipelineMultisampleStateCreateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    VkPipelineMultisampleStateCreateInfo deserialize(VkPipelineMultisampleStateCreateInfo vkobj) {
        return vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO)
                .pNext(VK10.VK_NULL_HANDLE)
                .flags(flags)
                .rasterizationSamples(rasterizationSamples)
                .sampleShadingEnable(sampleShadingEnable)
                .minSampleShading(minSampleShading)
                .pSampleMask(pSampleMask)
                .alphaToCoverageEnable(alphaToCoverageEnable)
                .alphaToOneEnable(alphaToOneEnable);
    }
}
