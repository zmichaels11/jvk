package com.longlinkislong.jvk.memory;

import com.longlinkislong.jvk.Util;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkMemoryAllocateInfo;
import org.lwjgl.vulkan.VkMemoryRequirements;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.Objects;

import static com.longlinkislong.jvk.Util.vkAssert;

final class BuddyBlockMemoryAllocator implements MemoryAllocator {
    private final Object lock = new Object();
    private final WeakReference<VkDevice> device;
    private final long size;
    private final long handle;
    private final long minSize;
    private final int typeIndex;
    private BuddyBlock root;
    private ByteBuffer address;
    private int mapCount;

    BuddyBlockMemoryAllocator(final VkDevice device, final int typeIndex, final long minSize, final long totalSize) {
        this.device = new WeakReference<>(device);
        this.size = totalSize;
        this.minSize = minSize;
        this.typeIndex = typeIndex;

        try (var mem = MemoryStack.stackPush()) {
            final var info = VkMemoryAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                    .memoryTypeIndex(typeIndex)
                    .allocationSize(this.size);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkAllocateMemory(device, info, null, pHandle));

            this.handle = pHandle.get();
        }

        this.root = new BuddyBlock(0L, this.size);
    }

    @Override
    public boolean isEmpty() {
        synchronized (lock) {
            this.root.reclaim();

            return this.root.type == MemoryType.FREE;
        }
    }

    @Override
    public int getTypeIndex() {
        return this.typeIndex;
    }

    @Override
    public MemoryBlock malloc(MemoryType type, VkMemoryRequirements pMemReqs) {
        final long size = pMemReqs.size();
        final long alignment = pMemReqs.alignment();
        final BuddyBlock alloc;

        synchronized (lock) {
            this.root.reclaim();

            alloc = this.root.sub(size, alignment);
        }

        if (alloc == null) {
            throw new OutOfMemoryError();
        }

        alloc.type = type;
        alloc.align(alignment);

        return alloc;
    }

    @Override
    public void free() {
        synchronized (lock) {
            VK10.vkFreeMemory(this.getDevice(), this.handle, null);
        }
    }

    @Override
    public VkDevice getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    private ByteBuffer map() {
        synchronized (lock) {
            if (this.address == null) {
                try (var mem = MemoryStack.stackPush()) {
                    final var ppData = mem.callocPointer(1);

                    vkAssert(VK10.vkMapMemory(this.getDevice(), this.handle, 0L, this.size, 0, ppData));

                    this.address = ppData.getByteBuffer(0, (int) this.size);
                }
            }

            this.mapCount++;

            return this.address;
        }
    }

    private void unmap() {
        synchronized (lock) {
            if (0 == --this.mapCount) {
                this.address = null;
                VK10.vkUnmapMemory(this.getDevice(), this.handle);
            }
        }
    }

    private final class BuddyBlock implements MemoryBlock {
        private final long offset;
        private final long size;
        private MemoryType type;
        private long alignedOffset;
        private BuddyBlock left, right;

        private BuddyBlock(final long offset, final long size) {
            this.offset = offset;
            this.size = size;
            this.type = MemoryType.FREE;
            this.alignedOffset = offset;
            this.left = this.right = null;
        }

        private void reclaim() {
            if (this.left != null && this.right != null) {
                this.left.reclaim();
                this.right.reclaim();

                if (this.left.type == MemoryType.FREE && this.right.type == MemoryType.FREE) {
                    this.type = MemoryType.FREE;
                    this.left = null;
                    this.right = null;
                }
            }
        }

        private BuddyBlock sub(long size, long alignment) {
            if (this.left != null) {
                final var out = this.left.sub(size, alignment);

                if (out != null) {
                    return out;
                }
            }

            if (this.right != null) {
                final var out = this.right.sub(size, alignment);

                if (out != null) {
                    return out;
                }
            }

            if (this.type != MemoryType.FREE) {
                return null;
            }

            this.align(alignment);

            if (this.getSize() < size) {
                return null;
            }

            final long halfSize = this.getSize() / 2;

            if (halfSize < size || halfSize < minSize) {
                // cant split; leave
                return this;
            }

            // try subdividing
            final var left = new BuddyBlock(this.offset, halfSize);
            final var right = new BuddyBlock(this.offset + halfSize, halfSize);

            left.align(alignment);
            right.align(alignment);

            if (left.getSize() >= size) {
                this.type = MemoryType.UNKNOWN;
                this.left = left;
                this.right = right;

                final var out = left.sub(size, alignment);

                if (out != null) {
                    return out;
                }
            }

            if (right.getSize() >= size) {
                this.type = MemoryType.UNKNOWN;
                this.left = left;
                this.right = right;

                final var out = right.sub(size, alignment);

                if (out != null) {
                    return out;
                }
            }

            return this;
        }

        @Override
        public String toString() {
            return String.format("ContextImageType: %s PTR: +0x%x Size: 0x%x", type, offset, size);
        }

        private void align(final long alignment) {
            this.alignedOffset = Util.alignUp(this.offset, alignment);
        }

        @Override
        public long getHandle() {
            return BuddyBlockMemoryAllocator.this.handle;
        }

        @Override
        public long getOffset() {
            return this.alignedOffset;
        }

        @Override
        public long getSize() {
            return this.offset + this.size - this.alignedOffset;
        }

        @Override
        public ByteBuffer map() {
            final var superBlock = BuddyBlockMemoryAllocator.this.map();

            return MemoryUtil.memSlice(superBlock, (int) this.getOffset(), (int) this.getSize());
        }

        @Override
        public void unmap() {
            BuddyBlockMemoryAllocator.this.unmap();
        }

        @Override
        public void free() {
            this.type = MemoryType.FREE;
            this.left = null;
            this.right = null;
        }

        @Override
        public VkDevice getDevice() {
            return BuddyBlockMemoryAllocator.this.getDevice();
        }
    }
}
