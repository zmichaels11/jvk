package com.longlinkislong.jvk.memory;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkMemoryRequirements;
import org.lwjgl.vulkan.VkPhysicalDeviceMemoryProperties;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class MemoryManager {
    private static final long LARGE_ALLOC_THRESHOLD = 128 * 1024 * 1024L;
    private static final long SMALL_ALLOC_THRESHOLD = 32 * 1024L;
    private static final long STANDARD_BUFFER_HEAP_SIZE = 1024 * 1024 * 128L;
    private static final long STANDARD_IMAGE_HEAP_SIZE = 1024 * 1024 * 256L;
    private static final long MINIMUM_BUFFER_SUBDIV_SIZE = 1024 * 4L;
    private static final long MINIMUM_IMAGE_SUBDIV_SIZE = 1024 * 4L;
    private static final List<SlabMemoryAllocator.SlabSizeInfo> SMALL_HEAP_SIZES;

    static {
        SMALL_HEAP_SIZES = List.of(
                new SlabMemoryAllocator.SlabSizeInfo(4 * 1024L, 256),
                new SlabMemoryAllocator.SlabSizeInfo(8 * 1024L, 128),
                new SlabMemoryAllocator.SlabSizeInfo(16 * 1024L, 64),
                new SlabMemoryAllocator.SlabSizeInfo(32 * 1024L, 32));
    }

    private final WeakReference<VkDevice> device;
    private final UniqueMemoryAllocator[] largeBufferHeaps, largeImageHeaps;
    private final List<SlabMemoryAllocator> smallBufferHeaps = new ArrayList<>();
    private final List<SlabMemoryAllocator> smallImageHeaps = new ArrayList<>();
    private final List<BuddyBlockMemoryAllocator> standardBufferHeaps = new ArrayList<>();
    private final List<BuddyBlockMemoryAllocator> standardImageHeaps = new ArrayList<>();
    private final VkPhysicalDeviceMemoryProperties pPhysicalDeviceMemoryProperties;
    private final Object lock = new Object();

    public MemoryManager(final VkDevice device) {
        this.device = new WeakReference<>(device);
        this.pPhysicalDeviceMemoryProperties = VkPhysicalDeviceMemoryProperties.calloc();

        VK10.vkGetPhysicalDeviceMemoryProperties(device.getPhysicalDevice(), pPhysicalDeviceMemoryProperties);

        final int typeCount = pPhysicalDeviceMemoryProperties.memoryTypeCount();

        this.largeBufferHeaps = new UniqueMemoryAllocator[typeCount];
        this.largeImageHeaps = new UniqueMemoryAllocator[typeCount];
    }

    public VkDevice getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public void garbageCollect() {
        synchronized (lock) {
            Stream.of(this.smallBufferHeaps, this.smallImageHeaps, this.standardBufferHeaps, this.standardImageHeaps)
                    .peek(heap -> heap.stream()
                            .filter(MemoryAllocator::isEmpty)
                            .forEach(MemoryAllocator::free))
                    .forEach(heap -> heap.removeIf(MemoryAllocator::isEmpty));
        }
    }

    public void free() {
        synchronized (lock) {
            Stream.of(this.largeBufferHeaps, this.largeImageHeaps)
                    .flatMap(Arrays::stream)
                    .filter(Objects::nonNull)
                    .forEach(MemoryAllocator::free);

            Stream.of(this.smallBufferHeaps, this.smallImageHeaps, this.standardImageHeaps, this.standardBufferHeaps)
                    .flatMap(List::stream)
                    .forEach(MemoryAllocator::free);
        }
    }

    private static <MemAllocT extends MemoryAllocator> MemoryBlock allocate(
            final MemoryType memType, final VkMemoryRequirements pMemReqs, final int index,
            final List<MemAllocT> allocator, final Supplier<MemAllocT> constructor) {

        final var selectedHeaps = allocator.stream()
                .filter(testHeap -> testHeap.getTypeIndex() == index)
                .collect(Collectors.toList());

        for (var selectedHeap : selectedHeaps) {
            try {
                return selectedHeap.malloc(memType, pMemReqs);
            } catch (OutOfMemoryError err) {
                // selected heap is too full; try next
            }
        }

        final var newHeap = constructor.get();

        allocator.add(newHeap);

        // if this throws OOM; then the wrong heap type was selected
        return newHeap.malloc(memType, pMemReqs);
    }

    public MemoryBlock allocateImageMemory(final VkMemoryRequirements pMemReqs, final int properties) {
        final var index = this.getMemoryTypeIndex(pMemReqs.memoryTypeBits(), properties);
        final var size = pMemReqs.size();
        final var device = this.getDevice();

        synchronized (lock) {
            if (size > LARGE_ALLOC_THRESHOLD) {
                if (this.largeImageHeaps[index] == null) {
                    this.largeImageHeaps[index] = new UniqueMemoryAllocator(device, index);
                }

                return this.largeImageHeaps[index].malloc(MemoryType.IMAGE, pMemReqs);
            } else if (size <= SMALL_ALLOC_THRESHOLD) {
                return allocate(MemoryType.IMAGE, pMemReqs, index, this.smallImageHeaps, () -> new SlabMemoryAllocator(device, index, SMALL_HEAP_SIZES));
            } else {
                return allocate(MemoryType.IMAGE, pMemReqs, index, this.standardImageHeaps, () -> new BuddyBlockMemoryAllocator(device, index, MINIMUM_IMAGE_SUBDIV_SIZE, STANDARD_IMAGE_HEAP_SIZE));
            }
        }
    }

    public MemoryBlock allocateBufferMemory(final VkMemoryRequirements pMemReqs, final int properties) {
        final var index = this.getMemoryTypeIndex(pMemReqs.memoryTypeBits(), properties);
        final var size = pMemReqs.size();
        final var device = this.getDevice();

        synchronized (lock) {
            if (size >= LARGE_ALLOC_THRESHOLD) {
                if (this.largeBufferHeaps[index] == null) {
                    this.largeBufferHeaps[index] = new UniqueMemoryAllocator(device, index);
                }

                return this.largeBufferHeaps[index].malloc(MemoryType.BUFFER, pMemReqs);
            } else if (size < SMALL_ALLOC_THRESHOLD) {
                return allocate(MemoryType.BUFFER, pMemReqs, index, this.smallBufferHeaps, () -> new SlabMemoryAllocator(device, index, SMALL_HEAP_SIZES));
            } else {
                return allocate(MemoryType.BUFFER, pMemReqs, index, this.standardBufferHeaps, () -> new BuddyBlockMemoryAllocator(device, index, MINIMUM_BUFFER_SUBDIV_SIZE, STANDARD_BUFFER_HEAP_SIZE));
            }
        }
    }

    private int getMemoryTypeIndex(
            final int typeBits, final int requirementsMask) {

        final var props = this.pPhysicalDeviceMemoryProperties;

        for (int i = 0; i < props.memoryTypeCount(); i++) {
            if ((typeBits & (1 << i)) != 0) {
                if ((props.memoryTypes(i).propertyFlags() & requirementsMask) == requirementsMask) {
                    return i;
                }
            }
        }

        throw new UnsupportedOperationException("No MemoryType exists with the required features!");
    }

}
