package com.longlinkislong.jvk.memory;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;

import java.nio.ByteBuffer;

import static com.longlinkislong.jvk.Util.vkAssert;

public interface MemoryBlock {
    long getHandle();

    long getOffset();

    long getSize();

    ByteBuffer map();

    void unmap();

    void free();

    VkDevice getDevice();

    default void bindToImage(long img) {
        vkAssert(VK10.vkBindImageMemory(getDevice(), img, getHandle(), getOffset()));
    }

    default void bindToBuffer(long buf) {
        vkAssert(VK10.vkBindBufferMemory(getDevice(), buf, getHandle(), getOffset()));
    }
}
