package com.longlinkislong.jvk.memory;

import com.longlinkislong.jvk.Util;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkMemoryAllocateInfo;
import org.lwjgl.vulkan.VkMemoryRequirements;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static com.longlinkislong.jvk.Util.vkAssert;

final class SlabMemoryAllocator implements MemoryAllocator {
    static final class SlabSizeInfo {
        final long size;
        final int count;

        SlabSizeInfo(final long size, final int count) {
            this.size = size;
            this.count = count;
        }
    }

    private static final class SlabList {
        private final long size;
        private final List<MemorySlab> slabs;

        private SlabList(final long size, final List<MemorySlab> slabs) {
            this.size = size;
            this.slabs = List.copyOf(slabs);
        }
    }

    private final Object lock = new Object();
    private final WeakReference<VkDevice> device;
    private final long size;
    private final long handle;
    private final List<SlabList> lists;
    private final int typeIndex;
    private ByteBuffer address;
    private int mapCount;

    SlabMemoryAllocator(final VkDevice device, final int index, final List<SlabSizeInfo> sizeInfos) {
        this.device = new WeakReference<>(device);
        this.size = sizeInfos.stream()
                .mapToLong(sizeInfo -> sizeInfo.size * sizeInfo.count)
                .sum();

        this.typeIndex = index;

        try (var mem = MemoryStack.stackPush()) {
            final var info = VkMemoryAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                    .memoryTypeIndex(index)
                    .allocationSize(this.size);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkAllocateMemory(device, info, null, pHandle));

            this.handle = pHandle.get();
        }

        this.lists = new ArrayList<>();

        long offset = 0L;
        for (var sizeInfo : sizeInfos) {
            final var baseOffset = offset;
            final var slabs = LongStream.range(0, sizeInfo.count)
                    .map(idx -> baseOffset + sizeInfo.size * idx)
                    .mapToObj(off -> new MemorySlab(off, sizeInfo.size))
                    .collect(Collectors.toList());

            this.lists.add(new SlabList(sizeInfo.size, slabs));
            offset += sizeInfo.count * sizeInfo.size;
        }
    }

    @Override
    public boolean isEmpty() {
        synchronized (lock) {
            for (var slabList : this.lists) {
                for (var slab : slabList.slabs) {
                    if (slab.type != MemoryType.FREE) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    @Override
    public int getTypeIndex() {
        return this.typeIndex;
    }

    @Override
    public MemoryBlock malloc(MemoryType type, VkMemoryRequirements pMemReqs) {
        synchronized (lock) {
            final var slabListIt = this.lists.listIterator();
            final var requiredSize = pMemReqs.size();
            final var alignment = pMemReqs.alignment();

            while (slabListIt.hasNext()) {
                final var slabList = slabListIt.next();

                if (slabList.size < requiredSize) {
                    continue;
                }

                for (MemorySlab slab : slabList.slabs) {
                    if (slab.type != MemoryType.FREE) {
                        continue;
                    }

                    slab.align(alignment);

                    if (slab.getSize() >= requiredSize) {
                        slab.type = type;

                        return slab;
                    } else {
                        break;
                    }
                }
            }
        }

        throw new OutOfMemoryError();
    }

    private ByteBuffer map() {
        synchronized (lock) {
            if (null == this.address) {
                try (var mem = MemoryStack.stackPush()) {
                    final var ppData = mem.callocPointer(1);

                    vkAssert(VK10.vkMapMemory(this.getDevice(), this.handle, 0L, this.size, 0, ppData));

                    this.address = ppData.getByteBuffer(0, (int) this.size);
                }
            }

            this.mapCount++;

            return this.address;
        }
    }

    private void unmap() {
        synchronized (lock) {
            if (0 == --this.mapCount) {
                this.address = null;
                VK10.vkUnmapMemory(this.getDevice(), this.handle);
            }
        }
    }

    @Override
    public void free() {
        synchronized (lock) {
            this.lists.clear();
            VK10.vkFreeMemory(this.getDevice(), this.handle, null);
        }
    }

    @Override
    public VkDevice getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    private final class MemorySlab implements MemoryBlock {
        private final long offset;
        private final long size;
        private long alignedOffset;
        private MemoryType type;

        private MemorySlab(final long offset, final long size) {
            this.offset = offset;
            this.alignedOffset = offset;
            this.size = size;
            this.type = MemoryType.FREE;
        }

        private void align(final long alignment) {
            this.alignedOffset = Util.alignUp(this.offset, alignment);
        }

        @Override
        public long getHandle() {
            return SlabMemoryAllocator.this.handle;
        }

        @Override
        public long getOffset() {
            return this.alignedOffset;
        }

        @Override
        public long getSize() {
            return this.offset + this.size - this.alignedOffset;
        }

        @Override
        public ByteBuffer map() {
            final var superBlock = SlabMemoryAllocator.this.map();

            return MemoryUtil.memSlice(superBlock, (int) getOffset(), (int) getSize());
        }

        @Override
        public void unmap() {
            SlabMemoryAllocator.this.unmap();
        }

        @Override
        public void free() {
            synchronized (lock) {
                this.alignedOffset = this.offset;
                this.type = MemoryType.FREE;
            }
        }

        @Override
        public VkDevice getDevice() {
            return SlabMemoryAllocator.this.getDevice();
        }

        @Override
        public String toString() {
            return "MemorySlabType: " + this.type +
                    " PTR: +" + this.offset +
                    " Size: " + this.size;
        }
    }
}
