package com.longlinkislong.jvk.memory;

import com.longlinkislong.jvk.Util;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkMemoryAllocateInfo;
import org.lwjgl.vulkan.VkMemoryRequirements;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.longlinkislong.jvk.Util.vkAssert;

final class UniqueMemoryAllocator implements MemoryAllocator {
    private final WeakReference<VkDevice> device;
    private final int typeIndex;
    private final Set<UniqueMemoryBlock> allAllocations = new HashSet<>();
    private final Object lock = new Object();

    UniqueMemoryAllocator(final VkDevice device, final int typeIndex) {
        this.device = new WeakReference<>(device);
        this.typeIndex = typeIndex;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int getTypeIndex() {
        return this.typeIndex;
    }

    @Override
    public MemoryBlock malloc(MemoryType type, VkMemoryRequirements pMemReqs) {

        final long alignedSize = Util.alignUp(pMemReqs.size(), pMemReqs.alignment());

        return new UniqueMemoryBlock(type, alignedSize);
    }

    @Override
    public void free() {
        synchronized (lock) {
            this.allAllocations.forEach(alloc -> VK10.vkFreeMemory(this.getDevice(), alloc.handle, null));
            this.allAllocations.clear();
        }
    }

    @Override
    public VkDevice getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    private final class UniqueMemoryBlock implements MemoryBlock {
        private final long handle;
        private final long size;
        private final MemoryType type;

        private UniqueMemoryBlock(final MemoryType type, final long size) {
            this.size = size;
            this.type = type;

            try (var mem = MemoryStack.stackPush()) {
                final var pHandle = mem.callocLong(1);
                final var pAllocateInfo = VkMemoryAllocateInfo.callocStack(mem)
                        .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                        .memoryTypeIndex(typeIndex)
                        .allocationSize(size);

                vkAssert(VK10.vkAllocateMemory(getDevice(), pAllocateInfo, null, pHandle));

                this.handle = pHandle.get();
            }

            synchronized (lock) {
                allAllocations.add(this);
            }
        }

        @Override
        public String toString() {
            return String.format("ContextImageType: %s Offset: +0x%x Size: 0x%x", this.type, this.getOffset(), this.getSize());
        }

        @Override
        public VkDevice getDevice() {
            return UniqueMemoryAllocator.this.getDevice();
        }

        @Override
        public long getHandle() {
            return this.handle;
        }

        @Override
        public long getOffset() {
            return 0L;
        }

        @Override
        public long getSize() {
            return this.size;
        }

        @Override
        public ByteBuffer map() {
            try (var mem = MemoryStack.stackPush()) {
                final var ppData = mem.callocPointer(1);

                vkAssert(VK10.vkMapMemory(getDevice(), this.handle, 0, this.size, 0, ppData));

                return ppData.getByteBuffer(0, (int) this.size);
            }
        }

        @Override
        public void unmap() {
            VK10.vkUnmapMemory(getDevice(), this.handle);
        }

        @Override
        public void free() {
            synchronized (lock) {
                if (allAllocations.remove(this)) {
                    VK10.vkFreeMemory(getDevice(), this.handle, null);
                }
            }
        }
    }
}
