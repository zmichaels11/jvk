package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum Filter {
    NEAREST(VK10.VK_FILTER_NEAREST),
    LINEAR(VK10.VK_FILTER_LINEAR);

    final int value;

    Filter(final int value) {
        this.value = value;
    }
}
