package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkClearValue;

public final class ClearValue {
    public final Color color;
    public final DepthStencil depthStencil;

    public ClearValue(final Color color, final DepthStencil depthStencil) {
        this.color = color;
        this.depthStencil = depthStencil;
    }

    public ClearValue(final Color color) {
        this(color, null);
    }

    public ClearValue(final DepthStencil depthStencil) {
        this(null, depthStencil);
    }

    VkClearValue deserialize(final VkClearValue vkobj) {
        if (null != color) {
            vkobj.color()
                    .float32(0, color.red)
                    .float32(1, color.green)
                    .float32(2, color.blue)
                    .float32(3, color.alpha);
        } else if (null != depthStencil) {
            vkobj.depthStencil()
                    .depth(depthStencil.depth)
                    .stencil(depthStencil.stencil);
        }

        return vkobj;
    }
}
