package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum StoreOp {
    DONT_CARE(VK10.VK_ATTACHMENT_STORE_OP_DONT_CARE),
    STORE(VK10.VK_ATTACHMENT_STORE_OP_STORE);

    final int value;

    StoreOp(final int value) {
        this.value = value;
    }
}
