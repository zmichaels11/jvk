package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineTessellationStateCreateInfo;

public final class PipelineTessellationStateCreateInfo {
    public final int flags;
    public final int patchControlPoints;

    public PipelineTessellationStateCreateInfo(final int flags, final int patchControlPoints) {
        this.flags = flags;
        this.patchControlPoints = patchControlPoints;
    }

    VkPipelineTessellationStateCreateInfo deserialize(final VkPipelineTessellationStateCreateInfo vkobj) {
        return vkobj.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO)
                .pNext(VK10.VK_NULL_HANDLE)
                .flags(flags)
                .patchControlPoints(patchControlPoints);
    }
}
