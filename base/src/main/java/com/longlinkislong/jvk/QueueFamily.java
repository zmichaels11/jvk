package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.KHRSurface;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkQueueFamilyProperties;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class QueueFamily {
    public final int timestampValidBits;
    public final Extent3D minImageTransferGranularity;
    public final List<Queue> queues;
    public final Set<QueueFlag> flags;
    public final int index;
    private final WeakReference<Device> device;
    private final ThreadLocal<CommandPool> commandPools = ThreadLocal.withInitial(() -> new CommandPool(this, Set.of(CommandPoolCreateFlag.CREATE_RESET_COMMAND_BUFFER)));    private final AtomicInteger queueSelect = new AtomicInteger(0);

    QueueFamily(final Device device, final int queueFamilyIndex, final VkQueueFamilyProperties.Buffer ppvkProperties) {
        this.device = new WeakReference<>(device);

        final var pvkProperties = ppvkProperties.get(queueFamilyIndex);

        this.index = queueFamilyIndex;
        this.timestampValidBits = pvkProperties.timestampValidBits();
        this.minImageTransferGranularity = Extent3D.serialize(pvkProperties.minImageTransferGranularity());

        this.queues = IntStream.range(0, pvkProperties.queueCount())
                .mapToObj(queueIndex -> new Queue(device, queueFamilyIndex, queueIndex))
                .collect(Collectors.toUnmodifiableList());

        this.flags = QueueFlag.toEnumSet(pvkProperties.queueFlags());
    }

    public Queue getQueue(final int queueIndex) {
        return this.queues.get(queueIndex);
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public CommandPool getCurrentCommandPool() {
        return this.commandPools.get();
    }

    public CommandPool createCommandPool(final Set<CommandPoolCreateFlag> flags) {
        return new CommandPool(this, flags);
    }

    public void detach() {
        this.commandPools.get().free();
        this.commandPools.remove();
    }

    public boolean canPresent(final long surface) {
        try (var mem = MemoryStack.stackPush()) {
            final var pSupported = mem.callocInt(1);

            KHRSurface.vkGetPhysicalDeviceSurfaceSupportKHR(this.getDevice().handle.getPhysicalDevice(), this.index, surface, pSupported);

            return VK10.VK_TRUE == pSupported.get();
        }
    }

    static int indexOf(final QueueFamily qf) {
        return (null == qf) ? VK10.VK_QUEUE_FAMILY_IGNORED : qf.index;
    }
}
