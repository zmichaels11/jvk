package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

import java.util.Set;

public enum ImageUsageFlag {
    COLOR_ATTACHMENT(VK10.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT),
    SAMPLED(VK10.VK_IMAGE_USAGE_SAMPLED_BIT),
    DEPTH_STENCIL_ATTACHMENT(VK10.VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT),
    TRANSFER_DST(VK10.VK_IMAGE_USAGE_TRANSFER_DST_BIT),
    TRANSFER_SRC(VK10.VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
    STORAGE(VK10.VK_IMAGE_USAGE_STORAGE_BIT),
    INPUT_ATTACHMENT(VK10.VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT),
    TRANSIENT_ATTACHMENT(VK10.VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT);

    final int value;

    ImageUsageFlag(final int value) {
        this.value = value;
    }

    static int toBitfield(final Set<ImageUsageFlag> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .sum();
    }
}
