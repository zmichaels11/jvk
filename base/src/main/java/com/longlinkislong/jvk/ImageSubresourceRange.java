package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkImageSubresourceRange;

import java.util.Set;

public final class ImageSubresourceRange {
    public final Set<AspectFlag> aspectMask;
    public final int baseMipLevel;
    public final int levelCount;
    public final int baseArrayLayer;
    public final int layerCount;

    public ImageSubresourceRange(
            final Set<AspectFlag> aspectMask,
            final int baseMipLevel, final int levelCount,
            final int baseArrayLayer, final int layerCount) {

        this.aspectMask = Set.copyOf(aspectMask);
        this.baseMipLevel = baseMipLevel;
        this.levelCount = levelCount;
        this.baseArrayLayer = baseArrayLayer;
        this.layerCount = layerCount;
    }

    public ImageSubresourceRange() {
        this(Set.of(), 0, 0, 0, 0);
    }

    public ImageSubresourceRange withAspectMask(final Set<AspectFlag> aspectMask) {
        return new ImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }

    public ImageSubresourceRange withAspectMask(final AspectFlag... aspectMask) {
        return withAspectMask(Set.of(aspectMask));
    }

    public ImageSubresourceRange withBaseMipLevel(final int baseMipLevel) {
        return new ImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }

    public ImageSubresourceRange withLevelCount(final int levelCount) {
        return new ImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }

    public ImageSubresourceRange withMipRange(final int baseMipLevel, final int levelCount) {
        return new ImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }

    public ImageSubresourceRange withBaseArrayLayer(final int baseArrayLayer) {
        return new ImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }

    public ImageSubresourceRange withLayerCount(final int layerCount) {
        return new ImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }

    public ImageSubresourceRange withArrayLayerRange(final int baseArrayLayer, final int layerCount) {
        return new ImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }

    VkImageSubresourceRange deserialize(final VkImageSubresourceRange vkobj) {
        return vkobj.set(AspectFlag.toBitfield(this.aspectMask), this.baseMipLevel, this.levelCount, this.baseArrayLayer, this.layerCount);
    }
}
