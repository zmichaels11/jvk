package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum PipelineBindpoint {
    GRAPHICS(VK10.VK_PIPELINE_BIND_POINT_GRAPHICS),
    COMPUTE(VK10.VK_PIPELINE_BIND_POINT_COMPUTE);

    final int value;

    PipelineBindpoint(final int value) {
        this.value = value;
    }
}
