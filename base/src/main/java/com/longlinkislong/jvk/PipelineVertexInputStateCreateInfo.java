package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineVertexInputStateCreateInfo;

import java.util.List;

public final class PipelineVertexInputStateCreateInfo {
    public final int flags;
    public final List<VertexInputBindingDescription> vertexBindingDescriptions;
    public final List<VertexInputAttributeDescription> vertexAttributeDescriptions;

    public PipelineVertexInputStateCreateInfo(final int flags, final List<VertexInputBindingDescription> vertexBindingDescriptions, final List<VertexInputAttributeDescription> vertexAttributeDescriptions) {
        this.flags = flags;
        this.vertexBindingDescriptions = List.copyOf(vertexBindingDescriptions);
        this.vertexAttributeDescriptions = List.copyOf(vertexAttributeDescriptions);
    }

    public PipelineVertexInputStateCreateInfo() {
        this(0, List.of(), List.of());
    }

    public PipelineVertexInputStateCreateInfo withFlags(final int flags) {
        return new PipelineVertexInputStateCreateInfo(flags, vertexBindingDescriptions, vertexAttributeDescriptions);
    }

    public PipelineVertexInputStateCreateInfo withVertexBindingDescriptions(final List<VertexInputBindingDescription> vertexBindingDescriptions) {
        return new PipelineVertexInputStateCreateInfo(flags, vertexBindingDescriptions, vertexAttributeDescriptions);
    }

    public PipelineVertexInputStateCreateInfo withVertexBindingDescriptions(final VertexInputBindingDescription... vertexBindingDescriptions) {
        return withVertexBindingDescriptions(List.of(vertexBindingDescriptions));
    }

    public PipelineVertexInputStateCreateInfo withVertexAttributeDescriptions(final List<VertexInputAttributeDescription> vertexAttributeDescriptions) {
        return new PipelineVertexInputStateCreateInfo(flags, vertexBindingDescriptions, vertexAttributeDescriptions);
    }

    public PipelineVertexInputStateCreateInfo withVertexAttributeDescriptions(final VertexInputAttributeDescription... vertexAttributeDescriptions) {
        return withVertexAttributeDescriptions(List.of(vertexAttributeDescriptions));
    }

    VkPipelineVertexInputStateCreateInfo deserialize(final VkPipelineVertexInputStateCreateInfo vkobj, final MemoryStack mem) {
        return vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO)
                .pNext(VK10.VK_NULL_HANDLE)
                .flags(flags)
                .pVertexBindingDescriptions(VertexInputBindingDescription.deserialize(vertexBindingDescriptions, mem))
                .pVertexAttributeDescriptions(VertexInputAttributeDescription.deserialize(vertexAttributeDescriptions, mem));
    }
}
