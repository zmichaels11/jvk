package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkPushConstantRange;

import java.util.Set;

public final class PushConstantRange {
    public final Set<ShaderStage> stages;
    public final int offset;
    public final int size;

    public PushConstantRange(final Set<ShaderStage> stages, final int offset, final int size) {
        this.stages = Set.copyOf(stages);
        this.offset = offset;
        this.size = size;
    }

    public PushConstantRange() {
        this(Set.of(), 0, 0);
    }

    public PushConstantRange withStages(final Set<ShaderStage> stages) {
        return new PushConstantRange(stages, offset, size);
    }

    public PushConstantRange withStages(final ShaderStage... stages) {
        return withStages(Set.of(stages));
    }

    public PushConstantRange withOffset(final int offset) {
        return new PushConstantRange(stages, offset, size);
    }

    public PushConstantRange withSize(final int size) {
        return new PushConstantRange(stages, offset, size);
    }

    VkPushConstantRange deserialize(final VkPushConstantRange vkobj) {
        return vkobj.set(ShaderStage.toBitfield(stages), offset, size);
    }
}
