package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public final class BufferCopy {
    public final long srcOffset;
    public final long dstOffset;
    public final long size;

    public BufferCopy(final long srcOffset, final long dstOffset, final long size) {
        this.srcOffset = srcOffset;
        this.dstOffset = dstOffset;
        this.size = size;
    }

    public BufferCopy() {
        this(0L, 0L, VK10.VK_WHOLE_SIZE);
    }

    public BufferCopy withSrcOffet(final long srcOffset) {
        return new BufferCopy(srcOffset, dstOffset, size);
    }

    public BufferCopy withDstOffset(final long dstOffset) {
        return new BufferCopy(srcOffset, dstOffset, size);
    }

    public BufferCopy withSize(final long size) {
        return new BufferCopy(srcOffset, dstOffset, size);
    }
}
