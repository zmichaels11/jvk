package com.longlinkislong.jvk;

import org.lwjgl.PointerBuffer;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public final class BufferStreams {
    private BufferStreams() {}

    public static LongStream of(final LongBuffer buffer) {
        if (null == buffer) {
            return LongStream.empty();
        }

        return IntStream.range(buffer.position(), buffer.limit())
                .mapToLong(buffer::get);
    }

    public static IntStream of(final IntBuffer buffer) {
        if (null == buffer) {
            return IntStream.empty();
        }

        return IntStream.range(buffer.position(), buffer.limit())
                .map(buffer::get);
    }

    public static Stream<Float> of(final FloatBuffer buffer) {
        if (null == buffer) {
            return Stream.empty();
        }

        return IntStream.range(buffer.position(), buffer.limit())
                .mapToObj(buffer::get);
    }

    public static LongStream of(final PointerBuffer buffer) {
        if (null == buffer) {
            return LongStream.empty();
        }

        return IntStream.range(buffer.position(), buffer.limit())
                .mapToLong(buffer::get);
    }
}
