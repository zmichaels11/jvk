package com.longlinkislong.jvk;

import com.longlinkislong.jvk.memory.MemoryBlock;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkImageCreateInfo;
import org.lwjgl.vulkan.VkMemoryRequirements;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class Image {
    public static final class CreateInfo {
        public final int flags;
        public final ImageType imageType;
        public final Format format;
        public final Extent3D extent;
        public final int mipLevels;
        public final int arrayLayers;
        public final int samples;
        public final ImageTiling tiling;
        public final Set<ImageUsageFlag> usage;
        public final SharingMode sharingMode;
        public final List<QueueFamily> queueFamilies;
        public final ImageLayout initialLayout;

        public CreateInfo(
                final int flags,
                final ImageType imageType,
                final Format format,
                final Extent3D extent,
                final int mipLevels, final int arrayLayers,
                final int samples,
                final ImageTiling tiling,
                final Set<ImageUsageFlag> usage,
                final SharingMode sharingMode,
                final List<QueueFamily> queueFamilies,
                final ImageLayout initialLayout) {

            this.flags = flags;
            this.imageType = imageType;
            this.format = format;
            this.extent = extent;
            this.mipLevels = mipLevels;
            this.arrayLayers = arrayLayers;
            this.samples = samples;
            this.tiling = tiling;
            this.usage = Set.copyOf(usage);
            this.sharingMode = sharingMode;
            this.queueFamilies = List.copyOf(queueFamilies);
            this.initialLayout = initialLayout;
        }

        public CreateInfo() {
            this(0, null, null, null, 1, 1, VK10.VK_SAMPLE_COUNT_1_BIT, ImageTiling.OPTIMAL, Set.of(), SharingMode.EXCLUSIVE, List.of(), ImageLayout.UNDEFINED);
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withImageType(final ImageType imageType) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withFormat(final Format format) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withExtent(final Extent3D extent) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withExtent(final int width) {
            return withExtent(new Extent3D(width, 1, 1));
        }

        public CreateInfo withExtent(final int width, final int height) {
            return withExtent(new Extent3D(width, height, 1));
        }

        public CreateInfo withExtent(final int width, final int height, final int depth) {
            return withExtent(new Extent3D(width, height, depth));
        }

        public CreateInfo withMipLevels(final int mipLevels) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withArrayLayers(final int arrayLayers) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withSamples(final int samples) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withTiling(final ImageTiling tiling) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withUsage(final Set<ImageUsageFlag> usage) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withUsage(final ImageUsageFlag... usage) {
            return withUsage(Set.of(usage));
        }

        public CreateInfo withSharingMode(final SharingMode sharingMode) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withQueueFamilies(final List<QueueFamily> queueFamilies) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }

        public CreateInfo withQueueFamilies(final QueueFamily... queueFamilies) {
            return withQueueFamilies(List.of(queueFamilies));
        }

        public CreateInfo withInitialLayout(final ImageLayout initialLayout) {
            return new CreateInfo(flags, imageType, format, extent, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
        }
    }

    private final WeakReference<Device> device;
    public final long handle;
    public final CreateInfo info;
    private final MemoryBlock memory;
    public Object userData = null;

    Image(final Device device, final CreateInfo info, final long handle) {
        this.device = new WeakReference<>(device);
        this.info = info;
        this.handle = handle;
        this.memory = null;
    }

    Image(final Device device, final CreateInfo info, final int memoryProperties) {
        this.device = new WeakReference<>(device);
        this.info = info;

        try (var mem = MemoryStack.stackPush()) {
            final IntBuffer pQueueFamilyIndicies;

            if (info.queueFamilies.isEmpty()) {
                pQueueFamilyIndicies = null;
            } else {
                pQueueFamilyIndicies = info.queueFamilies.stream()
                        .map(queueFamily -> queueFamily.index)
                        .collect(BufferCollectors.toIntBuffer(mem));
            }


            final var pvkImageCI = VkImageCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO)
                    .flags(info.flags)
                    .imageType(info.imageType.value)
                    .format(info.format.value)
                    .mipLevels(info.mipLevels)
                    .arrayLayers(info.arrayLayers)
                    .samples(info.samples)
                    .tiling(info.tiling.value)
                    .usage(ImageUsageFlag.toBitfield(info.usage))
                    .sharingMode(info.sharingMode.value)
                    .pQueueFamilyIndices(pQueueFamilyIndicies)
                    .initialLayout(info.initialLayout.value);

            info.extent.deserialize(pvkImageCI.extent());

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateImage(device.handle, pvkImageCI, null, pHandle));

            this.handle = pHandle.get();

            final var pvkMemoryRequirements = VkMemoryRequirements.callocStack(mem);

            VK10.vkGetImageMemoryRequirements(device.handle, this.handle, pvkMemoryRequirements);

            this.memory = device.memoryManager.allocateImageMemory(pvkMemoryRequirements, memoryProperties);
            this.memory.bindToImage(this.handle);
        }
    }

    public ImageSubresourceRange getFullRange() {
        return new ImageSubresourceRange(this.info.format.aspect, 0, this.info.mipLevels, 0, this.info.arrayLayers);
    }

    public ImageSubresourceLayers getFullLayerRange(final int mipLevel) {
        return new ImageSubresourceLayers(this.info.format.aspect, mipLevel, 0, this.info.arrayLayers);
    }

    public ImageSubresourceLayers getSubresourceLayers(final int mipLevel, final int baseArrayLayer, final int layerCount) {
        return new ImageSubresourceLayers(this.info.format.aspect, mipLevel, baseArrayLayer, layerCount);
    }

    public ImageSubresourceRange getSubresourceRange(final int baseMipLevel, final int levelCount, final int baseArrayLayer, final int layerCount) {
        return new ImageSubresourceRange(this.info.format.aspect, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public Optional<MemoryBlock> getMemory() {
        return Optional.ofNullable(this.memory);
    }

    public Optional<ByteBuffer> map() {
        return this.getMemory().map(MemoryBlock::map);
    }

    public void unmap() {
        this.getMemory().ifPresent(MemoryBlock::unmap);
    }

    public void withMap(final Consumer<Optional<ByteBuffer>> onMap) {
        onMap.accept(this.map());
        this.unmap();
    }

    public void free() {
        VK10.vkDestroyImage(this.getDevice().handle, this.handle, null);

        if (null != this.memory) {
            this.memory.free();
        }
    }

    public boolean isArrayed() {
        return this.info.arrayLayers > 1;
    }

    public boolean hasMipmap() {
        return this.info.mipLevels > 1;
    }

    public ImageView.CreateInfo getFullViewCreateInfo() {
        final ImageViewType viewType;

        if (this.isArrayed()) {
            switch (this.info.imageType) {
                case IMAGE_1D:
                    viewType = ImageViewType.VIEW_1D_ARRAY;
                    break;
                case IMAGE_2D:
                    viewType = ImageViewType.VIEW_2D_ARRAY;
                    break;
                default:
                    throw new UnsupportedOperationException("Only 1D and 2D images can be arrayed!");
            }
        } else {
            switch (this.info.imageType) {
                case IMAGE_1D:
                    viewType = ImageViewType.VIEW_1D;
                    break;
                case IMAGE_2D:
                    viewType = ImageViewType.VIEW_2D;
                    break;
                case IMAGE_3D:
                    viewType = ImageViewType.VIEW_3D;
                    break;
                default:
                    throw new UnsupportedOperationException("ImageView.CreateInfo can only be derived from 1D, 2D, or 3D images!");
            }
        }

        return new ImageView.CreateInfo(viewType, this.info.format, this.getFullRange());
    }
}
