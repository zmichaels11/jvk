package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineInputAssemblyStateCreateInfo;

public final class PipelineInputAssemblyStateCreateInfo {
    public final int flags;
    public final PrimitiveTopology topology;
    public final boolean primitiveRestartEnable;

    public PipelineInputAssemblyStateCreateInfo(final int flags, final PrimitiveTopology topology, final boolean primitiveRestartEnable) {
        this.flags = flags;
        this.topology = topology;
        this.primitiveRestartEnable = primitiveRestartEnable;
    }

    public PipelineInputAssemblyStateCreateInfo() {
        this(0, PrimitiveTopology.TRIANGLE_LIST, false);
    }

    public PipelineInputAssemblyStateCreateInfo withFlags(final int flags) {
        return new PipelineInputAssemblyStateCreateInfo(flags, topology, primitiveRestartEnable);
    }

    public PipelineInputAssemblyStateCreateInfo withTopology(final PrimitiveTopology topology) {
        return new PipelineInputAssemblyStateCreateInfo(flags, topology, primitiveRestartEnable);
    }

    public PipelineInputAssemblyStateCreateInfo withPrimitiveRestartEnable(final boolean primitiveRestartEnable) {
        return new PipelineInputAssemblyStateCreateInfo(flags, topology, primitiveRestartEnable);
    }

    VkPipelineInputAssemblyStateCreateInfo deserialize(final VkPipelineInputAssemblyStateCreateInfo vkobj) {
        return vkobj.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO)
                .pNext(VK10.VK_NULL_HANDLE)
                .flags(flags)
                .topology(topology.value)
                .primitiveRestartEnable(primitiveRestartEnable);
    }
}
