package com.longlinkislong.jvk;

import java.util.List;

public final class RenderPassBeginInfo {
    public final Framebuffer framebuffer;
    public final RenderPass renderPass;
    public final SubpassContents subpassContents;
    public final Rect2D renderArea;
    public final List<ClearValue> clearValues;

    public RenderPassBeginInfo(final Framebuffer framebuffer, final RenderPass renderPass, final SubpassContents subpassContents, final Rect2D renderArea, final List<ClearValue> clearValues) {
        this.framebuffer = framebuffer;
        this.renderPass = renderPass;
        this.subpassContents = subpassContents;
        this.clearValues = List.copyOf(clearValues);
        this.renderArea = renderArea;
    }

    public RenderPassBeginInfo() {
        this(null, null, SubpassContents.INLINE, null, List.of());
    }

    public RenderPassBeginInfo withFramebuffer(final Framebuffer framebuffer) {
        return new RenderPassBeginInfo(framebuffer, renderPass, subpassContents, renderArea, clearValues);
    }

    public RenderPassBeginInfo withRenderPass(final RenderPass renderPass) {
        return new RenderPassBeginInfo(framebuffer, renderPass, subpassContents, renderArea, clearValues);
    }

    public RenderPassBeginInfo withSubpassContents(final SubpassContents subpassContents) {
        return new RenderPassBeginInfo(framebuffer, renderPass, subpassContents, renderArea, clearValues);
    }

    public RenderPassBeginInfo withRenderArea(final Rect2D renderArea) {
        return new RenderPassBeginInfo(framebuffer, renderPass, subpassContents, renderArea, clearValues);
    }

    public RenderPassBeginInfo withClearValues(final List<ClearValue> clearValues) {
        return new RenderPassBeginInfo(framebuffer, renderPass, subpassContents, renderArea, clearValues);
    }

    public RenderPassBeginInfo withClearValues(final ClearValue... clearValues) {
        return withClearValues(List.of(clearValues));
    }
}
