package com.longlinkislong.jvk;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.lwjgl.vulkan.VK11.*;

public enum QueueFlag {
    GRAPHICS(VK_QUEUE_GRAPHICS_BIT),
    COMPUTE(VK_QUEUE_COMPUTE_BIT),
    TRANSFER(VK_QUEUE_TRANSFER_BIT),
    SPARSE_BINDING(VK_QUEUE_SPARSE_BINDING_BIT),
    PROTECTED(VK_QUEUE_PROTECTED_BIT);

    final int value;

    QueueFlag(final int value) {
        this.value = value;
    }

    static Set<QueueFlag> toEnumSet(final int bitfield) {
        return Arrays.stream(values())
                .filter(flag -> 0 != (bitfield & flag.value))
                .collect(Collectors.toUnmodifiableSet());
    }
}
