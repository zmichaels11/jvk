package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.IntStream;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class CommandBuffer {
    public static final class InheritanceInfo {
        public final RenderPass renderPass;
        public final int subpass;
        public final Framebuffer framebuffer;
        public final boolean occlusionQueryEnable;
        public final int queryFlags;
        public final int pipelineStatistics;

        public InheritanceInfo(
                final RenderPass renderPass,
                final int subpass,
                final Framebuffer framebuffer,
                final boolean occlusionQueryEnable,
                final int queryFlags, final int pipelineStatistics) {

            this.renderPass = renderPass;
            this.subpass = subpass;
            this.framebuffer = framebuffer;
            this.occlusionQueryEnable = occlusionQueryEnable;
            this.queryFlags = queryFlags;
            this.pipelineStatistics = pipelineStatistics;
        }
    }

    public final CommandBufferLevel level;
    public final VkCommandBuffer handle;
    private final WeakReference<CommandPool> pool;

    CommandBuffer(final CommandPool pool, final CommandBufferLevel level, final long handle) {
        this.handle = new VkCommandBuffer(handle, pool.getDevice().handle);
        this.pool = new WeakReference<>(pool);
        this.level = level;
    }

    public void reset(final int flags) {
        VK10.vkResetCommandBuffer(this.handle, flags);
    }

    public void free() {
        VK10.vkFreeCommandBuffers(this.handle.getDevice(), this.getCommandPool().handle, this.handle);
    }

    public CommandPool getCommandPool() {
        return Objects.requireNonNull(this.pool.get(), "CommandPool was lost!");
    }

    public CommandBuffer begin(final Set<CommandBufferUsageFlag> flags) {
        return begin(flags, null);
    }

    public CommandBuffer begin(final Set<CommandBufferUsageFlag> flags, final InheritanceInfo inheritanceInfo) {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkCommandBufferBI = VkCommandBufferBeginInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
                    .flags(CommandBufferUsageFlag.toBitfield(flags));

            if (null != inheritanceInfo) {
                final var pvkCommandBufferII = VkCommandBufferInheritanceInfo.callocStack(mem)
                        .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO)
                        .renderPass(inheritanceInfo.renderPass == null ? VK10.VK_NULL_HANDLE : inheritanceInfo.renderPass.handle)
                        .subpass(inheritanceInfo.subpass)
                        .framebuffer(inheritanceInfo.framebuffer == null ? VK10.VK_NULL_HANDLE : inheritanceInfo.framebuffer.handle)
                        .occlusionQueryEnable(inheritanceInfo.occlusionQueryEnable)
                        .queryFlags(inheritanceInfo.queryFlags)
                        .pipelineStatistics(inheritanceInfo.pipelineStatistics);

                pvkCommandBufferBI.pInheritanceInfo(pvkCommandBufferII);
            }

            vkAssert(VK10.vkBeginCommandBuffer(this.handle,  pvkCommandBufferBI));
        }

        return this;
    }

    public CommandBuffer end() {
        vkAssert(VK10.vkEndCommandBuffer(this.handle));

        return this;
    }

    public CommandBuffer executeCommands(final List<CommandBuffer> commands) {
        try (var mem = MemoryStack.stackPush()) {
            final var pCommandBuffers = commands.stream()
                    .mapToLong(command -> command.handle.address())
                    .boxed()
                    .collect(BufferCollectors.toPointerBuffer(mem));

            VK10.vkCmdExecuteCommands(this.handle, pCommandBuffers);
        }

        return this;
    }

    public CommandBuffer beginRenderPass(final RenderPassBeginInfo beginInfo) {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkBeginRenderPass = VkRenderPassBeginInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO)
                    .renderPass(beginInfo.renderPass.handle)
                    .framebuffer(beginInfo.framebuffer.handle);

            beginInfo.renderArea.deserialize(pvkBeginRenderPass.renderArea());

            final var ppClearValues = VkClearValue.callocStack(beginInfo.clearValues.size(), mem);

            IntStream.range(0, beginInfo.clearValues.size()).forEach(i -> beginInfo.clearValues.get(i).deserialize(ppClearValues.get(i)));

            pvkBeginRenderPass.pClearValues(ppClearValues);

            VK10.vkCmdBeginRenderPass(this.handle, pvkBeginRenderPass, beginInfo.subpassContents.value);
        }

        return this;
    }

    public CommandBuffer nextSubpass(final SubpassContents contents) {
        VK10.vkCmdNextSubpass(this.handle, contents.value);

        return this;
    }

    public CommandBuffer endRenderPass() {
        VK10.vkCmdEndRenderPass(this.handle);

        return this;
    }

    public CommandBuffer setViewport(float x, float y, float width, float height) {
        try (var mem = MemoryStack.stackPush()) {
            final var pViewports = VkViewport.callocStack(1, mem);

            pViewports.get(0).set(x, y, width, height, 0.0F, 1.0F);

            VK10.vkCmdSetViewport(this.handle, 0, pViewports);
        }

        return this;
    }

    public CommandBuffer setScissor(final int x, final int y, final int width, final int height) {
        try (var mem = MemoryStack.stackPush()) {
            final var pScissors = VkRect2D.callocStack(1, mem);

            pScissors.get(0).offset().set(x, y);
            pScissors.get(0).extent().set(width, height);

            VK10.vkCmdSetScissor(this.handle, 0, pScissors);
        }

        return this;
    }

    public CommandBuffer bindVertexBuffers(final int firstBinding, final List<Buffer> buffers, final List<Long> offsets) {
        try (var mem = MemoryStack.stackPush()) {
            final var pBuffers = buffers.stream()
                    .mapToLong(buffer -> buffer.handle)
                    .boxed()
                    .collect(BufferCollectors.toLongBuffer(mem));

            final var pOffsets = offsets.stream()
                    .collect(BufferCollectors.toLongBuffer(mem));

            VK10.vkCmdBindVertexBuffers(this.handle, firstBinding, pBuffers, pOffsets);
        }

        return this;
    }

    public CommandBuffer bindVertexBuffer(final int binding, final Buffer buffer, final long offset) {
        try (var mem = MemoryStack.stackPush()) {
            final var pBuffer = mem.longs(buffer.handle);
            final var pOffset = mem.longs(offset);

            VK10.vkCmdBindVertexBuffers(this.handle, binding, pBuffer, pOffset);
        }

        return this;
    }

    public CommandBuffer bindIndexBuffer(final Buffer buffer, final long offset, final IndexType indexType) {
        VK10.vkCmdBindIndexBuffer(this.handle, buffer.handle, offset, indexType.value);

        return this;
    }

    public CommandBuffer dispatch(final int groupsX) {
        return this.dispatch(groupsX, 1, 1);
    }

    public CommandBuffer dispatch(final int groupsX, final int groupsY) {
        return this.dispatch(groupsX, groupsY, 1);
    }

    public CommandBuffer dispatch(final int groupsX, final int groupsY, final int groupsZ) {
        VK10.vkCmdDispatch(this.handle, groupsX, groupsY, groupsZ);

        return this;
    }

    public CommandBuffer dispatchIndirect(final Buffer buffer, final long offset) {
        VK10.vkCmdDispatchIndirect(this.handle, buffer.handle, offset);

        return this;
    }

    public CommandBuffer draw(final int vertexCount, final int instanceCount, final int firstVertex, final int firstInstance) {
        VK10.vkCmdDraw(this.handle, vertexCount, instanceCount, firstVertex, firstInstance);

        return this;
    }

    public CommandBuffer drawIndirect(final Buffer buffer, final long offset, final int drawCount, final int stride) {
        VK10.vkCmdDrawIndirect(this.handle, buffer.handle, offset, drawCount, stride);

        return this;
    }

    public CommandBuffer drawIndexed(final int indexCount, final int instanceCount, final int firstIndex, final int vertexOffset, final int firstInstance) {
        VK10.vkCmdDrawIndexed(this.handle, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);

        return this;
    }

    public CommandBuffer drawIndexedIndirect(final Buffer buffer, final long offset, final int drawCount, final int stride) {
        VK10.vkCmdDrawIndexedIndirect(this.handle, buffer.handle, offset, drawCount, stride);

        return this;
    }

    public CommandBuffer updateBuffer(final Buffer buffer, final long offset, final ByteBuffer data) {
        VK10.vkCmdUpdateBuffer(this.handle, buffer.handle, offset, data);

        return this;
    }

    public CommandBuffer pipelineBarrier(
            final Set<PipelineStageFlag> srcStageMask, final Set<PipelineStageFlag> dstStageMask,
            final int dependencyFlags,
            final List<MemoryBarrier> memoryBarriers,
            final List<BufferMemoryBarrier> bufferMemoryBarriers,
            final List<ImageMemoryBarrier> imageMemoryBarriers) {

        try (var mem = MemoryStack.stackPush()) {
            final VkMemoryBarrier.Buffer pMemoryBarriers;

            if (memoryBarriers.isEmpty()) {
                pMemoryBarriers = null;
            } else {
                pMemoryBarriers = VkMemoryBarrier.callocStack(memoryBarriers.size(), mem);

                IntStream.range(0, memoryBarriers.size()).forEach(i -> memoryBarriers.get(i).deserialize(pMemoryBarriers.get(i)));
            }

            final VkBufferMemoryBarrier.Buffer pBufferMemoryBarriers;

            if (bufferMemoryBarriers.isEmpty()) {
                pBufferMemoryBarriers = null;
            } else {
                pBufferMemoryBarriers = VkBufferMemoryBarrier.callocStack(bufferMemoryBarriers.size(), mem);

                IntStream.range(0, bufferMemoryBarriers.size()).forEach(i -> bufferMemoryBarriers.get(i).deserialize(pBufferMemoryBarriers.get(i)));
            }

            final VkImageMemoryBarrier.Buffer pImageMemoryBarriers;

            if (imageMemoryBarriers.isEmpty()) {
                pImageMemoryBarriers = null;
            } else {
                pImageMemoryBarriers = VkImageMemoryBarrier.callocStack(imageMemoryBarriers.size(), mem);

                IntStream.range(0, imageMemoryBarriers.size()).forEach(i -> imageMemoryBarriers.get(i).deserialize(pImageMemoryBarriers.get(i)));
            }

            VK10.vkCmdPipelineBarrier(this.handle, PipelineStageFlag.toBitfield(srcStageMask), PipelineStageFlag.toBitfield(dstStageMask), dependencyFlags, pMemoryBarriers, pBufferMemoryBarriers, pImageMemoryBarriers);
        }

        return this;
    }

    public CommandBuffer bindPipeline(final Pipeline pipeline) {
        VK10.vkCmdBindPipeline(this.handle, pipeline.getBindpoint().value, pipeline.getHandle());

        return this;
    }

    public CommandBuffer bindDescriptorSet(
            final PipelineBindpoint bindpoint, final PipelineLayout layout,
            final int firstSet, final DescriptorSet descriptorSet) {

        try (var mem = MemoryStack.stackPush()) {
            VK10.vkCmdBindDescriptorSets(this.handle, bindpoint.value, layout.handle, firstSet, mem.longs(descriptorSet.handle), null);
        }

        return this;
    }

    public CommandBuffer bindDescriptorSet(
            final PipelineBindpoint bindpoint, final PipelineLayout layout,
            final int firstSet, final DescriptorSet descriptorSet, final int dynamicOffsets) {

        try (var mem = MemoryStack.stackPush()) {
            VK10.vkCmdBindDescriptorSets(this.handle, bindpoint.value, layout.handle, firstSet, mem.longs(descriptorSet.handle), mem.ints(dynamicOffsets));
        }

        return this;
    }

    public CommandBuffer bindDescriptorSets(
            final PipelineBindpoint bindpoint, final PipelineLayout layout,
            final int firstSet, final List<DescriptorSet> descriptorSets, final List<Integer> dynamicOffsets) {

        try (var mem = MemoryStack.stackPush()) {
            final var pDescriptorSets = descriptorSets.stream()
                    .map(ds -> ds.handle)
                    .collect(BufferCollectors.toLongBuffer());

            final var pDynamicOffsets = dynamicOffsets.stream()
                    .collect(BufferCollectors.toIntBuffer());

            VK10.vkCmdBindDescriptorSets(this.handle, bindpoint.value, layout.handle, firstSet, pDescriptorSets, pDynamicOffsets);
        }

        return this;
    }

    public CommandBuffer pushConstants(PipelineLayout layout, final Set<ShaderStage> stage, final int offset, final ByteBuffer data) {
        VK10.vkCmdPushConstants(this.handle, layout.handle, ShaderStage.toBitfield(stage), offset, data);

        return this;
    }

    public CommandBuffer copyImage(
            final Image src, final ImageLayout srcLayout,
            final Image dst, final ImageLayout dstLayout,
            final List<ImageCopy> regions) {

        try (var mem = MemoryStack.stackPush()) {
            final var pRegions = VkImageCopy.callocStack(regions.size(), mem);

            IntStream.range(0, regions.size()).forEach(i -> regions.get(i).deserialize(pRegions.get(i)));

            VK10.vkCmdCopyImage(this.handle, src.handle, srcLayout.value, dst.handle, dstLayout.value, pRegions);
        }

        return this;
    }

    public CommandBuffer copyImage(
            final Image src, final ImageLayout srcLayout,
            final Image dst, final ImageLayout dstLayout,
            final Offset3D srcOffset, final Offset3D dstOffset, final Extent3D extent,
            final ImageSubresourceLayers srcSubresource, final ImageSubresourceLayers dstSubresource) {

        try (var mem = MemoryStack.stackPush()) {
            final var pRegion = VkImageCopy.callocStack(1, mem);

            srcOffset.deserialize(pRegion.srcOffset());
            dstOffset.deserialize(pRegion.dstOffset());
            extent.deserialize(pRegion.extent());
            srcSubresource.deserialize(pRegion.srcSubresource());
            dstSubresource.deserialize(pRegion.dstSubresource());

            VK10.vkCmdCopyImage(this.handle, src.handle, srcLayout.value, dst.handle, dstLayout.value, pRegion);
        }

        return this;
    }

    public CommandBuffer transferBuffer(
            final Buffer buffer,
            final Set<PipelineStageFlag> srcStageMask, final Set<PipelineStageFlag> dstStageMask,
            final Set<AccessFlag> srcAccess, final Set<AccessFlag> dstAccess,
            final QueueFamily srcQueueFamily, final QueueFamily dstQueueFamily) {

        try (var mem = MemoryStack.stackPush()) {
            final var pBufferMemoryBarriers = VkBufferMemoryBarrier.callocStack(1, mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER)
                    .buffer(buffer.handle)
                    .offset(0L)
                    .size(VK10.VK_WHOLE_SIZE)
                    .srcAccessMask(AccessFlag.toBitfield(srcAccess))
                    .dstAccessMask(AccessFlag.toBitfield(dstAccess))
                    .srcQueueFamilyIndex(QueueFamily.indexOf(srcQueueFamily))
                    .dstQueueFamilyIndex(QueueFamily.indexOf(dstQueueFamily));

            VK10.vkCmdPipelineBarrier(this.handle, PipelineStageFlag.toBitfield(srcStageMask), PipelineStageFlag.toBitfield(dstStageMask), 0, null, pBufferMemoryBarriers, null);
        }

        return this;
    }

    public CommandBuffer transferImage(
            final Image image, final ImageLayout oldLayout, final ImageLayout newLayout,
            final Set<PipelineStageFlag> srcStageMask, final Set<PipelineStageFlag> dstStageMask,
            final Set<AccessFlag> srcAccess, final Set<AccessFlag> dstAccess,
            final QueueFamily srcQueueFamily, final QueueFamily dstQueueFamily) {

        try (var mem = MemoryStack.stackPush()) {
            final var pImageMemoryBarriers = VkImageMemoryBarrier.callocStack(1, mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                    .srcAccessMask(AccessFlag.toBitfield(srcAccess))
                    .dstAccessMask(AccessFlag.toBitfield(dstAccess))
                    .oldLayout(oldLayout.value)
                    .newLayout(newLayout.value)
                    .srcQueueFamilyIndex(QueueFamily.indexOf(srcQueueFamily))
                    .dstQueueFamilyIndex(QueueFamily.indexOf(dstQueueFamily))
                    .image(image.handle);

            image.getFullRange().deserialize(pImageMemoryBarriers.subresourceRange());

            VK10.vkCmdPipelineBarrier(this.handle, PipelineStageFlag.toBitfield(srcStageMask), PipelineStageFlag.toBitfield(dstStageMask), 0, null, null, pImageMemoryBarriers);
        }

        return this;
    }

    public CommandBuffer copyBufferToImage(final Buffer src, final Image dst, final ImageLayout layout, final BufferImageCopy... regions) {
        return this.copyBufferToImage(src, dst, layout, List.of(regions));
    }

    public CommandBuffer copyBufferToImage(final Buffer src, final Image dst, final ImageLayout layout, final List<BufferImageCopy> regions) {
        try (var mem = MemoryStack.stackPush()) {
            final var pRegions = VkBufferImageCopy.callocStack(regions.size(), mem);

            IntStream.range(0, regions.size()).forEach(i -> regions.get(i).deserialize(pRegions.get(i)));

            VK10.vkCmdCopyBufferToImage(this.handle, src.handle, dst.handle, layout.value, pRegions);
        }

        return this;
    }

    public CommandBuffer copyBufferToImage(final Buffer src, final Image dst, final ImageLayout layout) {
        return this.copyBufferToImage(src, dst, layout, 0);
    }

    public CommandBuffer copyBufferToImage(final Buffer src, final Image dst, final ImageLayout layout, final int mipLevel) {
        return this.copyBufferToImage(src, 0L, dst, layout, dst.getFullLayerRange(mipLevel), Offset3D.ZERO, dst.info.extent);
    }

    public CommandBuffer copyBufferToImage(final Buffer src, final long bufferOffset, final Image dst, final ImageLayout layout, final ImageSubresourceLayers subresourceRange, final Offset3D imageOffset, final Extent3D imageExtent) {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkBufferImageCopy = VkBufferImageCopy.callocStack(1, mem)
                    .bufferOffset(bufferOffset);

            imageOffset.deserialize(pvkBufferImageCopy.imageOffset());
            imageExtent.deserialize(pvkBufferImageCopy.imageExtent());
            subresourceRange.deserialize(pvkBufferImageCopy.imageSubresource());

            VK10.vkCmdCopyBufferToImage(this.handle, src.handle, dst.handle, layout.value, pvkBufferImageCopy);
        }

        return this;
    }

    public CommandBuffer copyBuffer(final Buffer src, final Buffer dst, final long srcOffset, final long dstOffset, final long size) {
        try (var mem = MemoryStack.stackPush()) {
            final var pRegion = VkBufferCopy.callocStack(1, mem);

            pRegion.get(0).set(srcOffset, dstOffset, size);

            VK10.vkCmdCopyBuffer(this.handle, src.handle, dst.handle, pRegion);
        }

        return this;
    }

    public CommandBuffer copyBuffers(final Buffer src, final Buffer dst, final List<BufferCopy> regions) {
        try (var mem = MemoryStack.stackPush()) {
            final var pRegions = VkBufferCopy.callocStack(regions.size(), mem);

            IntStream.range(0, regions.size()).forEach(i -> {
                final var region = regions.get(i);

                pRegions.get(i).set(region.srcOffset, region.dstOffset, region.size);
            });

            VK10.vkCmdCopyBuffer(this.handle, src.handle, dst.handle, pRegions);

            return this;
        }
    }

    public CommandBuffer fillBuffer(final Buffer buffer, final long offset, final long size, final int data) {
        VK10.vkCmdFillBuffer(this.handle, buffer.handle, offset, size, data);

        return this;
    }
}
