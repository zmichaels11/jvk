package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDescriptorPoolCreateInfo;
import org.lwjgl.vulkan.VkDescriptorSetAllocateInfo;

import java.lang.ref.WeakReference;
import java.util.*;

import static com.longlinkislong.jvk.Util.vkAssert;

public class DescriptorPool {
    public static final class CreateInfo {
        public final int flags;
        public final int maxSets;
        public final List<DescriptorPoolSize> poolSizes;

        public CreateInfo(final int flags, final int maxSets, final List<DescriptorPoolSize> poolSizes) {
            this.flags = flags;
            this.maxSets = maxSets;
            this.poolSizes = List.copyOf(poolSizes);
        }
    }

    private final class Pool {
        private final long handle;
        private final int index;
        private int allocatedSets = 0;

        Pool(final long handle, final int index) {
            this.handle = handle;
            this.index = index;
        }
    }

    public final CreateInfo info;
    private final WeakReference<DescriptorSetLayout> descriptorSetLayout;
    private final Object lock = new Object();
    private final List<Pool> pools = new ArrayList<>();
    private final Set<DescriptorSet> allocatedDescriptorSets = new HashSet<>();

    DescriptorPool(final CreateInfo info, final DescriptorSetLayout descriptorSetLayout) {
        this.info = info;
        this.descriptorSetLayout = new WeakReference<>(descriptorSetLayout);
    }

    void freeDescriptorSet(final DescriptorSet set) {
        synchronized (lock) {
            assert this.allocatedDescriptorSets.contains(set);

            final var pool = this.pools.get(set.poolIndex);

            VK10.vkFreeDescriptorSets(this.getDevice().handle, pool.handle, set.handle);

            pool.allocatedSets -= 1;

            this.allocatedDescriptorSets.remove(set);
        }
    }

    private Pool allocatePool(Device device) {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkDescriptorPoolSizes = DescriptorPoolSize.deserialize(this.info.poolSizes, mem);
            final var pvkDescriptorPoolCI = VkDescriptorPoolCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO)
                    .flags(info.flags | VK10.VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT)
                    .pPoolSizes(pvkDescriptorPoolSizes)
                    .maxSets(info.maxSets);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateDescriptorPool(device.handle, pvkDescriptorPoolCI, null, pHandle));

            final int index = this.pools.size();
            final var out = new Pool(pHandle.get(), index);

            this.pools.add(out);

            return out;
        }
    }

    public DescriptorSet allocate() {
        final var device = this.getDevice();

        try (var mem = MemoryStack.stackPush()) {
            final var pvkDescriptorSetAI = VkDescriptorSetAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO)
                    .pSetLayouts(mem.longs(this.getDescriptorSetLayout().handle));

            final var pHandle = mem.callocLong(1);

            synchronized (lock) {
                final var selectedPool = this.pools.stream()
                        .filter(pool -> pool.allocatedSets < this.info.maxSets)
                        .findFirst();

                if (selectedPool.isPresent()) {
                    final var pool = selectedPool.get();

                    pvkDescriptorSetAI.descriptorPool(pool.handle);

                    vkAssert(VK10.vkAllocateDescriptorSets(device.handle, pvkDescriptorSetAI, pHandle));

                    pool.allocatedSets += 1;

                    return new DescriptorSet(this, pool.index, pHandle.get());
                } else {
                    final var pool = this.allocatePool(device);

                    pvkDescriptorSetAI.descriptorPool(pool.handle);

                    vkAssert(VK10.vkAllocateDescriptorSets(device.handle, pvkDescriptorSetAI, pHandle));

                    pool.allocatedSets += 1;

                    return new DescriptorSet(this, pool.index, pHandle.get());
                }
            }
        }
    }

    public List<DescriptorSet> allocate(final int nSets) {
        final var device = this.getDevice();

        try (var mem = MemoryStack.stackPush()) {
            final var pvkDescriptorSetAI = VkDescriptorSetAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO)
                    .pSetLayouts(mem.longs(this.getDescriptorSetLayout().handle));

            final var pHandle = mem.callocLong(1);
            final var out = new ArrayList<DescriptorSet>();

            synchronized (lock) {
                while (out.size() < nSets) {
                    final var selectedPool = this.pools.stream()
                            .filter(pool -> pool.allocatedSets < this.info.maxSets)
                            .findFirst();

                    if (selectedPool.isPresent()) {
                        final var pool = selectedPool.get();

                        pvkDescriptorSetAI.descriptorPool(pool.handle);

                        pHandle.clear();
                        vkAssert(VK10.vkAllocateDescriptorSets(device.handle, pvkDescriptorSetAI, pHandle));

                        out.add(new DescriptorSet(this, pool.index, pHandle.get()));

                        pool.allocatedSets += 1;
                    } else {
                        this.allocatePool(device);
                    }
                }
            }

            return out;
        }
    }

    public void free() {
        final var device = this.getDevice();

        synchronized (lock) {
            this.allocatedDescriptorSets.forEach(set -> {
                final var pool = this.pools.get(set.poolIndex);

                VK10.vkFreeDescriptorSets(device.handle, pool.handle, set.handle);
            });

            this.allocatedDescriptorSets.clear();

            this.pools.forEach(pool -> VK10.vkDestroyDescriptorPool(device.handle, pool.handle, null));
            this.pools.clear();
        }
    }

    public Device getDevice() {
        return this.getDescriptorSetLayout().getDevice();
    }

    public DescriptorSetLayout getDescriptorSetLayout() {
        return Objects.requireNonNull(this.descriptorSetLayout.get(), "DescriptorSetLayout was lost!");
    }
}
