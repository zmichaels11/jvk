package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkImageViewCreateInfo;

import java.lang.ref.WeakReference;
import java.util.Objects;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class ImageView {
    public static final class CreateInfo {
        public final ImageViewType viewType;
        public final Format format;
        public final ImageSubresourceRange subresourceRange;

        public CreateInfo(final ImageViewType viewType, final Format format, final ImageSubresourceRange subresourceRange) {
            this.viewType = viewType;
            this.format = format;
            this.subresourceRange = subresourceRange;
        }

        public CreateInfo() {
            this(null, null, null);
        }

        public CreateInfo withImageViewType(final ImageViewType viewType) {
            return new CreateInfo(viewType, format, subresourceRange);
        }

        public CreateInfo withFormat(final Format format) {
            return new CreateInfo(viewType, format, subresourceRange);
        }

        public CreateInfo withSubresourceRange(final ImageSubresourceRange subresourceRange) {
            return new CreateInfo(viewType, format, subresourceRange);
        }
    }

    public final long handle;
    private final WeakReference<Image> image;
    public final CreateInfo info;

    public ImageView(final Image image) {
        this(image, image.getFullViewCreateInfo());
    }

    public ImageView(final Image image, final CreateInfo info) {
        final var device = image.getDevice();

        this.image = new WeakReference<>(image);
        this.info = info;

        try (var mem = MemoryStack.stackPush()) {
            final var pvkImageViewCI = VkImageViewCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO)
                    .image(image.handle)
                    .viewType(info.viewType.value)
                    .format(info.format.value);

            info.subresourceRange.deserialize(pvkImageViewCI.subresourceRange());

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateImageView(device.handle, pvkImageViewCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    public Device getDevice() {
        return this.getImage().getDevice();
    }

    public Image getImage() {
        return Objects.requireNonNull(this.image.get(), "Image was lost!");
    }


    public void free() {
        VK10.vkDestroyImageView(this.getDevice().handle, this.handle, null);
    }
}
