package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

import java.util.Set;

public enum CommandBufferUsageFlag {
    ONE_TIME_SUBMIT(VK10.VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT),
    RENDER_PASS_CONTINUE(VK10.VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT),
    SIMULTANEOUS_USE(VK10.VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);

    final int value;

    CommandBufferUsageFlag(final int value) {
        this.value = value;
    }

    static int toBitfield(final Set<CommandBufferUsageFlag> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .sum();
    }
}
