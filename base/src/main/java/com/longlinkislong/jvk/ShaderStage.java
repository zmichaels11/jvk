package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

import java.util.EnumSet;
import java.util.Set;

public enum ShaderStage {
    VERTEX(VK10.VK_SHADER_STAGE_VERTEX_BIT),
    TESSELLATION_CONTROL(VK10.VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT),
    TESSELLATION_EVALUATION(VK10.VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT),
    GEOMETRY(VK10.VK_SHADER_STAGE_GEOMETRY_BIT),
    FRAGMENT(VK10.VK_SHADER_STAGE_FRAGMENT_BIT),
    COMPUTE(VK10.VK_SHADER_STAGE_COMPUTE_BIT);

    final int value;

    ShaderStage(final int value) {
        this.value = value;
    }

    public static Set<ShaderStage> allGraphics() {
        return EnumSet.of(VERTEX, TESSELLATION_CONTROL, TESSELLATION_EVALUATION, GEOMETRY, FRAGMENT);
    }

    static int toBitfield(final Set<ShaderStage> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .sum();
    }
}
