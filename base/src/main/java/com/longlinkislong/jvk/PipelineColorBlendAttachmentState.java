package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkPipelineColorBlendAttachmentState;

import java.util.List;
import java.util.stream.IntStream;

public final class PipelineColorBlendAttachmentState {
    public final boolean blendEnable;
    public final BlendFactor srcColorBlendFactor;
    public final BlendFactor dstColorBlendFactor;
    public final BlendOp colorBlendOp;
    public final BlendFactor srcAlphaBlendFactor;
    public final BlendFactor dstAlphaBlendFactor;
    public final BlendOp alphaBlendOp;
    public final int colorWriteMask;

    public PipelineColorBlendAttachmentState(
            final boolean blendEnable,
            final BlendFactor srcColorBlendFactor, final BlendFactor dstColorBlendFactor, final BlendOp colorBlendOp,
            final BlendFactor srcAlphaBlendFactor, final BlendFactor dstAlphaBlendFactor, final BlendOp alphaBlendOp,
            final int colorWriteMask) {

        this.blendEnable = blendEnable;
        this.srcColorBlendFactor = srcColorBlendFactor;
        this.dstColorBlendFactor = dstColorBlendFactor;
        this.colorBlendOp = colorBlendOp;
        this.srcAlphaBlendFactor = srcAlphaBlendFactor;
        this.dstAlphaBlendFactor = dstAlphaBlendFactor;
        this.alphaBlendOp = alphaBlendOp;
        this.colorWriteMask = colorWriteMask;
    }

    public PipelineColorBlendAttachmentState() {
        this(false, BlendFactor.ONE, BlendFactor.ZERO, BlendOp.ADD, BlendFactor.ONE, BlendFactor.ZERO, BlendOp.ADD, 0xF);
    }

    public PipelineColorBlendAttachmentState withBlendEnable(final boolean blendEnable) {
        return new PipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public PipelineColorBlendAttachmentState withColorBlend(final BlendFactor srcColorBlendFactor, final BlendFactor dstColorBlendFactor, final BlendOp colorBlendOp) {
        return new PipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public PipelineColorBlendAttachmentState withAlphaBlend(final BlendFactor srcAlphaBlendFactor, final BlendFactor dstAlphaBlendFactor, final BlendOp alphaBlendOp) {
        return new PipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public PipelineColorBlendAttachmentState withColorWriteMask(final int colorWriteMask) {
        return new PipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    VkPipelineColorBlendAttachmentState deserialize(final VkPipelineColorBlendAttachmentState vkobj) {
        return vkobj.set(blendEnable, srcColorBlendFactor.value, dstColorBlendFactor.value, colorBlendOp.value, srcAlphaBlendFactor.value, dstAlphaBlendFactor.value, alphaBlendOp.value, colorWriteMask);
    }

    static VkPipelineColorBlendAttachmentState.Buffer deserialize(final List<PipelineColorBlendAttachmentState> list, final MemoryStack mem) {
        final var out = VkPipelineColorBlendAttachmentState.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id)));

        return out;
    }
}
