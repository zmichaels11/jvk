package com.longlinkislong.jvk;

import java.util.Set;

import static org.lwjgl.vulkan.VK11.*;

public enum MemoryProperty {
    DEVICE_LOCAL(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
    HOST_VISIBLE(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT),
    HOST_COHERENT(VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
    HOST_CACHED(VK_MEMORY_PROPERTY_HOST_CACHED_BIT),
    LAZILY_ALLOCATED(VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT);

    final int value;

    MemoryProperty(final int value) {
        this.value = value;
    }

    static int toBitfield(final Set<MemoryProperty> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .sum();
    }
}
