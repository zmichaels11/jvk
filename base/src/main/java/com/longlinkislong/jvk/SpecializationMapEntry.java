package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkSpecializationMapEntry;

import java.util.List;
import java.util.stream.IntStream;

public final class SpecializationMapEntry {
    public final int constantID;
    public final int offset;
    public final long size;

    public SpecializationMapEntry(final int constantID, final int offset, final long size) {
        this.constantID = constantID;
        this.offset = offset;
        this.size = size;
    }

    VkSpecializationMapEntry deserialize(final VkSpecializationMapEntry vkobj) {
        return vkobj.set(constantID, offset, size);
    }

    static VkSpecializationMapEntry.Buffer deserialize(final List<SpecializationMapEntry> list, final MemoryStack mem) {
        final var out = VkSpecializationMapEntry.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id)));

        return out;
    }
}
