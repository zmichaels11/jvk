package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkExtent2D;

public final class Extent2D {
    public final int width;
    public final int height;

    public Extent2D(final int width, final int height) {
        this.width = width;
        this.height = height;
    }

    VkExtent2D deserialize(final VkExtent2D vkobj) {
        return vkobj.set(this.width, this.height);
    }
}
