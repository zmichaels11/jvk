package com.longlinkislong.jvk;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.*;
import java.util.stream.Collector;

abstract class PointerBufferCollector implements Collector<Long, List<Long>, PointerBuffer> {
    @Override
    public Supplier<List<Long>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<Long>, Long> accumulator() {
        return List::add;
    }

    @Override
    public BinaryOperator<List<Long>> combiner() {
        return (left, right) -> {
            left.addAll(right);

            return right;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of(Characteristics.UNORDERED);
    }

    public static PointerBufferCollector allocate(final IntFunction<PointerBuffer> constructor) {
        return new PointerBufferCollector() {
            @Override
            public Function<List<Long>, PointerBuffer> finisher() {
                return list -> {
                    final var out = constructor.apply(list.size());

                    list.forEach(out::put);
                    out.flip();

                    return out;
                };
            }
        };
    }

    static PointerBufferCollector create() {
        return allocate(BufferUtils::createPointerBuffer);
    }
}
