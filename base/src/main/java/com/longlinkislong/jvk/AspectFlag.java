package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum AspectFlag {
    COLOR(VK10.VK_IMAGE_ASPECT_COLOR_BIT),
    DEPTH(VK10.VK_IMAGE_ASPECT_DEPTH_BIT),
    STENCIL(VK10.VK_IMAGE_ASPECT_STENCIL_BIT);

    final int value;

    AspectFlag(final int value) {
        this.value = value;
    }

    static int toBitfield(final Set<AspectFlag> aspectMask) {
        return aspectMask.stream()
                .mapToInt(aspect -> aspect.value)
                .sum();
    }

    static Set<AspectFlag> toEnumSet(final int bitfield) {
        return Arrays.stream(values())
                .filter(flag -> 0 != (bitfield & flag.value))
                .collect(Collectors.toUnmodifiableSet());
    }
}
