package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkAttachmentReference;

import java.util.List;
import java.util.stream.IntStream;

public final class AttachmentReference {
    public final int attachment;
    public final ImageLayout layout;

    public AttachmentReference(final int attachment, final ImageLayout layout) {
        this.attachment = attachment;
        this.layout = layout;
    }

    VkAttachmentReference deserialize(final VkAttachmentReference vkobj) {
        return vkobj.set(attachment, layout.value);
    }

    static VkAttachmentReference.Buffer deserialize(final List<AttachmentReference> buffer, final MemoryStack mem) {
        if (buffer.isEmpty()) {
            return null;
        }

        final var out = VkAttachmentReference.callocStack(buffer.size(), mem);

        IntStream.range(0, buffer.size()).forEach(i -> buffer.get(i).deserialize(out.get(i)));

        return out;
    }
}
