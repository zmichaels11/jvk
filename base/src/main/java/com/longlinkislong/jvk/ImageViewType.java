package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum ImageViewType {
    VIEW_1D(VK10.VK_IMAGE_VIEW_TYPE_1D),
    VIEW_1D_ARRAY(VK10.VK_IMAGE_VIEW_TYPE_1D_ARRAY),
    VIEW_2D(VK10.VK_IMAGE_VIEW_TYPE_2D),
    VIEW_2D_ARRAY(VK10.VK_IMAGE_VIEW_TYPE_2D_ARRAY),
    VIEW_3D(VK10.VK_IMAGE_VIEW_TYPE_3D),
    CUBE(VK10.VK_IMAGE_VIEW_TYPE_CUBE),
    CUBE_ARRAY(VK10.VK_IMAGE_VIEW_TYPE_CUBE_ARRAY);

    final int value;

    ImageViewType(final int value) {
        this.value = value;
    }
}
