package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum SamplerMipmapFilter {
    NEAREST(VK10.VK_SAMPLER_MIPMAP_MODE_NEAREST),
    LINEAR(VK10.VK_SAMPLER_MIPMAP_MODE_LINEAR);

    final int value;

    SamplerMipmapFilter(final int value) {
        this.value = value;
    }
}
