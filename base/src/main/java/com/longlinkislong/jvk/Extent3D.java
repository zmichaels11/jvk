package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkExtent3D;

public final class Extent3D {
    public final int width;
    public final int height;
    public final int depth;

    public Extent3D(final int width, final int height, final int depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    VkExtent3D deserialize(final VkExtent3D vkobj) {
        return vkobj.set(width, height, depth);
    }

    static Extent3D serialize(final VkExtent3D vkobj) {
        return new Extent3D(vkobj.width(), vkobj.height(), vkobj.depth());
    }
}
