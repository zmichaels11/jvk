package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkBufferViewCreateInfo;

import java.lang.ref.WeakReference;
import java.util.Objects;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class BufferView {
    public static final class CreateInfo {
        public final int flags;
        public final Format format;
        public final long offset;
        public final long range;

        public CreateInfo(final int flags, final Format format, final long offset, final long range) {
            this.flags = flags;
            this.format = format;
            this.offset = offset;
            this.range = range;
        }
    }

    public final long handle;
    public final CreateInfo info;
    private final WeakReference<Buffer> buffer;

    public BufferView(final Buffer buffer, final CreateInfo info) {
        final var device = buffer.getDevice();

        this.buffer = new WeakReference<>(buffer);
        this.info = info;

        try (var mem = MemoryStack.stackPush()) {
            final var pvkBufferViewCI = VkBufferViewCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO)
                    .flags(info.flags)
                    .buffer(buffer.handle)
                    .format(info.format.value)
                    .offset(info.offset)
                    .range(info.range);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateBufferView(device.handle, pvkBufferViewCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    public Device getDevice() {
        return this.getBuffer().getDevice();
    }

    public Buffer getBuffer() {
        return Objects.requireNonNull(this.buffer.get(), "Buffer was lost!");
    }

    public void free() {
        VK10.vkDestroyBufferView(this.getDevice().handle, this.handle, null);
    }
}
