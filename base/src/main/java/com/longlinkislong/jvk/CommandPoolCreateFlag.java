package com.longlinkislong.jvk;

import java.util.Set;

import static org.lwjgl.vulkan.VK11.*;

public enum CommandPoolCreateFlag {
    CREATE_PROTECTED(VK_COMMAND_POOL_CREATE_PROTECTED_BIT),
    CREATE_RESET_COMMAND_BUFFER(VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT),
    CREATE_TRANSIENT(VK_COMMAND_POOL_CREATE_TRANSIENT_BIT);

    final int value;

    CommandPoolCreateFlag(final int value) {
        this.value = value;
    }

    static int toBitfield(final Set<CommandPoolCreateFlag> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .sum();
    }
}
