package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkMemoryBarrier;

import java.util.Set;

public final class MemoryBarrier {
    public final Set<AccessFlag> srcAccessMask;
    public final Set<AccessFlag> dstAccessMask;

    public MemoryBarrier(final Set<AccessFlag> srcAccessMask, final Set<AccessFlag> dstAccessMask) {
        this.srcAccessMask = Set.copyOf(srcAccessMask);
        this.dstAccessMask = Set.copyOf(dstAccessMask);
    }

    public MemoryBarrier() {
        this(Set.of(), Set.of());
    }

    public MemoryBarrier withSrcAccessMask(final Set<AccessFlag> srcAccessMask) {
        return new MemoryBarrier(srcAccessMask, dstAccessMask);
    }

    public MemoryBarrier withSrcAccessMask(final AccessFlag... srcAccess) {
        return withSrcAccessMask(Set.of(srcAccess));
    }

    public MemoryBarrier withDstAccessMask(final Set<AccessFlag> dstAccessMask) {
        return new MemoryBarrier(srcAccessMask, dstAccessMask);
    }

    public MemoryBarrier withDstAccessMask(final AccessFlag... dstAccess) {
        return withDstAccessMask(Set.of(dstAccess));
    }

    VkMemoryBarrier deserialize(final VkMemoryBarrier vkobj) {
        return vkobj.set(VK10.VK_STRUCTURE_TYPE_MEMORY_BARRIER, 0L, AccessFlag.toBitfield(srcAccessMask), AccessFlag.toBitfield(dstAccessMask));
    }
}
