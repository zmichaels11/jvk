package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineLayoutCreateInfo;
import org.lwjgl.vulkan.VkPushConstantRange;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class PipelineLayout {
    public static final class CreateInfo {
        public final int flags;
        public final List<PushConstantRange> pushConstantRanges;
        public final List<DescriptorSetLayout.CreateInfo> setLayoutInfos;

        public CreateInfo(final int flags, final List<PushConstantRange> pushConstantRanges, final List<DescriptorSetLayout.CreateInfo> setLayoutInfos) {
            this.flags = flags;
            this.pushConstantRanges = List.copyOf(pushConstantRanges);
            this.setLayoutInfos = List.copyOf(setLayoutInfos);
        }

        public CreateInfo() {
            this(0, List.of(), List.of());
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, pushConstantRanges, setLayoutInfos);
        }

        public CreateInfo withPushConstantRanges(final List<PushConstantRange> pushConstantRanges) {
            return new CreateInfo(flags, pushConstantRanges, setLayoutInfos);
        }

        public CreateInfo withPushConstantRanges(final PushConstantRange... pushConstantRanges) {
            return withPushConstantRanges(List.of(pushConstantRanges));
        }

        public CreateInfo withSetLayoutInfos(final List<DescriptorSetLayout.CreateInfo> setLayoutInfos) {
            return new CreateInfo(flags, pushConstantRanges, setLayoutInfos);
        }

        public CreateInfo withSetLayoutInfos(final DescriptorSetLayout.CreateInfo... setLayoutInfos) {
            return withSetLayoutInfos(List.of(setLayoutInfos));
        }
    }

    public final CreateInfo info;
    public final long handle;
    private final WeakReference<PipelineLayoutCache> cache;
    private final List<WeakReference<DescriptorSetLayout>> setLayouts;

    PipelineLayout(final PipelineLayoutCache cache, final CreateInfo createInfo) {
        final var device = cache.getDevice();

        final var setLayouts = createInfo.setLayoutInfos.stream()
                .map(device.descriptorSetLayoutCache::allocateDescriptorSetLayout)
                .collect(Collectors.toList());

        this.setLayouts = setLayouts.stream()
                .map(WeakReference::new)
                .collect(Collectors.toList());

        this.cache = new WeakReference<>(cache);
        this.info = createInfo;

        try (var mem = MemoryStack.stackPush()) {
            final VkPushConstantRange.Buffer pPushConstantRanges;

            if (createInfo.pushConstantRanges.isEmpty()) {
                pPushConstantRanges = null;
            } else {
                pPushConstantRanges = VkPushConstantRange.callocStack(createInfo.pushConstantRanges.size(), mem);

                IntStream.range(0, createInfo.pushConstantRanges.size()).forEach(i -> createInfo.pushConstantRanges.get(i).deserialize(pPushConstantRanges.get(i)));
            }

            final var pSetLayouts = setLayouts.stream()
                    .mapToLong(setLayout -> setLayout.handle)
                    .boxed()
                    .collect(BufferCollectors.toLongBuffer(mem));

            final var pvkPipelineLayoutCI = VkPipelineLayoutCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO)
                    .flags(createInfo.flags)
                    .pSetLayouts(pSetLayouts)
                    .pPushConstantRanges(pPushConstantRanges);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreatePipelineLayout(device.handle, pvkPipelineLayoutCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    public List<DescriptorSetLayout> getDescriptorSetLayouts() {
        return this.setLayouts.stream()
                .map(WeakReference::get)
                .map(Objects::requireNonNull)
                .collect(Collectors.toList());
    }

    public DescriptorSetLayout getDescriptorSetLayout(final int index) {
        return Objects.requireNonNull(this.setLayouts.get(index).get(), "DescriptorSetLayout was lost!");
    }

    public PipelineLayoutCache getPipelineLayoutCache() {
        return Objects.requireNonNull(this.cache.get(), "PipelineLayoutCache was lost!");
    }

    public Device getDevice() {
        return this.getPipelineLayoutCache().getDevice();
    }

    public void release() {
        this.getPipelineLayoutCache().releasePipelineLayout(this);
    }

    void free() {
        VK10.vkDestroyPipelineLayout(this.getDevice().handle, this.handle, null);
        this.setLayouts.stream()
                .map(WeakReference::get)
                .filter(Objects::nonNull)
                .forEach(DescriptorSetLayout::release);

        this.setLayouts.clear();
    }
}
