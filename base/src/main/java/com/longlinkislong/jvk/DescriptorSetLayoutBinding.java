package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkDescriptorSetLayoutBinding;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.IntStream;

public final class DescriptorSetLayoutBinding {
    public final int binding;
    public final DescriptorType descriptorType;
    public final int descriptorCount;
    public final Set<ShaderStage> stages;
    public final List<Long> immutableSamplers;

    public DescriptorSetLayoutBinding(
            final int binding,
            final DescriptorType descriptorType, final int descriptorCount,
            final Set<ShaderStage> stages, final List<Long> immutableSamplers) {

        this.binding = binding;
        this.descriptorType = descriptorType;
        this.descriptorCount = descriptorCount;
        this.stages = Set.copyOf(stages);
        this.immutableSamplers = List.copyOf(immutableSamplers);
    }

    public DescriptorSetLayoutBinding() {
        this(0, null, 0, Set.of(), List.of());
    }

    public DescriptorSetLayoutBinding withBinding(final int binding) {
        return new DescriptorSetLayoutBinding(binding, descriptorType, descriptorCount, stages, immutableSamplers);
    }

    public DescriptorSetLayoutBinding withDescriptorType(final DescriptorType descriptorType) {
        return new DescriptorSetLayoutBinding(binding, descriptorType, descriptorCount, stages, immutableSamplers);
    }

    public DescriptorSetLayoutBinding withDescriptorCount(final int descriptorCount) {
        return new DescriptorSetLayoutBinding(binding, descriptorType, descriptorCount, stages, immutableSamplers);
    }

    public DescriptorSetLayoutBinding withStages(final Set<ShaderStage> stages) {
        return new DescriptorSetLayoutBinding(binding, descriptorType, descriptorCount, stages, immutableSamplers);
    }

    public DescriptorSetLayoutBinding withStages(final ShaderStage... stages) {
        return withStages(Set.of(stages));
    }

    public DescriptorSetLayoutBinding withImmutableSamplers(final List<Long> immutableSamplers) {
        return new DescriptorSetLayoutBinding(binding, descriptorType, descriptorCount, stages, immutableSamplers);
    }

    VkDescriptorSetLayoutBinding deserialize(final VkDescriptorSetLayoutBinding vkobj, final MemoryStack mem) {
        if (this.immutableSamplers.isEmpty()) {
            return vkobj.set(binding, descriptorType.value, descriptorCount, ShaderStage.toBitfield(stages), null);
        } else {
            final var pImmutableSamplers = immutableSamplers.stream().collect(BufferCollectors.toLongBuffer(mem));

            return vkobj.set(binding, descriptorType.value, descriptorCount, ShaderStage.toBitfield(stages), pImmutableSamplers);
        }
    }

    static VkDescriptorSetLayoutBinding.Buffer deserialize(final List<DescriptorSetLayoutBinding> list, final MemoryStack mem) {
        final var out = VkDescriptorSetLayoutBinding.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id), mem));

        return out;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DescriptorSetLayoutBinding that = (DescriptorSetLayoutBinding) o;
        return binding == that.binding &&
                descriptorCount == that.descriptorCount &&
                descriptorType == that.descriptorType &&
                Objects.equals(stages, that.stages) &&
                Objects.equals(immutableSamplers, that.immutableSamplers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(binding, descriptorType, descriptorCount, stages, immutableSamplers);
    }
}
