package com.longlinkislong.jvk;

import com.longlinkislong.jvk.memory.MemoryManager;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.*;

import java.lang.ref.WeakReference;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.longlinkislong.jvk.Util.vkAssert;


public final class Device {
    public final VkDevice handle;
    public final List<QueueFamily> queueFamilies;
    public final Set<String> enabledExtensions;

    final MemoryManager memoryManager;
    final SamplerCache samplerCache;
    final DescriptorSetLayoutCache descriptorSetLayoutCache;
    final PipelineLayoutCache pipelineLayoutCache;
    final PipelineCache pipelineCache;

    private final FencePool fencePool;
    private final SemaphorePool semaphorePool;

    private final WeakReference<PhysicalDevice> physicalDevice;
    private final Object lock = new Object();
    private final Map<ShaderModule.CreateInfo, ShaderModule> shaderCache = new HashMap<>();

    public Object userData = null;

    Device(final PhysicalDevice physicalDevice, final Set<String> enabledExtensions) {
        this.physicalDevice = new WeakReference<>(physicalDevice);
        this.enabledExtensions = Set.copyOf(enabledExtensions);

        try (var mem = MemoryStack.stackPush()) {
            final var pQueueFamilyCount = mem.callocInt(1);

            VK10.vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice.handle, pQueueFamilyCount, null);

            final var queueFamilyCount = pQueueFamilyCount.get(0);
            final var pvkQueueFamilyProperties = VkQueueFamilyProperties.callocStack(queueFamilyCount, mem);

            VK10.vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice.handle, pQueueFamilyCount, pvkQueueFamilyProperties);

            final var pvkDeviceQueueCI = VkDeviceQueueCreateInfo.callocStack(queueFamilyCount, mem);

            for (int i = 0; i < queueFamilyCount; i++) {
                final var pProperties = IntStream.range(0, pvkQueueFamilyProperties.get(i).queueCount())
                        .mapToObj(__ -> 1.0F)
                        .collect(BufferCollectors.toFloatBuffer(mem));

                pvkDeviceQueueCI.get(i)
                        .sType(VK10.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO)
                        .queueFamilyIndex(i)
                        .pQueuePriorities(pProperties);
            }

            final var ppEnabledExtensions = enabledExtensions.stream()
                    .map(mem::UTF8)
                    .map(MemoryUtil::memAddress)
                    .collect(BufferCollectors.toPointerBuffer(mem));

            final var pvkDeviceCI = VkDeviceCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO)
                    .pQueueCreateInfos(pvkDeviceQueueCI)
                    .pEnabledFeatures(physicalDevice.pvkFeatures)
                    .ppEnabledExtensionNames(ppEnabledExtensions);

            final var pHandle = mem.callocPointer(1);

            vkAssert(VK10.vkCreateDevice(physicalDevice.handle, pvkDeviceCI, null, pHandle));

            this.handle = new VkDevice(pHandle.get(), physicalDevice.handle, pvkDeviceCI);

            this.queueFamilies = IntStream.range(0, queueFamilyCount)
                    .mapToObj(queueFamilyIndex -> new QueueFamily(this, queueFamilyIndex, pvkQueueFamilyProperties))
                    .collect(Collectors.toList());
        }

        this.semaphorePool = new SemaphorePool(this);
        this.memoryManager = new MemoryManager(this.handle);
        this.descriptorSetLayoutCache = new DescriptorSetLayoutCache(this);
        this.pipelineLayoutCache = new PipelineLayoutCache(this);
        this.pipelineCache = new PipelineCache(this);
        this.fencePool = new FencePool(this);
        this.samplerCache = new SamplerCache(this);
    }

    public PhysicalDevice getPhysicalDevice() {
        return Objects.requireNonNull(this.physicalDevice.get(), "PhysicalDevice was lost!");
    }

    ShaderModule getShaderModule(final ShaderModule.CreateInfo createInfo) {
        synchronized (lock) {
            return this.shaderCache.computeIfAbsent(createInfo, info -> new ShaderModule(this, info));
        }
    }

    public Fence acquireFence() {
        return this.fencePool.acquireFence();
    }

    public Semaphore acquireSemaphore() {
        return this.semaphorePool.acquireSemaphore();
    }

    public ComputePipeline allocatePipeline(final ComputePipeline.CreateInfo createInfo) {
        return this.pipelineCache.allocate(createInfo);
    }

    public GraphicsPipeline allocatePipeline(final GraphicsPipeline.CreateInfo createInfo, final RenderPass renderPass) {
        return this.pipelineCache.allocate(createInfo, renderPass);
    }

    public Buffer createBuffer(final Buffer.CreateInfo createInfo, final MemoryProperty... properties) {
        return createBuffer(createInfo, Set.of(properties));
    }

    public Buffer createBuffer(final Buffer.CreateInfo createInfo, final Set<MemoryProperty> properties) {
        return new Buffer(this, createInfo, MemoryProperty.toBitfield(properties));
    }

    public Image createImage(final Image.CreateInfo createInfo, final Set<MemoryProperty> properties) {
        return new Image(this, createInfo, MemoryProperty.toBitfield(properties));
    }

    public Image createImage(final Image.CreateInfo createInfo, final MemoryProperty... properties) {
        return createImage(createInfo, Set.of(properties));
    }

    public Sampler createSampler(final Sampler.CreateInfo createInfo) {
        return this.samplerCache.allocate(createInfo);
    }

    public RenderPass createRenderPass(final RenderPass.CreateInfo createInfo) {
        return new RenderPass(this, createInfo);
    }

    public Swapchain createSwapchain(final Swapchain.CreateInfo createInfo) {
        return new Swapchain(this, createInfo);
    }

    public void waitIdle() {
        VK10.vkDeviceWaitIdle(this.handle);
    }

    public void free() {
        this.waitIdle();

        synchronized (this) {
            this.samplerCache.free();
            this.pipelineLayoutCache.free();
            this.pipelineCache.free();
            this.memoryManager.free();
            this.descriptorSetLayoutCache.free();
            this.semaphorePool.free();
            this.fencePool.free();
            this.shaderCache.values().forEach(ShaderModule::free);
            this.shaderCache.clear();
            this.queueFamilies.clear();
            VK10.vkDestroyDevice(this.handle, null);
        }
    }

    public QueueFamily getQueueFamily(final int index) {
        return this.queueFamilies.get(index);
    }

    public int getQueueFamilyCount() {
        return this.queueFamilies.size();
    }
}
