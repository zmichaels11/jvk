package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkImageMemoryBarrier;

import java.util.Set;

public final class ImageMemoryBarrier {
    public final Set<AccessFlag> srcAccessMask;
    public final Set<AccessFlag> dstAccessMask;
    public final ImageLayout oldLayout;
    public final ImageLayout newLayout;
    public final QueueFamily srcQueueFamily;
    public final QueueFamily dstQueueFamily;
    public final Image image;
    public final ImageSubresourceRange subresourceRange;

    public ImageMemoryBarrier(
            final Set<AccessFlag> srcAccessMask, final Set<AccessFlag> dstAccessMask,
            final ImageLayout oldLayout, final ImageLayout newLayout,
            final QueueFamily srcQueueFamily, final QueueFamily dstQueueFamily,
            final Image image, final ImageSubresourceRange subresourceRange) {

        this.srcAccessMask = Set.copyOf(srcAccessMask);
        this.dstAccessMask = Set.copyOf(dstAccessMask);
        this.oldLayout = oldLayout;
        this.newLayout = newLayout;
        this.srcQueueFamily = srcQueueFamily;
        this.dstQueueFamily = dstQueueFamily;
        this.image = image;
        this.subresourceRange = subresourceRange;
    }

    public ImageMemoryBarrier() {
        this(Set.of(), Set.of(), ImageLayout.UNDEFINED, ImageLayout.UNDEFINED, null, null, null, null);
    }

    public ImageMemoryBarrier withSrcAccessMask(final Set<AccessFlag> srcAccessMask) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    public ImageMemoryBarrier withSrcAccessMask(final AccessFlag... srcAccessMask) {
        return withSrcAccessMask(Set.of(srcAccessMask));
    }

    public ImageMemoryBarrier withDstAccessMask(final Set<AccessFlag> dstAccessMask) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    public ImageMemoryBarrier withDstAccessMask(final AccessFlag... dstAccessMask) {
        return withDstAccessMask(Set.of(dstAccessMask));
    }

    public ImageMemoryBarrier withOldLayout(final ImageLayout oldLayout) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    public ImageMemoryBarrier withNewLayout(final ImageLayout newLayout) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    public ImageMemoryBarrier withSrcQueueFamily(final QueueFamily srcQueueFamily) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    public ImageMemoryBarrier withDstQueueFamily(final QueueFamily dstQueueFamily) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    public ImageMemoryBarrier withQueueFamilyTransfer(final QueueFamily srcQueueFamily, final QueueFamily dstQueueFamily) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    public ImageMemoryBarrier withImage(final Image image) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    public ImageMemoryBarrier withSubresourceRange(final ImageSubresourceRange subresourceRange) {
        return new ImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }

    VkImageMemoryBarrier deserialize(final VkImageMemoryBarrier vkobj) {
        vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                .srcAccessMask(AccessFlag.toBitfield(srcAccessMask))
                .dstAccessMask(AccessFlag.toBitfield(dstAccessMask))
                .oldLayout(oldLayout.value)
                .newLayout(newLayout.value)
                .srcQueueFamilyIndex(QueueFamily.indexOf(this.srcQueueFamily))
                .dstQueueFamilyIndex(QueueFamily.indexOf(this.dstQueueFamily))
                .image(image.handle);

        subresourceRange.deserialize(vkobj.subresourceRange());

        return vkobj;
    }
}
