package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkCommandBufferAllocateInfo;
import org.lwjgl.vulkan.VkCommandPoolCreateInfo;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class CommandPool {
    public final Set<CommandPoolCreateFlag> flags;
    public final long handle;
    private final WeakReference<QueueFamily> queueFamily;

    CommandPool(final QueueFamily queueFamily, final Set<CommandPoolCreateFlag> flags) {
        this.flags = Set.copyOf(flags);
        this.queueFamily = new WeakReference<>(queueFamily);

        final var device = queueFamily.getDevice();

        try (var mem = MemoryStack.stackPush()) {
            final var pvkCommandPoolCI = VkCommandPoolCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO)
                    .flags(CommandPoolCreateFlag.toBitfield(flags))
                    .queueFamilyIndex(queueFamily.index);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateCommandPool(device.handle, pvkCommandPoolCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    public Device getDevice() {
        return this.getQueueFamily().getDevice();
    }

    public QueueFamily getQueueFamily() {
        return Objects.requireNonNull(this.queueFamily.get(), "QueueFamily was lost!");
    }

    public void free() {
        VK10.vkDestroyCommandPool(this.getDevice().handle, this.handle, null);
    }

    public void reset(final int flags) {
        VK10.vkResetCommandPool(this.getDevice().handle, this.handle, flags);
    }

    public CommandBuffer allocate(final CommandBufferLevel level) {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkCommandBufferAI = VkCommandBufferAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
                    .commandPool(this.handle)
                    .level(level.value)
                    .commandBufferCount(1);

            final var pHandle = mem.callocPointer(1);

            vkAssert(VK10.vkAllocateCommandBuffers(this.getDevice().handle, pvkCommandBufferAI, pHandle));

            return new CommandBuffer(this, level, pHandle.get());
        }
    }

    public List<CommandBuffer> allocate(final CommandBufferLevel level, final int count) {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkCommandBufferAI = VkCommandBufferAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
                    .commandPool(this.handle)
                    .level(level.value)
                    .commandBufferCount(count);

            final var pHandle = mem.callocPointer(count);

            vkAssert(VK10.vkAllocateCommandBuffers(this.getDevice().handle, pvkCommandBufferAI, pHandle));

            return BufferStreams.of(pHandle)
                    .mapToObj(handle -> new CommandBuffer(this, level, handle))
                    .collect(Collectors.toList());
        }
    }
}
