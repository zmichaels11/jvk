package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum ImageTiling {
    OPTIMAL(VK10.VK_IMAGE_TILING_OPTIMAL),
    LINEAR(VK10.VK_IMAGE_TILING_LINEAR);

    final int value;

    ImageTiling(final int value) {
        this.value = value;
    }
}
