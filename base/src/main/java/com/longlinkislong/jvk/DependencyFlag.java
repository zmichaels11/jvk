package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK11;

import java.util.Set;

public enum DependencyFlag {
    BY_REGION(VK11.VK_DEPENDENCY_BY_REGION_BIT),
    DEVICE_GROUP(VK11.VK_DEPENDENCY_DEVICE_GROUP_BIT),
    VIEW_LOCAL(VK11.VK_DEPENDENCY_VIEW_LOCAL_BIT);

    final int value;

    DependencyFlag(final int value) {
        this.value = value;
    }

    static int toBitfield(final Set<DependencyFlag> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .sum();
    }
}
