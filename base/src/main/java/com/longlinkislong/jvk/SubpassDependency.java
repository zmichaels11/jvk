package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkSubpassDependency;

import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

public final class SubpassDependency {
    public final int srcSubpass;
    public final int dstSubpass;
    public final Set<PipelineStageFlag> srcStageMask;
    public final Set<PipelineStageFlag> dstStageMask;
    public final Set<AccessFlag> srcAccessMask;
    public final Set<AccessFlag> dstAccessMask;
    public final Set<DependencyFlag> dependencyFlags;

    public SubpassDependency(
            final int srcSubpass, final int dstSubpass,
            final Set<PipelineStageFlag> srcStageMask, final Set<PipelineStageFlag> dstStageMask,
            final Set<AccessFlag> srcAccessMask, final Set<AccessFlag> dstAccessMask,
            final Set<DependencyFlag> dependencyFlags) {

        this.srcSubpass = srcSubpass;
        this.dstSubpass = dstSubpass;
        this.srcStageMask = Set.copyOf(srcStageMask);
        this.dstStageMask = Set.copyOf(dstStageMask);
        this.srcAccessMask = Set.copyOf(srcAccessMask);
        this.dstAccessMask = Set.copyOf(dstAccessMask);
        this.dependencyFlags = Set.copyOf(dependencyFlags);
    }

    public SubpassDependency() {
        this(SUBPASS_EXTERNAL, SUBPASS_EXTERNAL, Set.of(PipelineStageFlag.BOTTOM_OF_PIPE), Set.of(PipelineStageFlag.BOTTOM_OF_PIPE), Set.of(), Set.of(), Set.of(DependencyFlag.BY_REGION));
    }

    public SubpassDependency withSrcSubpass(final int srcSubpass) {
        return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
    }

    public SubpassDependency withDstSubpass(final int dstSubpass) {
        return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
    }

    public SubpassDependency withSrcStageMask(final Set<PipelineStageFlag> srcStageMask) {
        return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
    }

    public SubpassDependency withSrcStageMask(final PipelineStageFlag... srcStageMask) {
        return withSrcStageMask(Set.of(srcStageMask));
    }

    public SubpassDependency withDstStageMask(final Set<PipelineStageFlag> dstStageMask) {
        return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
    }

    public SubpassDependency withDstStageMask(final PipelineStageFlag... dstStageMask) {
        return withDstStageMask(Set.of(dstStageMask));
    }

    public SubpassDependency withSrcAccessMask(final Set<AccessFlag> srcAccessMask) {
        return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
    }

    public SubpassDependency withSrcAccessMask(final AccessFlag... srcAccessMask) {
        return withSrcAccessMask(Set.of(srcAccessMask));
    }

    public SubpassDependency withDstAccessMask(final Set<AccessFlag> dstAccessMask) {
        return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
    }

    public SubpassDependency withDstAccessMask(final AccessFlag... dstAccessMask) {
        return withDstAccessMask(Set.of(dstAccessMask));
    }

    public SubpassDependency withDependencyFlags(final Set<DependencyFlag> dependencyFlags) {
        return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
    }

    public SubpassDependency withDependencyFlags(final DependencyFlag... dependencyFlags) {
        return withDependencyFlags(Set.of(dependencyFlags));
    }

    VkSubpassDependency deserialize(final VkSubpassDependency vkobj) {
        return vkobj.set(srcSubpass, dstSubpass, PipelineStageFlag.toBitfield(srcStageMask), PipelineStageFlag.toBitfield(dstStageMask), AccessFlag.toBitfield(srcAccessMask), AccessFlag.toBitfield(dstAccessMask), DependencyFlag.toBitfield(dependencyFlags));
    }

    static VkSubpassDependency.Buffer deserialize(final List<SubpassDependency> list, final MemoryStack mem) {
        final var out = VkSubpassDependency.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id)));

        return out;
    }

    public static final int SUBPASS_EXTERNAL = VK10.VK_SUBPASS_EXTERNAL;
}
