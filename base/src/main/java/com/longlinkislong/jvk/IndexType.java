package com.longlinkislong.jvk;

import static org.lwjgl.vulkan.VK10.VK_INDEX_TYPE_UINT16;
import static org.lwjgl.vulkan.VK10.VK_INDEX_TYPE_UINT32;

public enum IndexType {
    UINT16(VK_INDEX_TYPE_UINT16),
    UINT32(VK_INDEX_TYPE_UINT32);

    final int value;

    IndexType(final int value) {
        this.value = value;
    }
}
