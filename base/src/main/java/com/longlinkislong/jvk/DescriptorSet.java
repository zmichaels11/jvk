package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDescriptorBufferInfo;
import org.lwjgl.vulkan.VkDescriptorImageInfo;
import org.lwjgl.vulkan.VkWriteDescriptorSet;

import java.lang.ref.WeakReference;
import java.util.Objects;

public final class DescriptorSet {
    public final long handle;
    private final WeakReference<DescriptorPool> pool;
    final int poolIndex;

    DescriptorSet(final DescriptorPool pool, final int poolIndex, final long handle) {
        this.pool = new WeakReference<>(pool);
        this.poolIndex = poolIndex;
        this.handle = handle;
    }

    public DescriptorPool getDescriptorPool() {
        return Objects.requireNonNull(this.pool.get(), "DescriptorPool was lost!");
    }

    public Device getDevice() {
        return this.getDescriptorPool().getDevice();
    }

    public void free() {
        this.getDescriptorPool().freeDescriptorSet(this);
    }

    public DescriptorSet writeBuffer(final DescriptorType type, final int binding, final Buffer buffer) {
        return this.writeBuffer(type, binding, buffer, 0L, VK10.VK_WHOLE_SIZE);
    }

    public DescriptorSet writeBuffer(final DescriptorType type, final int binding, final Buffer buffer, final long offset, final long size) {
        try (var mem = MemoryStack.stackPush()) {
            final var pBufferInfo = VkDescriptorBufferInfo.callocStack(1, mem);

            pBufferInfo.get(0).set(buffer.handle, offset, size);

            final var pvkWriteDescriptorSet = VkWriteDescriptorSet.callocStack(1, mem);

            pvkWriteDescriptorSet.get(0)
                    .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                    .dstSet(this.handle)
                    .dstBinding(binding)
                    .descriptorType(type.value)
                    .pBufferInfo(pBufferInfo);

            VK10.vkUpdateDescriptorSets(this.getDevice().handle, pvkWriteDescriptorSet, null);
        }

        return this;
    }

    public DescriptorSet writeTexelBuffer(final DescriptorType type, final int binding, final BufferView view) {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkWriteDescriptorSet = VkWriteDescriptorSet.callocStack(1, mem);

            pvkWriteDescriptorSet.get(0)
                    .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                    .dstSet(this.handle)
                    .dstBinding(binding)
                    .descriptorType(type.value)
                    .pTexelBufferView(mem.longs(view.handle));

            VK10.vkUpdateDescriptorSets(this.getDevice().handle, pvkWriteDescriptorSet, null);
        }

        return this;
    }

    public DescriptorSet writeImage(
            final DescriptorType type, final int binding,
            final Sampler sampler, final ImageLayout imageLayout, final ImageView imageView) {

        try (var mem = MemoryStack.stackPush()) {
            final var pImageInfo = VkDescriptorImageInfo.callocStack(1, mem);

            if (null != sampler) {
                pImageInfo.sampler(sampler.handle);
            }

            pImageInfo.imageView(imageView.handle)
                    .imageLayout(imageLayout.value)
                    .imageView(imageView.handle);

            final var pvkWriteDescriptorSet = VkWriteDescriptorSet.callocStack(1, mem);

            pvkWriteDescriptorSet.get(0)
                    .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                    .dstSet(this.handle)
                    .dstBinding(binding)
                    .descriptorType(type.value)
                    .pImageInfo(pImageInfo);

            VK10.vkUpdateDescriptorSets(this.getDevice().handle, pvkWriteDescriptorSet, null);
        }

        return this;
    }
}
