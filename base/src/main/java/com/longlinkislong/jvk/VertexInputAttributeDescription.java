package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkVertexInputAttributeDescription;

import java.util.List;
import java.util.stream.IntStream;

public final class VertexInputAttributeDescription {
    public final int location;
    public final int binding;
    public final Format format;
    public final int offset;

    public VertexInputAttributeDescription(final int location, final int binding, final Format format, final int offset) {
        this.location = location;
        this.binding = binding;
        this.format = format;
        this.offset = offset;
    }

    public VertexInputAttributeDescription() {
        this(0, 0, null, 0);
    }

    public VertexInputAttributeDescription withLocation(final int location) {
        return new VertexInputAttributeDescription(location, binding, format, offset);
    }

    public VertexInputAttributeDescription withBinding(final int binding) {
        return new VertexInputAttributeDescription(location, binding, format, offset);
    }

    public VertexInputAttributeDescription withFormat(final Format format) {
        return new VertexInputAttributeDescription(location, binding, format, offset);
    }

    public VertexInputAttributeDescription withOffset(final int offset) {
        return new VertexInputAttributeDescription(location, binding, format, offset);
    }

    VkVertexInputAttributeDescription deserialize(final VkVertexInputAttributeDescription vkobj) {
        return vkobj.set(location, binding, format.value, offset);
    }

    static VkVertexInputAttributeDescription.Buffer deserialize(final List<VertexInputAttributeDescription> list, final MemoryStack mem) {
        final var out = VkVertexInputAttributeDescription.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id)));

        return out;
    }
}
