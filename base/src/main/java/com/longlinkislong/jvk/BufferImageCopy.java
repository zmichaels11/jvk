package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkBufferImageCopy;

public final class BufferImageCopy {
    public final long bufferOffset;
    public final int bufferImageHeight;
    public final int bufferRowLength;
    public final ImageSubresourceLayers imageSubresource;
    public final Offset3D imageOffset;
    public final Extent3D imageExtent;

    public BufferImageCopy(final long bufferOffset, final int bufferRowLength, final int bufferImageHeight, final ImageSubresourceLayers imageSubresource, final Offset3D imageOffset, final Extent3D imageExtent) {
        this.bufferOffset = bufferOffset;
        this.bufferRowLength = bufferRowLength;
        this.bufferImageHeight = bufferImageHeight;
        this.imageSubresource = imageSubresource;
        this.imageOffset = imageOffset;
        this.imageExtent = imageExtent;
    }

    public BufferImageCopy() {
        this(0L, 0, 0, null, Offset3D.ZERO, null);
    }

    public BufferImageCopy withBufferOffset(final long bufferOffset) {
        return new BufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffset, imageExtent);
    }

    public BufferImageCopy withBufferImageHeight(final int bufferImageHeight) {
        return new BufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffset, imageExtent);
    }

    public BufferImageCopy withBufferRowLength(final int bufferRowLength) {
        return new BufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffset, imageExtent);
    }

    public BufferImageCopy withImageSubresource(final ImageSubresourceLayers imageSubresource) {
        return new BufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffset, imageExtent);
    }

    public BufferImageCopy withImageOffset(final Offset3D imageOffset) {
        return new BufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffset, imageExtent);
    }

    public BufferImageCopy withImageOffset(final int x, final int y, final int z) {
        return withImageOffset(new Offset3D(x, y, z));
    }

    public BufferImageCopy withImageExtent(final Extent3D imageExtent) {
        return new BufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffset, imageExtent);
    }

    public BufferImageCopy withImageExtent(final int width, final int height, final int depth) {
        return withImageExtent(new Extent3D(width, height, depth));
    }

    VkBufferImageCopy deserialize(final VkBufferImageCopy vkobj) {
        vkobj.bufferOffset(bufferOffset)
                .bufferRowLength(bufferRowLength)
                .bufferImageHeight(bufferImageHeight);

        imageSubresource.deserialize(vkobj.imageSubresource());
        imageOffset.deserialize(vkobj.imageOffset());
        imageExtent.deserialize(vkobj.imageExtent());

        return vkobj;
    }
}
