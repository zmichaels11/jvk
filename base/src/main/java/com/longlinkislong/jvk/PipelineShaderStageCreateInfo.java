package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineShaderStageCreateInfo;
import org.lwjgl.vulkan.VkSpecializationInfo;

import java.util.Objects;

public final class PipelineShaderStageCreateInfo {
    public final int flags;
    public final ShaderStage stage;
    public final ShaderModule.CreateInfo moduleInfo;
    public final String name;
    public final SpecializationInfo specializationInfo;

    public PipelineShaderStageCreateInfo(
            final int flags,
            final ShaderStage stage,
            final ShaderModule.CreateInfo moduleInfo,
            final String name,
            final SpecializationInfo specializationInfo) {

        this.flags = flags;
        this.stage = stage;
        this.moduleInfo = moduleInfo;
        this.name = name;
        this.specializationInfo = specializationInfo;
    }

    public PipelineShaderStageCreateInfo() {
        this(0, null, null, "main", null);
    }

    public PipelineShaderStageCreateInfo withFlags(final int flags) {
        return new PipelineShaderStageCreateInfo(flags, stage, moduleInfo, name, specializationInfo);
    }

    public PipelineShaderStageCreateInfo withStage(final ShaderStage stage) {
        return new PipelineShaderStageCreateInfo(flags, stage, moduleInfo, name, specializationInfo);
    }

    public PipelineShaderStageCreateInfo withModuleInfo(final ShaderModule.CreateInfo moduleInfo) {
        return new PipelineShaderStageCreateInfo(flags, stage, moduleInfo, name, specializationInfo);
    }

    public PipelineShaderStageCreateInfo withName(final String name) {
        return new PipelineShaderStageCreateInfo(flags, stage, moduleInfo, name, specializationInfo);
    }

    public PipelineShaderStageCreateInfo withSpecializationInfo(final SpecializationInfo specializationInfo) {
        return new PipelineShaderStageCreateInfo(flags, stage, moduleInfo, name, specializationInfo);
    }

    VkPipelineShaderStageCreateInfo deserialize(final VkPipelineShaderStageCreateInfo vkobj, final Device device, final MemoryStack mem) {
        vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
                .flags(flags)
                .stage(stage.value)
                .module(device.getShaderModule(moduleInfo).handle);

        if (null == name || name.isEmpty()) {
            vkobj.pName(mem.UTF8("main"));
        } else {
            vkobj.pName(mem.UTF8(name));
        }

        if (null != this.specializationInfo) {
            final var pSpecializationInfo = VkSpecializationInfo.callocStack(mem);

            vkobj.pSpecializationInfo(specializationInfo.deserialize(pSpecializationInfo, mem));
        }

        return vkobj;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PipelineShaderStageCreateInfo that = (PipelineShaderStageCreateInfo) o;
        return flags == that.flags &&
                stage == that.stage &&
                Objects.equals(moduleInfo, that.moduleInfo) &&
                Objects.equals(name, that.name) &&
                Objects.equals(specializationInfo, that.specializationInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flags, stage, moduleInfo, name, specializationInfo);
    }
}
