package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkOffset2D;

public final class Offset2D {
    public final int x;
    public final int y;

    public Offset2D(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    VkOffset2D deserialize(final VkOffset2D vkobj) {
        return vkobj.set(x, y);
    }

    public static final Offset2D ZERO = new Offset2D(0, 0);
}
