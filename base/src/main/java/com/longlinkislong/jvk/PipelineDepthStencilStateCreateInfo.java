package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineDepthStencilStateCreateInfo;

public final class PipelineDepthStencilStateCreateInfo {
    public final int flags;
    public final boolean depthTestEnable;
    public final boolean depthWriteEnable;
    public final CompareOp depthCompareOp;
    public final boolean depthBoundsTestEnable;
    public final boolean stencilTestEnable;
    public final StencilOpState front;
    public final StencilOpState back;
    public final float minDepthBounds;
    public final float maxDepthBounds;

    public PipelineDepthStencilStateCreateInfo(
            final int flags,
            final boolean depthTestEnable, final boolean depthWriteEnable,
            final CompareOp depthCompareOp,
            final boolean depthBoundsTestEnable, final boolean stencilTestEnable,
            final StencilOpState front, final StencilOpState back,
            final float minDepthBounds, final float maxDepthBounds) {

        this.flags = flags;
        this.depthTestEnable = depthTestEnable;
        this.depthWriteEnable = depthWriteEnable;
        this.depthCompareOp = depthCompareOp;
        this.depthBoundsTestEnable = depthBoundsTestEnable;
        this.stencilTestEnable = stencilTestEnable;
        this.front = front;
        this.back = back;
        this.minDepthBounds = minDepthBounds;
        this.maxDepthBounds = maxDepthBounds;
    }

    public PipelineDepthStencilStateCreateInfo() {
        this(0, false, false, CompareOp.ALWAYS, false, false, new StencilOpState(), new StencilOpState(), 0.0F, 1.0F);
    }

    VkPipelineDepthStencilStateCreateInfo deserialize(VkPipelineDepthStencilStateCreateInfo vkobj) {
        front.deserialize(vkobj.front());
        back.deserialize(vkobj.back());

        return vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO)
                .pNext(VK10.VK_NULL_HANDLE)
                .flags(flags)
                .depthTestEnable(depthTestEnable)
                .depthWriteEnable(depthWriteEnable)
                .depthCompareOp(depthCompareOp.value)
                .depthBoundsTestEnable(depthBoundsTestEnable)
                .stencilTestEnable(stencilTestEnable)
                .minDepthBounds(minDepthBounds)
                .maxDepthBounds(maxDepthBounds);
    }
}
