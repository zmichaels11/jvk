package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkSamplerCreateInfo;

import java.lang.ref.WeakReference;
import java.util.Objects;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class Sampler {
    public static final class CreateInfo {
        public final int flags;
        public final Filter minFilter;
        public final Filter magFilter;
        public final SamplerMipmapFilter mipmapFilter;
        public final SamplerAddressMode addressModeU;
        public final SamplerAddressMode addressModeV;
        public final SamplerAddressMode addressModeW;
        public final float mipLodBias;
        public final boolean anistropyEnable;
        public final float maxAnisotropy;
        public final boolean compareEnable;
        public final CompareOp compareOp;
        public final float minLod;
        public final float maxLod;
        public final BorderColor borderColor;
        public final boolean unnormalizedCoordinates;

        public CreateInfo(
                final int flags,
                final Filter minFilter, final Filter magFilter, final SamplerMipmapFilter mipmapFilter,
                final SamplerAddressMode addressModeU, final SamplerAddressMode addressModeV, final SamplerAddressMode addressModeW,
                final float mipLodBias,
                final boolean anistropyEnable, final float maxAnisotropy,
                final boolean compareEnable, final CompareOp compareOp,
                final float maxLod, final float minLod,
                final BorderColor borderColor,
                final boolean unnormalizedCoordinates) {

            this.flags = flags;
            this.minFilter = minFilter;
            this.magFilter = magFilter;
            this.mipmapFilter = mipmapFilter;
            this.addressModeU = addressModeU;
            this.addressModeV = addressModeV;
            this.addressModeW = addressModeW;
            this.mipLodBias = mipLodBias;
            this.anistropyEnable = anistropyEnable;
            this.maxAnisotropy = maxAnisotropy;
            this.compareEnable = compareEnable;
            this.compareOp = compareOp;
            this.maxLod = maxLod;
            this.minLod = minLod;
            this.borderColor = borderColor;
            this.unnormalizedCoordinates = unnormalizedCoordinates;
        }

        public CreateInfo() {
            this(0,
                    Filter.NEAREST, Filter.NEAREST, SamplerMipmapFilter.NEAREST,
                    SamplerAddressMode.REPEAT, SamplerAddressMode.REPEAT, SamplerAddressMode.REPEAT,
                    0.0F,
                    false, 1.0F,
                    false, CompareOp.ALWAYS,
                    1000.0F, -1000.0F,
                    BorderColor.FLOAT_TRANSPARENT_BLACK,
                    false);
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }

        public CreateInfo withFilters(final Filter minFilter, final Filter magFilter, final SamplerMipmapFilter mipmapFilter) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }

        public CreateInfo withMipLodBias(final float mipLodBias) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }

        public CreateInfo withAddressMode(final SamplerAddressMode addressModeU, final SamplerAddressMode addressModeV, final SamplerAddressMode addressModeW) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }

        public CreateInfo withAnisotropy(final boolean anisotropyEnable, final float maxAnisotropy) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }

        public CreateInfo withCompareOp(final boolean compareEnable, final CompareOp compareOp) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }

        public CreateInfo withLod(final float maxLod, final float minLod) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }

        public CreateInfo withBorderColor(final BorderColor borderColor) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }

        public CreateInfo withUnnormalizedCoordinates(final boolean unnormalizedCoordinates) {
            return new CreateInfo(flags, minFilter, magFilter, mipmapFilter, addressModeU, addressModeV, addressModeW, mipLodBias, anistropyEnable, maxAnisotropy, compareEnable, compareOp, maxLod, minLod, borderColor, unnormalizedCoordinates);
        }
    }

    public final CreateInfo info;
    public final long handle;
    private final WeakReference<SamplerCache> cache;

    Sampler(final SamplerCache cache, final CreateInfo info) {
        this.cache = new WeakReference<>(cache);
        this.info = info;

        try (var mem = MemoryStack.stackPush()) {
            final var pvkSamplerCI = VkSamplerCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO)
                    .magFilter(info.magFilter.value)
                    .minFilter(info.minFilter.value)
                    .anisotropyEnable(info.anistropyEnable)
                    .maxAnisotropy(info.maxAnisotropy)
                    .maxLod(info.maxLod)
                    .minLod(info.minLod)
                    .borderColor(info.borderColor.value)
                    .unnormalizedCoordinates(info.unnormalizedCoordinates);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateSampler(cache.getDevice().handle, pvkSamplerCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    public SamplerCache getSamplerCache() {
        return Objects.requireNonNull(this.cache.get(), "SamplerCache was lost!");
    }

    public Device getDevice() {
        return this.getSamplerCache().getDevice();
    }

    void free() {
        VK10.vkDestroySampler(this.getDevice().handle, this.handle, null);
    }

    public void release() {
        this.getSamplerCache().release(this);
    }
}
