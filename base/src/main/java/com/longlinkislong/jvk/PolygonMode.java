package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum PolygonMode {
    FILL(VK10.VK_POLYGON_MODE_FILL),
    LINE(VK10.VK_POLYGON_MODE_LINE),
    POINT(VK10.VK_POLYGON_MODE_POINT);

    final int value;

    PolygonMode(final int value) {
        this.value = value;
    }
}
