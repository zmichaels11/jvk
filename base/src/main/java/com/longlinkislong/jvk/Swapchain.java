package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.*;

import java.lang.ref.WeakReference;
import java.nio.IntBuffer;
import java.util.*;
import java.util.stream.Collectors;

import static com.longlinkislong.jvk.Util.clamp;
import static com.longlinkislong.jvk.Util.vkAssert;

public final class Swapchain {
    private static final long SLEEP_INTERVAL = Long.getLong("jvk.Swapchain.SLEEP_INTERVAL", 1L);
    public static final class CreateInfo {
        public final long surface;
        public final SurfaceFormat surfaceFormat;
        public final QueueFamily queueFamily;
        public final int width;
        public final int height;

        public CreateInfo(final long surface, final SurfaceFormat surfaceFormat, final QueueFamily queueFamily, final int width, final int height) {
            this.surface = surface;
            this.surfaceFormat = surfaceFormat;
            this.queueFamily = queueFamily;
            this.width = width;
            this.height = height;
        }

        public CreateInfo() {
            this(0L, SurfaceFormat.DEFAULT, null, 640, 480);
        }

        public CreateInfo withSurface(final long surface) {
            return new CreateInfo(surface, surfaceFormat, queueFamily, width, height);
        }

        public CreateInfo withSurfaceFormat(final SurfaceFormat surfaceFormat) {
            return new CreateInfo(surface, surfaceFormat, queueFamily, width, height);
        }

        public CreateInfo withQueueFamily(final QueueFamily queueFamily) {
            return new CreateInfo(surface, surfaceFormat, queueFamily, width, height);
        }

        public CreateInfo withSize(final int width, final int height) {
            return new CreateInfo(surface, surfaceFormat, queueFamily, width, height);
        }
    }

    public final class Backbuffer {
        public final Image image;
        public final Semaphore acquireSemaphore;
        public final int index;

        private Backbuffer(final Image image, final Semaphore acquireSemaphore, final int index) {
            this.image = image;
            this.acquireSemaphore = acquireSemaphore;
            this.index = index;
        }
    }

    private static final class Support {
        private final VkSurfaceCapabilitiesKHR capabilities;
        private final VkSurfaceFormatKHR.Buffer surfaceFormats;
        private final IntBuffer presentModes;

        private Support(final VkPhysicalDevice physicalDevice, final long surface) {
            this.capabilities = VkSurfaceCapabilitiesKHR.calloc();

            try (var mem = MemoryStack.stackPush()) {
                KHRSurface.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, this.capabilities);

                final var pFormatCount = mem.callocInt(1);

                vkAssert(KHRSurface.vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, pFormatCount, null));

                this.surfaceFormats = VkSurfaceFormatKHR.calloc(pFormatCount.get(0));

                vkAssert(KHRSurface.vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, pFormatCount, this.surfaceFormats));

                final var pPresentModeCount = mem.callocInt(1);

                vkAssert(KHRSurface.vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, pPresentModeCount, null));

                this.presentModes = MemoryUtil.memCallocInt(pPresentModeCount.get(0));

                vkAssert(KHRSurface.vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, pPresentModeCount, this.presentModes));
            }
        }

        private void free() {
            this.capabilities.free();
            this.surfaceFormats.free();
            MemoryUtil.memFree(this.presentModes);
        }
    }

    public final CreateInfo info;
    private final WeakReference<Device> device;
    private long handle = VK10.VK_NULL_HANDLE;
    private PresentMode presentMode = PresentMode.VSYNC;
    private Support support;
    private int width;
    private int height;
    private List<Image> images = Collections.emptyList();
    public Object userData = null;

    Swapchain(final Device device, final CreateInfo createInfo) {
        this.device = new WeakReference<>(device);
        this.info = createInfo;

        final var physicalDevice = device.handle.getPhysicalDevice();

        try (var mem = MemoryStack.stackPush()) {
            final var pSupported = mem.callocInt(1);

            vkAssert(KHRSurface.vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, createInfo.queueFamily.index, createInfo.surface, pSupported));

            if (VK10.VK_TRUE != pSupported.get()) {
                throw new UnsupportedOperationException("KHRSurface is not supported on QueueFamily: " + createInfo.queueFamily);
            }


        }

        this.resize(createInfo.width, createInfo.height);
    }

    public List<Image> getImages() {
        return List.copyOf(this.images);
    }

    public OptionalLong getHandle() {
        return (VK10.VK_NULL_HANDLE == handle) ? OptionalLong.empty() : OptionalLong.of(this.handle);
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public Backbuffer acquireNextImage() throws InterruptedException {
        final var device = this.getDevice();
        final var imageAcquireSemaphore = device.acquireSemaphore();

        try (var mem = MemoryStack.stackPush()) {
            final var pImageIndex = mem.callocInt(1);
            final var fence = getDevice().acquireFence();
            final int result = KHRSwapchain.vkAcquireNextImageKHR(device.handle, this.handle, -1L, imageAcquireSemaphore.handle, fence.handle, pImageIndex);

            while (!fence.isSignaled()) {
                Thread.sleep(SLEEP_INTERVAL);
            }

            fence.resetFence();
            fence.release();

            if (KHRSwapchain.VK_ERROR_OUT_OF_DATE_KHR == result) {
                device.waitIdle();
                this.resize(this.width, this.height);

                return this.acquireNextImage();
            } else {
                vkAssert(result);
            }

            final var index = pImageIndex.get();
            final var image = this.images.get(index);

            return new Backbuffer(image, imageAcquireSemaphore, index);
        }
    }

    public void free() {
        if (VK10.VK_NULL_HANDLE != this.handle) {
            KHRSwapchain.vkDestroySwapchainKHR(this.getDevice().handle, this.handle, null);
            this.handle = VK10.VK_NULL_HANDLE;
            this.support.free();
        }
    }

    public void resize(final int width, final int height) {
        final var device = this.getDevice();
        final var physicalDevice = device.handle.getPhysicalDevice();
        final Format format;

        try (var mem = MemoryStack.stackPush()) {
            this.support = new Support(physicalDevice, this.info.surface);

            if (0 == this.support.surfaceFormats.remaining() || 0 == this.support.presentModes.remaining()) {
                throw new UnsupportedOperationException("Surface does not support any SurfaceFormats or PresentModes!");
            }

            final var surfaceFormat = chooseFormat(this.support.surfaceFormats, this.info.surfaceFormat.format, this.info.surfaceFormat.colorSpace);

            format = Arrays.stream(Format.values())
                    .filter(fmt -> fmt == surfaceFormat.format)
                    .findFirst()
                    .orElse(Format.B8G8R8A8_UNORM);

            final int presentMode;

            switch (this.presentMode) {
                case IMMEDIATE:
                    presentMode = choosePresentMode(this.support.presentModes, KHRSurface.VK_PRESENT_MODE_IMMEDIATE_KHR);
                    break;
                case ADAPTIVE_VSYNC:
                    presentMode = choosePresentMode(this.support.presentModes, KHRSurface.VK_PRESENT_MODE_FIFO_RELAXED_KHR);
                    break;
                default:
                    presentMode = choosePresentMode(this.support.presentModes, KHRSurface.VK_PRESENT_MODE_FIFO_KHR);
                    break;
            }

            final int imageCount;

            if (this.support.capabilities.maxImageCount() > 0
                && this.support.capabilities.minImageCount() + 1 > this.support.capabilities.maxImageCount()) {

                imageCount = this.support.capabilities.maxImageCount();
            } else {
                imageCount = this.support.capabilities.minImageCount() + 1;
            }

            final int preTransform;

            if (0 != (this.support.capabilities.currentTransform() & KHRSurface.VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)) {
                preTransform = KHRSurface.VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
            } else {
                preTransform = this.support.capabilities.currentTransform();
            }

            final var extent = this.support.capabilities.currentExtent();
            final var minExtent = this.support.capabilities.minImageExtent();
            final var maxExtent = this.support.capabilities.maxImageExtent();

            if (-1 == extent.width() || -1 == extent.height()) {
                extent.set(width, height);
            }

            this.width = clamp(extent.width(), minExtent.width(), maxExtent.width());
            this.height = clamp(extent.height(), minExtent.height(), maxExtent.height());

            extent.set(width, height);

            if (support.capabilities.minImageExtent().width() > extent.width()) {
                extent.width(support.capabilities.minImageExtent().width());
            } else if (support.capabilities.maxImageExtent().width() < extent.width()) {
                extent.width(support.capabilities.maxImageExtent().width());
            }

            final var pvkSwapchainCI = VkSwapchainCreateInfoKHR.callocStack(mem)
                    .sType(KHRSwapchain.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR)
                    .pNext(MemoryUtil.NULL)
                    .surface(this.info.surface)
                    .minImageCount(imageCount)
                    .imageFormat(surfaceFormat.format.value)
                    .imageColorSpace(surfaceFormat.colorSpace.value)
                    .preTransform(preTransform)
                    .imageArrayLayers(1)
                    .imageSharingMode(VK10.VK_SHARING_MODE_EXCLUSIVE)
                    .pQueueFamilyIndices(null)
                    .presentMode(presentMode)
                    .oldSwapchain(this.handle)
                    .clipped(true)
                    .compositeAlpha(KHRSurface.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
                    .imageExtent(extent)
                    .imageUsage(ImageUsageFlag.toBitfield(Set.of(ImageUsageFlag.COLOR_ATTACHMENT, ImageUsageFlag.TRANSFER_DST)));

            final var pHandle = mem.callocLong(1);

            vkAssert(KHRSwapchain.vkCreateSwapchainKHR(device.handle, pvkSwapchainCI, null, pHandle));

            if (VK10.VK_NULL_HANDLE != this.handle) {
                KHRSwapchain.vkDestroySwapchainKHR(device.handle, this.handle, null);
            }

            this.handle = pHandle.get();
        }

        try (var mem = MemoryStack.stackPush()) {
            final var pImageCount = mem.callocInt(1);

            vkAssert(KHRSwapchain.vkGetSwapchainImagesKHR(device.handle, this.handle, pImageCount, null));

            final var pvkSwapchainImages = mem.callocLong(pImageCount.get(0));

            vkAssert(KHRSwapchain.vkGetSwapchainImagesKHR(device.handle, this.handle, pImageCount, pvkSwapchainImages));

            final var imageCI = new Image.CreateInfo(
                    0,
                    ImageType.IMAGE_2D,
                    format,
                    new Extent3D(this.width, this.height, 1),
                    1, 1,
                    VK10.VK_SAMPLE_COUNT_1_BIT,
                    ImageTiling.OPTIMAL,
                    Set.of(ImageUsageFlag.COLOR_ATTACHMENT, ImageUsageFlag.TRANSFER_DST),
                    SharingMode.EXCLUSIVE,
                    Collections.emptyList(),
                    ImageLayout.PRESENT_SRC_KHR);

            this.images = BufferStreams.of(pvkSwapchainImages)
                    .mapToObj(handle -> new Image(device, imageCI, handle))
                    .collect(Collectors.toList());
        }
    }

    private static SurfaceFormat chooseFormat(final VkSurfaceFormatKHR.Buffer availableFormats, final Format format, final ColorSpace colorSpace) {
        if (1 == availableFormats.remaining() && VK10.VK_FORMAT_UNDEFINED == availableFormats.get(0).format()) {
            return SurfaceFormat.DEFAULT;
        }

        try {
            availableFormats.mark();

            while (availableFormats.hasRemaining()) {
                final var current = availableFormats.get();

                if (format.value == current.format() && colorSpace.value == current.colorSpace()) {
                    return new SurfaceFormat(format, colorSpace);
                }
            }
        } finally {
            availableFormats.reset();
        }

        final var selectedFormat = Format.toFormat(availableFormats.get(0).format())
                .orElseThrow(() -> new UnsupportedOperationException("Unsupported Format: 0x" + Integer.toHexString(availableFormats.get(0).format())));

        final var selectedColorSpace = ColorSpace.toColorSpace(availableFormats.get(0).colorSpace())
                .orElseThrow(() -> new UnsupportedOperationException("Unsupported ColorSpace: 0x" + Integer.toHexString(availableFormats.get(0).colorSpace())));

        return new SurfaceFormat(selectedFormat, selectedColorSpace);
    }

    private static int choosePresentMode(final IntBuffer availablePresentModes, final int presentMode) {
        try {
            availablePresentModes.mark();

            while (availablePresentModes.hasRemaining()) {
                if (presentMode == availablePresentModes.get()) {
                    return presentMode;
                }
            }
        } finally {
            availablePresentModes.reset();
        }

        return (availablePresentModes.hasRemaining()) ? availablePresentModes.get(0) : KHRSurface.VK_PRESENT_MODE_FIFO_KHR;
    }
}
