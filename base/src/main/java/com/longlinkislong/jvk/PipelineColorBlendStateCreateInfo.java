package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineColorBlendStateCreateInfo;

import java.util.List;

public final class PipelineColorBlendStateCreateInfo {
    public final int flags;
    public final boolean logicOpEnable;
    public final LogicOp logicOp;
    public final List<PipelineColorBlendAttachmentState> attachments;
    public final Color blendConstants;

    public PipelineColorBlendStateCreateInfo(
            final int flags,
            final boolean logicOpEnable, final LogicOp logicOp,
            final List<PipelineColorBlendAttachmentState> attachments,
            final Color blendConstants) {

        this.flags = flags;
        this.logicOpEnable = logicOpEnable;
        this.logicOp = logicOp;
        this.attachments = List.copyOf(attachments);
        this.blendConstants = blendConstants;
    }

    public PipelineColorBlendStateCreateInfo() {
        this(0, false, LogicOp.NO_OP, List.of(), new Color(1.0F, 1.0F, 1.0F, 1.0F));
    }

    public PipelineColorBlendStateCreateInfo withFlags(final int flags) {
        return new PipelineColorBlendStateCreateInfo(flags, logicOpEnable, logicOp, attachments, blendConstants);
    }

    public PipelineColorBlendStateCreateInfo withLogicOpEnable(final boolean logicOpEnable) {
        return new PipelineColorBlendStateCreateInfo(flags, logicOpEnable, logicOp, attachments, blendConstants);
    }

    public PipelineColorBlendStateCreateInfo withLogicOp(final LogicOp logicOp) {
        return new PipelineColorBlendStateCreateInfo(flags, logicOpEnable, logicOp, attachments, blendConstants);
    }

    public PipelineColorBlendStateCreateInfo withAttachments(final List<PipelineColorBlendAttachmentState> attachments) {
        return new PipelineColorBlendStateCreateInfo(flags, logicOpEnable, logicOp, attachments, blendConstants);
    }

    public PipelineColorBlendStateCreateInfo withAttachments(final PipelineColorBlendAttachmentState... attachments) {
        return withAttachments(List.of(attachments));
    }

    public PipelineColorBlendStateCreateInfo withBlendConstants(final Color blendConstants) {
        return new PipelineColorBlendStateCreateInfo(flags, logicOpEnable, logicOp, attachments, blendConstants);
    }

    VkPipelineColorBlendStateCreateInfo deserialize(final VkPipelineColorBlendStateCreateInfo vkobj, final MemoryStack mem) {
        return vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO)
                .pNext(VK10.VK_NULL_HANDLE)
                .flags(flags)
                .logicOpEnable(logicOpEnable)
                .logicOp(logicOp.value)
                .pAttachments(PipelineColorBlendAttachmentState.deserialize(this.attachments, mem))
                .blendConstants(blendConstants.deserialize(mem.callocFloat(4)));
    }
}
