package com.longlinkislong.jvk;

import org.lwjgl.BufferUtils;

import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.*;
import java.util.stream.Collector;

abstract class LongBufferCollector implements Collector<Long, List<Long>, LongBuffer> {

    @Override
    public Supplier<List<Long>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<Long>, Long> accumulator() {
        return List::add;
    }

    @Override
    public BinaryOperator<List<Long>> combiner() {
        return (left, right) -> {
            left.addAll(right);

            return left;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of(Characteristics.UNORDERED);
    }

    public static LongBufferCollector allocate(final IntFunction<LongBuffer> constructor) {
        return new LongBufferCollector() {
            @Override
            public Function<List<Long>, LongBuffer> finisher() {
                return list -> {
                    final var out = constructor.apply(list.size());

                    list.forEach(out::put);
                    out.flip();

                    return out;
                };
            }
        };
    }

    static LongBufferCollector create() {
        return allocate(BufferUtils::createLongBuffer);
    }
}
