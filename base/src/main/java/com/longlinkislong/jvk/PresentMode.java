package com.longlinkislong.jvk;

import org.lwjgl.vulkan.KHRSurface;

public enum PresentMode {
    IMMEDIATE(KHRSurface.VK_PRESENT_MODE_IMMEDIATE_KHR),
    VSYNC(KHRSurface.VK_PRESENT_MODE_FIFO_KHR),
    ADAPTIVE_VSYNC(KHRSurface.VK_PRESENT_MODE_FIFO_RELAXED_KHR);

    final int value;

    PresentMode(final int value) {
        this.value = value;
    }
}
