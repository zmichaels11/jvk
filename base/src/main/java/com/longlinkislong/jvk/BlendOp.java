package com.longlinkislong.jvk;

import static org.lwjgl.vulkan.VK10.*;

public enum BlendOp {
    ADD(VK_BLEND_OP_ADD),
    SUBTRACT(VK_BLEND_OP_SUBTRACT),
    REVERSE_SUBTRACT(VK_BLEND_OP_REVERSE_SUBTRACT),
    MIN(VK_BLEND_OP_MIN),
    MAX(VK_BLEND_OP_MAX);

    final int value;

    BlendOp(final int value) {
        this.value = value;
    }
}
