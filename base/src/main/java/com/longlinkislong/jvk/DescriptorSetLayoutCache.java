package com.longlinkislong.jvk;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class DescriptorSetLayoutCache {
    private final class Layout {
        private final DescriptorSetLayout instance;
        private int references;

        private Layout(final DescriptorSetLayout instance) {
            this.instance = instance;
        }
    }

    private final WeakReference<Device> device;
    private final Object lock = new Object();
    private final Map<DescriptorSetLayout.CreateInfo, Layout> layouts = new HashMap<>();

    DescriptorSetLayoutCache(final Device device) {
        this.device = new WeakReference<>(device);
    }

    public void free() {
        synchronized (lock) {
            this.layouts.values().forEach(layout -> layout.instance.free());
            this.layouts.clear();
        }
    }

    public DescriptorSetLayout allocateDescriptorSetLayout(final DescriptorSetLayout.CreateInfo createInfo) {
        synchronized (lock) {
            final var layout = this.layouts.computeIfAbsent(createInfo, info -> new Layout(new DescriptorSetLayout(this, createInfo)));

            layout.references += 1;

            return layout.instance;
        }
    }

    public void releaseDescriptorSetLayout(final DescriptorSetLayout layout) {
        assert layout.getDescriporSetLayoutCache() == this;

        synchronized (lock) {
            final var dsl = this.layouts.get(layout.info);

            dsl.references -= 1;

            if (0 == dsl.references) {
                layout.free();
                this.layouts.remove(layout.info);
            }
        }
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }
}
