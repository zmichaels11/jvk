package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum FrontFace {
    COUNTER_CLOCKWISE(VK10.VK_FRONT_FACE_COUNTER_CLOCKWISE),
    CLOCKWISE(VK10.VK_FRONT_FACE_CLOCKWISE);

    final int value;

    FrontFace(final int value) {
        this.value = value;
    }
}
