package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkShaderModuleCreateInfo;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Objects;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class ShaderModule {
    public static final class CreateInfo {
        public final int flags;
        public final Path path;

        public CreateInfo(final int flags, final Path path) {
            this.flags = flags;
            this.path = path;
        }

        public CreateInfo() {
            this(0, null);
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, path);
        }

        public CreateInfo withPath(final Path path) {
            return new CreateInfo(flags, path);
        }
    }

    public final long handle;
    public final CreateInfo info;
    private final WeakReference<Device> device;

    ShaderModule(final Device device, final CreateInfo info) {
        this.device = new WeakReference<>(device);
        this.info = info;

        try (
                var mem = MemoryStack.stackPush();
                var fc = FileChannel.open(info.path, StandardOpenOption.READ)) {

            final var pvkShaderModuleCI = VkShaderModuleCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO)
                    .flags(info.flags)
                    .pCode(fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size()));

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateShaderModule(device.handle, pvkShaderModuleCI, null, pHandle));

            this.handle = pHandle.get();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public void free() {
        VK10.vkDestroyShaderModule(this.getDevice().handle, this.handle, null);
    }
}
