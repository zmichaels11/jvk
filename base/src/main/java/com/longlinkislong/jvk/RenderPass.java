package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkRenderPassCreateInfo;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class RenderPass {
    public static final class CreateInfo {
        public final int flags;
        public final List<AttachmentDescription> attachments;
        public final List<SubpassDescription> subpasses;
        public final List<SubpassDependency> dependencies;

        public CreateInfo(
                final int flags,
                final List<AttachmentDescription> attachments,
                final List<SubpassDescription> subpasses,
                final List<SubpassDependency> dependencies) {

            this.flags = flags;
            this.attachments = List.copyOf(attachments);
            this.subpasses = List.copyOf(subpasses);
            this.dependencies = List.copyOf(dependencies);
        }

        public CreateInfo() {
            this(0, List.of(), List.of(), List.of());
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, attachments, subpasses, dependencies);
        }

        public CreateInfo withAttachments(final List<AttachmentDescription> attachments) {
            return new CreateInfo(flags, attachments, subpasses, dependencies);
        }

        public CreateInfo withAttachments(final AttachmentDescription... attachments) {
            return withAttachments(List.of(attachments));
        }

        public CreateInfo withSubpasses(final List<SubpassDescription> subpasses) {
            return new CreateInfo(flags, attachments, subpasses, dependencies);
        }

        public CreateInfo withSubpasses(final SubpassDescription... subpasses) {
            return withSubpasses(List.of(subpasses));
        }

        public CreateInfo withDependencies(final List<SubpassDependency> dependencies) {
            return new CreateInfo(flags, attachments, subpasses, dependencies);
        }

        public CreateInfo withDependencies(final SubpassDependency... dependencies) {
            return withDependencies(List.of(dependencies));
        }
    }

    public final long handle;
    public final CreateInfo info;
    private final WeakReference<Device> device;

    public RenderPass(final Device device, final CreateInfo info) {
        this.device = new WeakReference<>(device);
        this.info = info;

        try (var mem = MemoryStack.stackPush()) {
            var pvkRenderPassCI = VkRenderPassCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO)
                    .flags(info.flags)
                    .pAttachments(AttachmentDescription.deserialize(info.attachments, mem))
                    .pSubpasses(SubpassDescription.deserialize(info.subpasses, mem))
                    .pDependencies(SubpassDependency.deserialize(info.dependencies, mem));

            var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateRenderPass(device.handle, pvkRenderPassCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public void free() {
        VK10.vkDestroyRenderPass(this.getDevice().handle, this.handle, null);
    }
}
