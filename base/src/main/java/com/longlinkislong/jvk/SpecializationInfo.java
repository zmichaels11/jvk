package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkSpecializationInfo;

import java.nio.ByteBuffer;
import java.util.List;

public final class SpecializationInfo {
    public final List<SpecializationMapEntry> mapEntries;
    public final ByteBuffer data;

    public SpecializationInfo(final List<SpecializationMapEntry> mapEntries, final ByteBuffer data) {
        this.mapEntries = List.copyOf(mapEntries);
        this.data = data;
    }

    VkSpecializationInfo deserialize(final VkSpecializationInfo vkobj, MemoryStack mem) {
        return vkobj.set(SpecializationMapEntry.deserialize(mapEntries, mem), data);
    }
}
