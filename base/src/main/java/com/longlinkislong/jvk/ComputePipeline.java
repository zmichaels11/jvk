package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkComputePipelineCreateInfo;
import org.lwjgl.vulkan.VkPipelineShaderStageCreateInfo;

import java.lang.ref.WeakReference;
import java.util.Objects;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class ComputePipeline implements Pipeline {
    public static final class CreateInfo {
        public final int flags;
        public final PipelineShaderStageCreateInfo stage;
        public final PipelineLayout.CreateInfo layoutInfo;

        public CreateInfo(
                final int flags,
                final PipelineShaderStageCreateInfo stage,
                final PipelineLayout.CreateInfo layoutInfo) {

            this.flags = flags;
            this.stage = stage;
            this.layoutInfo = layoutInfo;
        }

        public CreateInfo() {
            this(0, null, null);
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, stage, layoutInfo);
        }

        public CreateInfo withStage(final PipelineShaderStageCreateInfo stage) {
            return new CreateInfo(flags, stage, layoutInfo);
        }

        public CreateInfo withLayoutInfo(final PipelineLayout.CreateInfo layoutInfo) {
            return new CreateInfo(flags, stage, layoutInfo);
        }
    }

    public final CreateInfo info;
    public final long handle;
    private final WeakReference<PipelineCache> cache;
    private final WeakReference<PipelineLayout> layout;

    ComputePipeline(final PipelineCache cache, final CreateInfo createInfo) {
        this.cache = new WeakReference<>(cache);
        this.info = createInfo;

        final var device = cache.getDevice();

        try (var mem = MemoryStack.stackPush()) {
            final var layout = device.pipelineLayoutCache.allocatePipelineLayout(createInfo.layoutInfo);

            this.layout = new WeakReference<>(layout);

            final var pvkComputePipelineCI = VkComputePipelineCreateInfo.callocStack(1, mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO)
                    .flags(createInfo.flags)
                    .layout(layout.handle)
                    .stage(createInfo.stage.deserialize(VkPipelineShaderStageCreateInfo.callocStack(mem), device, mem));

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateComputePipelines(device.handle, cache.handle, pvkComputePipelineCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    @Override
    public long getHandle() {
        return this.handle;
    }

    @Override
    public PipelineBindpoint getBindpoint() {
        return PipelineBindpoint.COMPUTE;
    }

    @Override
    public PipelineLayout getPipelineLayout() {
        return Objects.requireNonNull(this.layout.get(), "PipelineLayout was lost!");
    }

    @Override
    public PipelineCache getPipelineCache() {
        return Objects.requireNonNull(this.cache.get(), "PipelineCache was lost!");
    }

    void free() {
        this.getPipelineLayout().release();
        VK10.vkDestroyPipeline(this.getDevice().handle, this.handle, null);
    }

    @Override
    public void release() {
        this.getPipelineCache().release(this);
    }
}
