package com.longlinkislong.jvk;

import org.lwjgl.vulkan.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;

public final class PhysicalDevice {
    public final VkPhysicalDevice handle;
    public final VkPhysicalDeviceProperties pvkProperties = VkPhysicalDeviceProperties.create();
    public final VkPhysicalDeviceFeatures pvkFeatures = VkPhysicalDeviceFeatures.create();
    public final VkPhysicalDeviceMemoryProperties pvkMemoryProperties = VkPhysicalDeviceMemoryProperties.create();

    PhysicalDevice(final VkPhysicalDevice handle) {
        this.handle = handle;

        VK10.vkGetPhysicalDeviceProperties(handle, pvkProperties);
        VK10.vkGetPhysicalDeviceFeatures(handle, pvkFeatures);
        VK10.vkGetPhysicalDeviceMemoryProperties(handle, pvkMemoryProperties);
    }

    public Device createDevice(final Set<String> enabledExtensions) {
        return new Device(this, enabledExtensions);
    }

    public Device createDevice(final String... enabledExtensions) {
        return createDevice(Set.of(enabledExtensions));
    }

    @Override
    public String toString() {
        final int apiVersion = pvkProperties.apiVersion();
        final int major = VK10.VK_VERSION_MAJOR(apiVersion);
        final int minor = VK10.VK_VERSION_MINOR(apiVersion);
        final int patch = VK10.VK_VERSION_PATCH(apiVersion);

        return String.format("Physical Device: {DeviceID=0x%x, VendorID=0x%x, DeviceName=%s, API Version=%d.%d.%d, DeviceType=%s, TotalRam: %.2fGB}",
                pvkProperties.deviceID(),
                pvkProperties.vendorID(),
                pvkProperties.deviceNameString(),
                major, minor, patch,
                getPhysicalDeviceType(),
                (getTotalDeviceRAM() * 1E-9));
    }

    public PhysicalDeviceType getPhysicalDeviceType() {
        final var deviceType = this.pvkProperties.deviceType();

        return Arrays.stream(PhysicalDeviceType.values())
                .filter(type -> type.value == deviceType)
                .findFirst()
                .orElseThrow(() -> new UnsupportedOperationException("Unsupported DeviceType: " + Integer.toHexString(deviceType)));
    }

    public long getTotalDeviceRAM() {
        return this.pvkMemoryProperties.memoryHeaps().stream()
                .filter(heap -> 0 != (heap.flags() & VK10.VK_MEMORY_HEAP_DEVICE_LOCAL_BIT))
                .mapToLong(VkMemoryHeap::size)
                .sum();
    }

    public static Comparator<PhysicalDevice> preferDiscreteGPU() {
        return (pDev0, pDev1) -> {
            final var type0 = pDev0.getPhysicalDeviceType();
            final var type1 = pDev1.getPhysicalDeviceType();

            switch (type0) {
                case DISCRETE_GPU:
                    if (type1 == type0) {
                        return 0;
                    } else {
                        return 1;
                    }
                default:
                    if (PhysicalDeviceType.DISCRETE_GPU == type1) {
                        return -1;
                    } else {
                        return 0;
                    }
            }
        };
    }

    public static Comparator<PhysicalDevice> preferIntegratedGPU() {
        return (pDev0, pDev1) -> {
            final var type0 = pDev0.getPhysicalDeviceType();
            final var type1 = pDev1.getPhysicalDeviceType();

            switch (type0) {
                case INTEGRATED_GPU:
                    if (type1 == type0) {
                        return 0;
                    } else {
                        return 1;
                    }
                default:
                    if (PhysicalDeviceType.INTEGRATED_GPU == type1) {
                        return -1;
                    } else {
                        return 0;
                    }
            }
        };
    }

    public static Comparator<PhysicalDevice> preferDeviceRAM() {
        return (pDev0, pDev1) -> Long.compareUnsigned(pDev0.getTotalDeviceRAM(), pDev1.getTotalDeviceRAM());
    }
}
