package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkAttachmentDescription;

import java.util.List;
import java.util.stream.IntStream;

public final class AttachmentDescription {
    public final int flags;
    public final Format format;
    public final int samples;
    public final LoadOp loadOp;
    public final StoreOp storeOp;
    public final LoadOp stencilLoadOp;
    public final StoreOp stencilStoreOp;
    public final ImageLayout initialLayout;
    public final ImageLayout finalLayout;

    public AttachmentDescription(
            final int flags,
            final Format format,
            final int samples,
            final LoadOp loadOp, final StoreOp storeOp,
            final LoadOp stencilLoadOp, final StoreOp stencilStoreOp,
            final ImageLayout initialLayout, final ImageLayout finalLayout) {

        this.flags = flags;
        this.format = format;
        this.samples = samples;
        this.loadOp = loadOp;
        this.storeOp = storeOp;
        this.stencilLoadOp = stencilLoadOp;
        this.stencilStoreOp = stencilStoreOp;
        this.initialLayout = initialLayout;
        this.finalLayout = finalLayout;
    }

    public AttachmentDescription() {
        this(0, null, VK10.VK_SAMPLE_COUNT_1_BIT, LoadOp.CLEAR, StoreOp.STORE, LoadOp.DONT_CARE, StoreOp.DONT_CARE, ImageLayout.UNDEFINED, ImageLayout.UNDEFINED);
    }

    public AttachmentDescription withFlags(final int flags) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    public AttachmentDescription withFormat(final Format format) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    public AttachmentDescription withSamples(final int samples) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    public AttachmentDescription withLoadOp(final LoadOp loadOp) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    public AttachmentDescription withStoreOp(final StoreOp storeOp) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    public AttachmentDescription withStencilLoadOp(final LoadOp stencilLoadOp) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    public AttachmentDescription withStencilStoreOp(final StoreOp stencilStoreOp) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    public AttachmentDescription withInitialLayout(final ImageLayout initialLayout) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    public AttachmentDescription withFinalLayout(final ImageLayout finalLayout) {
        return new AttachmentDescription(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
    }

    VkAttachmentDescription deserialize(final VkAttachmentDescription vkobj) {
        return vkobj.set(flags, format.value, samples, loadOp.value, storeOp.value, stencilLoadOp.value, stencilStoreOp.value, initialLayout.value, finalLayout.value);
    }

    static VkAttachmentDescription.Buffer deserialize(final List<AttachmentDescription> list, final MemoryStack mem) {
        final var out = VkAttachmentDescription.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id)));

        return out;
    }
}
