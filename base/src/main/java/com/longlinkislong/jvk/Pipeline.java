package com.longlinkislong.jvk;

public interface Pipeline {
    long getHandle();

    PipelineBindpoint getBindpoint();

    void release();

    PipelineCache getPipelineCache();

    PipelineLayout getPipelineLayout();

    default Device getDevice() {
        return this.getPipelineCache().getDevice();
    }
}
