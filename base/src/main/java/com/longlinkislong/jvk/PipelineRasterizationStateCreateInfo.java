package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineRasterizationStateCreateInfo;

public final class PipelineRasterizationStateCreateInfo {
    public final int flags;
    public final boolean depthClampEnable;
    public final boolean rasterizerDiscardEnable;
    public final PolygonMode polygonMode;
    public final CullMode cullMode;
    public final FrontFace frontFace;
    public final boolean depthBiasEnable;
    public final float depthBiasConstantFactor;
    public final float depthBiasClamp;
    public final float depthBiasSlopeFactor;
    public final float lineWidth;

    public PipelineRasterizationStateCreateInfo(
            final int flags,
            final boolean depthClampEnable, final boolean rasterizerDiscardEnable,
            final PolygonMode polygonMode, final CullMode cullMode, final FrontFace frontFace,
            final boolean depthBiasEnable, final float depthBiasConstantFactor,
            final float depthBiasClamp, final float depthBiasSlopeFactor,
            final float lineWidth) {

        this.flags = flags;
        this.depthClampEnable = depthClampEnable;
        this.rasterizerDiscardEnable = rasterizerDiscardEnable;
        this.polygonMode = polygonMode;
        this.cullMode = cullMode;
        this.frontFace = frontFace;
        this.depthBiasClamp = depthBiasClamp;
        this.depthBiasConstantFactor = depthBiasConstantFactor;
        this.depthBiasEnable = depthBiasEnable;
        this.depthBiasSlopeFactor = depthBiasSlopeFactor;
        this.lineWidth = lineWidth;
    }

    public PipelineRasterizationStateCreateInfo() {
        this(0, false, false, PolygonMode.FILL, CullMode.NONE, FrontFace.COUNTER_CLOCKWISE, false, 0.0F, 0.0F, 0.0F, 1.0F);
    }

    public PipelineRasterizationStateCreateInfo withFlags(final int flags) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withDepthClampEnable(final boolean depthClampEnable) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withRasterizerDiscardEnable(final boolean rasterizerDiscardEnable) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withPolygonMode(final PolygonMode polygonMode) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withCullMode(final CullMode cullMode) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withFrontFace(final FrontFace frontFace) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withDepthBiasEnable(final boolean depthBiasEnable) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withDepthBiasConstantFactor(final float depthBiasConstantFactor) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withDepthBiasClamp(final float depthBiasClamp) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withDepthBiasSlopeFactor(final float depthBiasSlopeFactor) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public PipelineRasterizationStateCreateInfo withLineWidth(final float lineWidth) {
        return new PipelineRasterizationStateCreateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    VkPipelineRasterizationStateCreateInfo deserialize(final VkPipelineRasterizationStateCreateInfo vkobj) {
        return vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO)
                .pNext(VK10.VK_NULL_HANDLE)
                .flags(flags)
                .depthClampEnable(depthClampEnable)
                .rasterizerDiscardEnable(rasterizerDiscardEnable)
                .polygonMode(polygonMode.value)
                .cullMode(cullMode.value)
                .frontFace(frontFace.value)
                .depthBiasEnable(depthBiasEnable)
                .depthBiasConstantFactor(depthBiasConstantFactor)
                .depthBiasClamp(depthBiasClamp)
                .depthBiasSlopeFactor(depthBiasSlopeFactor)
                .lineWidth(lineWidth);
    }
}
