package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkImageCopy;

public final class ImageCopy {
    public final Offset3D srcOffset;
    public final Offset3D dstOffset;
    public final Extent3D extent;
    public final ImageSubresourceLayers srcSubresource;
    public final ImageSubresourceLayers dstSubresource;

    public ImageCopy(
            final Offset3D srcOffset,
            final Offset3D dstOffset,
            final Extent3D extent,
            final ImageSubresourceLayers srcSubresource,
            final ImageSubresourceLayers dstSubresource) {

        this.srcOffset = srcOffset;
        this.dstOffset = dstOffset;
        this.extent = extent;
        this.srcSubresource = srcSubresource;
        this.dstSubresource = dstSubresource;
    }

    public ImageCopy() {
        this(new Offset3D(0, 0, 0), new Offset3D(0, 0, 0), new Extent3D(1, 1, 1), null, null);
    }

    public ImageCopy withSrcOffset(final Offset3D srcOffset) {
        return new ImageCopy(srcOffset, dstOffset, extent, srcSubresource, dstSubresource);
    }

    public ImageCopy withDstOffset(final Offset3D dstOffset) {
        return new ImageCopy(srcOffset, dstOffset, extent, srcSubresource, dstSubresource);
    }

    public ImageCopy withExtent(final Extent3D extent) {
        return new ImageCopy(srcOffset, dstOffset, extent, srcSubresource, dstSubresource);
    }

    public ImageCopy withSrcSubresource(final ImageSubresourceLayers srcSubresource) {
        return new ImageCopy(srcOffset, dstOffset, extent, srcSubresource, dstSubresource);
    }

    public ImageCopy withDstSubresource(final ImageSubresourceLayers dstSubresource) {
        return new ImageCopy(srcOffset, dstOffset, extent, srcSubresource, dstSubresource);
    }

    VkImageCopy deserialize(final VkImageCopy vkobj) {
        srcOffset.deserialize(vkobj.srcOffset());
        dstOffset.deserialize(vkobj.dstOffset());
        extent.deserialize(vkobj.extent());
        srcSubresource.deserialize(vkobj.srcSubresource());
        dstSubresource.deserialize(vkobj.dstSubresource());

        return vkobj;
    }
}
