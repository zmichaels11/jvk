package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkFramebufferCreateInfo;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class Framebuffer {
    public static final class CreateInfo {
        public final int width;
        public final int height;
        public final int layers;

        public CreateInfo(final int width, final int height, final int layers) {
            this.width = width;
            this.height = height;
            this.layers = layers;
        }

        public CreateInfo() {
            this(1, 1, 1);
        }

        public CreateInfo withWidth(final int width) {
            return new CreateInfo(width, height, layers);
        }

        public CreateInfo withHeight(final int height) {
            return new CreateInfo(width, height, layers);
        }

        public CreateInfo withLayers(final int layers) {
            return new CreateInfo(width, height, layers);
        }
    }

    public final long handle;
    public final CreateInfo info;
    public final int attachmentCount;
    private final WeakReference<RenderPass> renderPass;
    private final List<WeakReference<ImageView>> attachments;
    public Object userData = null;

    public Framebuffer(final RenderPass renderPass, final CreateInfo info, final ImageView... attachments) {
        this(renderPass, info, List.of(attachments));
    }

    public Framebuffer(final RenderPass renderPass, final CreateInfo info, final List<ImageView> attachments) {
        final var device = renderPass.getDevice();

        this.renderPass = new WeakReference<>(renderPass);
        this.attachmentCount = attachments.size();

        this.attachments = attachments.stream()
                .peek(attachment -> { assert attachment.getDevice() == device; })
                .map(WeakReference::new)
                .collect(Collectors.toList());

        this.info = info;

        try (var mem = MemoryStack.stackPush()) {
            final var pAttachments = attachments.stream()
                    .mapToLong(imageView -> imageView.handle)
                    .boxed()
                    .collect(BufferCollectors.toLongBuffer(mem));

            final var pvkFramebufferCI = VkFramebufferCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO)
                    .renderPass(renderPass.handle)
                    .pAttachments(pAttachments)
                    .width(info.width)
                    .height(info.height)
                    .layers(info.layers);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateFramebuffer(device.handle, pvkFramebufferCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    public ImageView getAttachment(final int index) {
        return Objects.requireNonNull(this.attachments.get(index).get(), "ImageView was lost!");
    }

    public List<ImageView> getAttachments() {
        return attachments.stream()
                .map(WeakReference::get)
                .peek(Objects::requireNonNull)
                .collect(Collectors.toList());
    }

    public Device getDevice() {
        return this.getRenderPass().getDevice();
    }

    public RenderPass getRenderPass() {
        return Objects.requireNonNull(this.renderPass.get(), "RenderPass was lost!");
    }

    public void free() {
        VK10.vkDestroyFramebuffer(this.getDevice().handle, this.handle, null);
    }

    public RenderPassBeginInfo getRenderPassBeginInfo() {
        return new RenderPassBeginInfo(this, this.getRenderPass(), SubpassContents.INLINE, new Rect2D(Offset2D.ZERO, new Extent2D(this.info.width, this.info.height)), List.of());
    }
}
