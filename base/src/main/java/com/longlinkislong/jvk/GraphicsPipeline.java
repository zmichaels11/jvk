package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class GraphicsPipeline implements Pipeline {
    public static final class CreateInfo {
        public final int flags;
        public final List<PipelineShaderStageCreateInfo> stages;
        public final PipelineLayout.CreateInfo layoutInfo;
        public final PipelineColorBlendStateCreateInfo colorBlendState;
        public final PipelineDepthStencilStateCreateInfo depthStencilState;
        public final PipelineInputAssemblyStateCreateInfo inputAssemblyState;
        public final PipelineMultisampleStateCreateInfo multisampleState;
        public final PipelineRasterizationStateCreateInfo rasterizationState;
        public final PipelineTessellationStateCreateInfo tessellationState;
        public final PipelineVertexInputStateCreateInfo vertexInputState;
        public final int subpass;

        public CreateInfo(
                final int flags,
                final List<PipelineShaderStageCreateInfo> stages,
                final PipelineLayout.CreateInfo layoutInfo,
                final PipelineColorBlendStateCreateInfo colorBlendState,
                final PipelineDepthStencilStateCreateInfo depthStencilState,
                final PipelineInputAssemblyStateCreateInfo inputAssemblyState,
                final PipelineMultisampleStateCreateInfo multisampleState,
                final PipelineRasterizationStateCreateInfo rasterizationState,
                final PipelineTessellationStateCreateInfo tessellationState,
                final PipelineVertexInputStateCreateInfo vertexInputState,
                final int subpass) {

            this.flags = flags;
            this.stages = List.copyOf(stages);
            this.layoutInfo = layoutInfo;
            this.colorBlendState = colorBlendState;
            this.depthStencilState = depthStencilState;
            this.inputAssemblyState = inputAssemblyState;
            this.multisampleState = multisampleState;
            this.rasterizationState = rasterizationState;
            this.tessellationState = tessellationState;
            this.vertexInputState = vertexInputState;
            this.subpass = subpass;
        }

        public CreateInfo() {
            this(0, List.of(), null, null, null, null, null, null, null, null, -1);
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withStages(final List<PipelineShaderStageCreateInfo> stages) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withStages(final PipelineShaderStageCreateInfo... stages) {
            return withStages(List.of(stages));
        }

        public CreateInfo withLayout(final PipelineLayout.CreateInfo layoutInfo) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withColorBlendState(final PipelineColorBlendStateCreateInfo colorBlendState) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withDepthStencilState(final PipelineDepthStencilStateCreateInfo depthStencilState) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withInputAssemblyState(final PipelineInputAssemblyStateCreateInfo inputAssemblyState) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withMultisampleState(final PipelineMultisampleStateCreateInfo multisampleState) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withRasterizationState(final PipelineRasterizationStateCreateInfo rasterizationState) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withTessellationState(final PipelineTessellationStateCreateInfo tessellationState) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withVertexInputState(final PipelineVertexInputStateCreateInfo vertexInputState) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }

        public CreateInfo withSubpass(final int subpass) {
            return new CreateInfo(flags, stages, layoutInfo, colorBlendState, depthStencilState, inputAssemblyState, multisampleState, rasterizationState, tessellationState, vertexInputState, subpass);
        }
    }

    public final CreateInfo info;
    public final long handle;
    private final WeakReference<PipelineCache> cache;
    private final WeakReference<PipelineLayout> layout;

    GraphicsPipeline(final PipelineCache cache, final CreateInfo createInfo, final RenderPass renderPass) {
        this.info = createInfo;
        this.cache = new WeakReference<>(cache);

        final var device = cache.getDevice();
        final var layout = device.pipelineLayoutCache.allocatePipelineLayout(createInfo.layoutInfo);

        this.layout = new WeakReference<>(layout);

        try (var mem = MemoryStack.stackPush()) {
            final var pStages = VkPipelineShaderStageCreateInfo.callocStack(createInfo.stages.size(), mem);

            IntStream.range(0, createInfo.stages.size()).forEach(i -> createInfo.stages.get(i).deserialize(pStages.get(i), device, mem));

            final VkPipelineColorBlendStateCreateInfo pColorBlendState;

            if (null != createInfo.colorBlendState) {
                pColorBlendState = createInfo.colorBlendState.deserialize(VkPipelineColorBlendStateCreateInfo.callocStack(mem), mem);
            } else {
                pColorBlendState = null;
            }

            final VkPipelineDepthStencilStateCreateInfo pDepthStencilState;

            if (null != createInfo.depthStencilState) {
                pDepthStencilState = createInfo.depthStencilState.deserialize(VkPipelineDepthStencilStateCreateInfo.callocStack(mem));
            } else {
                pDepthStencilState = null;
            }

            final var pDynamicState = VkPipelineDynamicStateCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO)
                    .pDynamicStates(mem.ints(VK10.VK_DYNAMIC_STATE_SCISSOR, VK10.VK_DYNAMIC_STATE_VIEWPORT));

            final var pInputAssemblyState = createInfo.inputAssemblyState.deserialize(VkPipelineInputAssemblyStateCreateInfo.callocStack(mem));

            final VkPipelineMultisampleStateCreateInfo pMultisampleState;

            if (null != createInfo.multisampleState) {
                pMultisampleState = createInfo.multisampleState.deserialize(VkPipelineMultisampleStateCreateInfo.callocStack(mem));
            } else {
                pMultisampleState = null;
            }

            final VkPipelineRasterizationStateCreateInfo pRasterizationState;

            if (null != createInfo.rasterizationState) {
                pRasterizationState = createInfo.rasterizationState.deserialize(VkPipelineRasterizationStateCreateInfo.callocStack(mem));
            } else {
                pRasterizationState = null;
            }

            final VkPipelineTessellationStateCreateInfo pTessellationState;

            if (null != createInfo.tessellationState) {
                pTessellationState = createInfo.tessellationState.deserialize(VkPipelineTessellationStateCreateInfo.callocStack(mem));
            } else {
                pTessellationState = null;
            }

            final VkPipelineVertexInputStateCreateInfo pVertexInputState;

            if (null != createInfo.vertexInputState) {
                pVertexInputState = createInfo.vertexInputState.deserialize(VkPipelineVertexInputStateCreateInfo.callocStack(mem), mem);
            } else {
                pVertexInputState = null;
            }

            final var pViewportState = VkPipelineViewportStateCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO)
                    .viewportCount(1)
                    .scissorCount(1);

            final var pvkGraphicsPipelineCI = VkGraphicsPipelineCreateInfo.callocStack(1, mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO)
                    .layout(layout.handle)
                    .pColorBlendState(pColorBlendState)
                    .pDepthStencilState(pDepthStencilState)
                    .pDynamicState(pDynamicState)
                    .pInputAssemblyState(pInputAssemblyState)
                    .pMultisampleState(pMultisampleState)
                    .pRasterizationState(pRasterizationState)
                    .pStages(pStages)
                    .pTessellationState(pTessellationState)
                    .pVertexInputState(pVertexInputState)
                    .pViewportState(pViewportState)
                    .subpass(createInfo.subpass)
                    .renderPass(renderPass.handle);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateGraphicsPipelines(device.handle, cache.handle, pvkGraphicsPipelineCI, null, pHandle));

            this.handle = pHandle.get();
        }
    }

    void free() {
        VK10.vkDestroyPipeline(this.getDevice().handle, this.handle, null);
    }

    @Override
    public long getHandle() {
        return this.handle;
    }

    @Override
    public PipelineBindpoint getBindpoint() {
        return PipelineBindpoint.GRAPHICS;
    }

    public void release() {
        this.getPipelineCache().release(this);
    }

    public Device getDevice() {
        return this.getPipelineCache().getDevice();
    }

    public PipelineLayout getPipelineLayout() {
        return Objects.requireNonNull(this.layout.get(), "PipelineLayout was lost!");
    }

    public PipelineCache getPipelineCache() {
        return Objects.requireNonNull(this.cache.get(), "PipelineCache was lost!");
    }
}
