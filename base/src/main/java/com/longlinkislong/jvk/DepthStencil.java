package com.longlinkislong.jvk;

public final class DepthStencil {
    public final float depth;
    public final int stencil;

    public DepthStencil(final float depth, final int stencil) {
        this.depth = depth;
        this.stencil = stencil;
    }
}
