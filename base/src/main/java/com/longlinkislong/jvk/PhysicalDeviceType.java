package com.longlinkislong.jvk;

import static org.lwjgl.vulkan.VK11.*;

public enum PhysicalDeviceType {
    OTHER(VK_PHYSICAL_DEVICE_TYPE_OTHER),
    INTEGRATED_GPU(VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU),
    DISCRETE_GPU(VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU),
    VIRTUAL_GPU(VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU),
    CPU(VK_PHYSICAL_DEVICE_TYPE_CPU);

    final int value;

    PhysicalDeviceType(final int value) {
        this.value = value;
    }
}
