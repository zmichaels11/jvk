package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkDescriptorPoolSize;

import java.util.List;
import java.util.stream.IntStream;

public final class DescriptorPoolSize {
    public final int type;
    public final int descriptorCount;

    public DescriptorPoolSize(final int type, final int descriptorCount) {
        this.type = type;
        this.descriptorCount = descriptorCount;
    }

    VkDescriptorPoolSize deserialize(final VkDescriptorPoolSize vkobj) {
        return vkobj.set(type, descriptorCount);
    }

    static VkDescriptorPoolSize.Buffer deserialize(final List<DescriptorPoolSize> list, final MemoryStack mem) {
        final var out = VkDescriptorPoolSize.callocStack(list.size(), mem);

        IntStream.range(0, list.size()).forEach(id -> list.get(id).deserialize(out.get(id)));

        return out;
    }
}
