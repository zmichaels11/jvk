package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkBufferMemoryBarrier;

import java.util.Set;

public final class BufferMemoryBarrier {
    public final Set<AccessFlag> srcAccessMask;
    public final Set<AccessFlag> dstAccessMask;
    public final QueueFamily srcQueueFamily;
    public final QueueFamily dstQueueFamily;
    public final Buffer buffer;
    public final long offset;
    public final long size;

    public BufferMemoryBarrier(
            final Set<AccessFlag> srcAccessMask, final Set<AccessFlag> dstAccessMask,
            final QueueFamily srcQueueFamily, final QueueFamily dstQueueFamily,
            final Buffer buffer, final long offset, final long size) {

        this.srcAccessMask = Set.copyOf(srcAccessMask);
        this.dstAccessMask = Set.copyOf(dstAccessMask);
        this.srcQueueFamily = srcQueueFamily;
        this.dstQueueFamily = dstQueueFamily;
        this.buffer = buffer;
        this.offset = offset;
        this.size = size;
    }

    public BufferMemoryBarrier() {
        this(Set.of(), Set.of(), null, null, null, 0L, VK10.VK_WHOLE_SIZE);
    }

    public BufferMemoryBarrier withSrcAccess(final Set<AccessFlag> srcAccessMask) {
        return new BufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }

    public BufferMemoryBarrier withSrcAccess(final AccessFlag... srcAccess) {
        return withSrcAccess(Set.of(srcAccess));
    }

    public BufferMemoryBarrier withDstAccess(final Set<AccessFlag> dstAccessMask) {
        return new BufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }

    public BufferMemoryBarrier withDstAccess(final AccessFlag... dstAccess) {
        return withDstAccess(Set.of(dstAccess));
    }

    public BufferMemoryBarrier withSrcQueueFamily(final QueueFamily srcQueueFamily) {
        return new BufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }

    public BufferMemoryBarrier withDstQueueFamily(final QueueFamily dstQueueFamily) {
        return new BufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }

    public BufferMemoryBarrier withQueueFamilyTransfer(final QueueFamily srcQueueFamily, final QueueFamily dstQueueFamily) {
        return new BufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }

    public BufferMemoryBarrier withBuffer(final Buffer buffer) {
        return new BufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }

    public BufferMemoryBarrier withOffset(final long offset) {
        return new BufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }

    public BufferMemoryBarrier withSize(final long size) {
        return new BufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }

    VkBufferMemoryBarrier deserialize(final VkBufferMemoryBarrier vkobj) {
        return vkobj.set(
                VK10.VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER, 0L,
                AccessFlag.toBitfield(srcAccessMask), AccessFlag.toBitfield(dstAccessMask),
                QueueFamily.indexOf(srcQueueFamily), QueueFamily.indexOf(dstQueueFamily),
                buffer.handle, offset, size);
    }
}
