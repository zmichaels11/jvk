package com.longlinkislong.jvk;

import com.longlinkislong.jvk.memory.MemoryBlock;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkBufferCreateInfo;
import org.lwjgl.vulkan.VkMemoryRequirements;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class Buffer {
    public static final class CreateInfo {
        public final int flags;
        public final long size;
        public final Set<BufferUsageFlag> usage;
        public final SharingMode sharingMode;
        public final List<QueueFamily> queueFamilies;

        public CreateInfo(final int flags, final long size, final Set<BufferUsageFlag> usage, final SharingMode sharingMode, final List<QueueFamily> queueFamilies) {
            this.flags = flags;
            this.size = size;
            this.usage = Set.copyOf(usage);
            this.sharingMode = sharingMode;
            this.queueFamilies = List.copyOf(queueFamilies);
        }

        public CreateInfo() {
            this(0, 0L, Set.of(), SharingMode.EXCLUSIVE, List.of());
        }

        public CreateInfo withSize(final long size) {
            return new CreateInfo(flags, size, usage, sharingMode, queueFamilies);
        }

        public CreateInfo withFlags(final int flags) {
            return new CreateInfo(flags, size, usage, sharingMode, queueFamilies);
        }

        public CreateInfo withUsage(final Set<BufferUsageFlag> usage) {
            return new CreateInfo(flags, size, usage, sharingMode, queueFamilies);
        }

        public CreateInfo withUsage(final BufferUsageFlag... usage) {
            return withUsage(Set.of(usage));
        }

        public CreateInfo withSharingMode(final SharingMode sharingMode) {
            return new CreateInfo(flags, size, usage, sharingMode, queueFamilies);
        }

        public CreateInfo withQueueFamilies(final List<QueueFamily> queueFamilies) {
            return new CreateInfo(flags, size, usage, sharingMode, queueFamilies);
        }

        public CreateInfo withQueueFamilies(final QueueFamily... queueFamilies) {
            return withQueueFamilies(List.of(queueFamilies));
        }
    }

    private final WeakReference<Device> device;
    public final long handle;
    public final CreateInfo info;
    public final MemoryBlock memory;

    Buffer(final Device device, final CreateInfo info, final int memoryProperties) {
        this.info = info;
        this.device = new WeakReference<>(device);

        try (var mem = MemoryStack.stackPush()) {
            final var pQueueFamilyIndices = info.queueFamilies.stream()
                    .map(qf -> qf.index)
                    .collect(BufferCollectors.toIntBuffer(mem));

            final var pvkBufferCI = VkBufferCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO)
                    .flags(info.flags)
                    .size(info.size)
                    .usage(BufferUsageFlag.toBitfield(info.usage))
                    .sharingMode(info.sharingMode.value)
                    .pQueueFamilyIndices(pQueueFamilyIndices);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateBuffer(device.handle, pvkBufferCI, null, pHandle));

            this.handle = pHandle.get();

            final var pvkMemoryRequirements = VkMemoryRequirements.callocStack(mem);

            VK10.vkGetBufferMemoryRequirements(device.handle, this.handle, pvkMemoryRequirements);

            this.memory = device.memoryManager.allocateBufferMemory(pvkMemoryRequirements, memoryProperties);
            this.memory.bindToBuffer(this.handle);
        }
    }

    public ByteBuffer map() {
        return this.memory.map();
    }

    public void unmap() {
        this.memory.unmap();
    }

    public void withMap(final Consumer<ByteBuffer> onMap) {
        onMap.accept(this.map());
        this.unmap();
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public void free() {
        VK10.vkDestroyBuffer(this.getDevice().handle, this.handle, null);
        this.memory.free();
    }
}
