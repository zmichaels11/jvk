package com.longlinkislong.jvk;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class SamplerCache {
    private static final class SamplerInstance {
        private final Sampler instance;
        private int references;

        private SamplerInstance(final Sampler sampler) {
            this.instance = sampler;
        }
    }

    private final Map<Sampler.CreateInfo, SamplerInstance> samplers = new HashMap<>();
    private final Object lock = new Object();
    private final WeakReference<Device> device;

    SamplerCache(final Device device) {
        this.device = new WeakReference<>(device);
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public Sampler allocate(final Sampler.CreateInfo createInfo) {
        synchronized (this.lock) {
            final var sampler = this.samplers.computeIfAbsent(createInfo, info -> new SamplerInstance(new Sampler(this, info)));

            sampler.references += 1;

            return sampler.instance;
        }
    }

    public void release(final Sampler sampler) {
        assert sampler.getSamplerCache() == this;

        synchronized (this.lock) {
            final var s = this.samplers.get(sampler.info);

            s.references -= 1;

            if (0 == s.references) {
                s.instance.free();
                this.samplers.remove(sampler.info);
            }
        }
    }

    void free() {
        synchronized (this.lock) {
            this.samplers.values().forEach(sampler -> sampler.instance.free());
            this.samplers.clear();
        }
    }
}
