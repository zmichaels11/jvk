package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineCacheCreateInfo;

import java.lang.ref.WeakReference;
import java.util.Objects;

public final class PipelineCache {
    public final long handle;
    private final WeakReference<Device> device;

    PipelineCache(final Device device) {
        this.device = new WeakReference<>(device);

        try (var mem = MemoryStack.stackPush()) {
            final var pvkPipelineCacheCI = VkPipelineCacheCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO);

            final var pHandle = mem.callocLong(1);
            final var result = VK10.vkCreatePipelineCache(device.handle, pvkPipelineCacheCI, null, pHandle);

            switch (result) {
                case VK10.VK_SUCCESS:
                    this.handle = pHandle.get();
                    break;
                default:
                    if (VK10.VK_NULL_HANDLE != pHandle.get(0)) {
                        VK10.vkDestroyPipelineCache(device.handle, pHandle.get(0), null);
                    }
                    this.handle = VK10.VK_NULL_HANDLE;
                    break;
            }
        }
    }

    public ComputePipeline allocate(final ComputePipeline.CreateInfo createInfo) {
        return new ComputePipeline(this, createInfo);
    }

    public GraphicsPipeline allocate(final GraphicsPipeline.CreateInfo createInfo, final RenderPass renderPass) {
        return new GraphicsPipeline(this, createInfo, renderPass);
    }

    public void release(final ComputePipeline pipeline) {
        pipeline.free();
    }

    public void release(final GraphicsPipeline pipeline) { pipeline.free(); }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    public void free() {
        if (this.handle != VK10.VK_NULL_HANDLE) {
            VK10.vkDestroyPipelineCache(this.getDevice().handle, this.handle, null);
        }
    }
}
