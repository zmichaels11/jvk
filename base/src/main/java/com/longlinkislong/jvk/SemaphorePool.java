package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkSemaphoreCreateInfo;

import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class SemaphorePool {
    private final WeakReference<Device> device;
    private final Object lock = new Object();
    private final Set<Semaphore> allSemaphores = new HashSet<>();
    private final java.util.Queue<Semaphore> availableSemaphores = new ArrayDeque<>();

    public SemaphorePool(final Device device) {
        this.device = new WeakReference<>(device);
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    private Semaphore allocateSemaphore() {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkSemaphoreCI = VkSemaphoreCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateSemaphore(this.getDevice().handle, pvkSemaphoreCI, null, pHandle));

            final var semaphore = new Semaphore(this, pHandle.get());

            this.allSemaphores.add(semaphore);

            return semaphore;
        }
    }

    public Semaphore acquireSemaphore() {
        synchronized (lock) {
            if (this.availableSemaphores.isEmpty()) {
                return allocateSemaphore();
            } else {
                return this.availableSemaphores.poll();
            }
        }
    }

    public void releaseSemaphore(final Semaphore semaphore) {
        synchronized (lock) {
            assert this.allSemaphores.contains(semaphore);

            this.availableSemaphores.offer(semaphore);
        }
    }

    public void free() {
        final var device = this.getDevice();

        synchronized (lock) {
            this.allSemaphores.forEach(semaphore -> VK10.vkDestroySemaphore(device.handle, semaphore.handle, null));
            this.allSemaphores.clear();
            this.availableSemaphores.clear();
        }
    }
}
