package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum SamplerAddressMode {
    CLAMP_TO_EDGE(VK10.VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE),
    REPEAT(VK10.VK_SAMPLER_ADDRESS_MODE_REPEAT);

    final int value;

    SamplerAddressMode(final int value) {
        this.value = value;
    }
}
