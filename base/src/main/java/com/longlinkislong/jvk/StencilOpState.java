package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkStencilOpState;

public final class StencilOpState {
    public final StencilOp failOp;
    public final StencilOp passOp;
    public final StencilOp depthFailOp;
    public final CompareOp compareOp;
    public final int compareMask;
    public final int writeMask;
    public final int reference;

    public StencilOpState(
            final StencilOp failOp, final StencilOp passOp, final StencilOp depthFailOp,
            final CompareOp compareOp,
            final int compareMask, final int writeMask, final int reference) {

        this.failOp = failOp;
        this.passOp = passOp;
        this.depthFailOp = depthFailOp;
        this.compareOp = compareOp;
        this.compareMask = compareMask;
        this.writeMask = writeMask;
        this.reference = reference;
    }

    public StencilOpState() {
        this(StencilOp.REPLACE, StencilOp.REPLACE, StencilOp.REPLACE, CompareOp.ALWAYS, ~0, ~0, ~0);
    }

    public StencilOpState withFailOp(final StencilOp failOp) {
        return new StencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public StencilOpState withPassOp(final StencilOp passOp) {
        return new StencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public StencilOpState withDepthFailOp(final StencilOp depthFailOp) {
        return new StencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public StencilOpState withCompareOp(final CompareOp compareOp) {
        return new StencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public StencilOpState withCompareMask(final int compareMask) {
        return new StencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public StencilOpState withWriteMask(final int writeMask) {
        return new StencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public StencilOpState withReference(final int reference) {
        return new StencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    VkStencilOpState deserialize(final VkStencilOpState vkobj) {
        return vkobj.set(failOp.value, passOp.value, depthFailOp.value, compareOp.value, compareMask, writeMask, reference);
    }
}
