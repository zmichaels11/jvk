package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkImageSubresourceLayers;

import java.util.Set;

public final class ImageSubresourceLayers {
    public final Set<AspectFlag> aspectMask;
    public final int mipLevel;
    public final int baseArrayLayer;
    public final int layerCount;

    public ImageSubresourceLayers(
            final Set<AspectFlag> aspectMask,
            final int mipLevel,
            final int baseArrayLayer, final int layerCount) {

        this.aspectMask = Set.copyOf(aspectMask);
        this.mipLevel = mipLevel;
        this.baseArrayLayer = baseArrayLayer;
        this.layerCount = layerCount;
    }

    public ImageSubresourceLayers() {
        this(Set.of(), 0, 0, 1);
    }

    public ImageSubresourceLayers withAspectMask(final Set<AspectFlag> aspectMask) {
        return new ImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }

    public ImageSubresourceLayers withAspectMask(final AspectFlag... aspectMask) {
        return withAspectMask(Set.of(aspectMask));
    }

    public ImageSubresourceLayers withMipLevel(final int mipLevel) {
        return new ImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }

    public ImageSubresourceLayers withBaseArrayLayer(final int baseArrayLayer) {
        return new ImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }

    public ImageSubresourceLayers withLayerCount(final int layerCount) {
        return new ImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }

    public ImageSubresourceLayers withArrayLayerRange(final int baseArrayLayer, final int layerCount) {
        return new ImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }

    VkImageSubresourceLayers deserialize(final VkImageSubresourceLayers vkobj) {
        return vkobj.set(AspectFlag.toBitfield(aspectMask), mipLevel, baseArrayLayer, layerCount);
    }
}
