package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum ImageType {
    IMAGE_1D(VK10.VK_IMAGE_TYPE_1D),
    IMAGE_2D(VK10.VK_IMAGE_TYPE_2D),
    IMAGE_3D(VK10.VK_IMAGE_TYPE_3D);

    final int value;

    ImageType(final int value) {
        this.value = value;
    }
}
