package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum CullMode {
    FRONT(VK10.VK_CULL_MODE_FRONT_BIT),
    BACK(VK10.VK_CULL_MODE_BACK_BIT),
    FRONT_AND_BACK(VK10.VK_CULL_MODE_FRONT_AND_BACK),
    NONE(VK10.VK_CULL_MODE_NONE);

    final int value;

    CullMode(final int value) {
        this.value = value;
    }
}
