package com.longlinkislong.jvk;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.*;
import java.util.stream.Collector;

abstract class IntBufferCollector implements Collector<Integer, List<Integer>, IntBuffer> {
    @Override
    public Supplier<List<Integer>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<Integer>, Integer> accumulator() {
        return List::add;
    }

    @Override
    public BinaryOperator<List<Integer>> combiner() {
        return (left, right) -> {
            left.addAll(right);

            return left;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of(Characteristics.UNORDERED);
    }

    static IntBufferCollector allocate(final IntFunction<IntBuffer> constructor) {
        return new IntBufferCollector() {
            @Override
            public Function<List<Integer>, IntBuffer> finisher() {
                return list -> {
                    final var out = constructor.apply(list.size());

                    list.forEach(out::put);
                    out.flip();

                    return out;
                };
            }
        };
    }
}
