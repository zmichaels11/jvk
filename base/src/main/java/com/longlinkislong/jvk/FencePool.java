package com.longlinkislong.jvk;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkFenceCreateInfo;

import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.longlinkislong.jvk.Util.vkAssert;

public final class FencePool {
    private final WeakReference<Device> device;
    private final Object lock = new Object();
    private final Set<Fence> allFences = new HashSet<>();
    private final java.util.Queue<Fence> availableFences = new ArrayDeque<>();

    FencePool(final Device device) {
        this.device = new WeakReference<>(device);
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }

    private Fence allocateFence() {
        try (var mem = MemoryStack.stackPush()) {
            final var pvkFenceCI = VkFenceCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_FENCE_CREATE_INFO);

            final var pHandle = mem.callocLong(1);

            vkAssert(VK10.vkCreateFence(this.getDevice().handle, pvkFenceCI, null, pHandle));

            final var fence = new Fence(this, pHandle.get());

            this.allFences.add(fence);

            return fence;
        }
    }

    public Fence acquireFence() {
        synchronized (lock) {
            if (this.availableFences.isEmpty()) {
                return this.allocateFence();
            } else {
                return this.availableFences.poll();
            }
        }
    }

    public void releaseFence(final Fence fence) {
        synchronized (lock) {
            assert this.allFences.contains(fence);

            this.availableFences.offer(fence);
        }
    }

    public void free() {
        final var device = this.getDevice();

        synchronized (lock) {
            this.allFences.forEach(fence -> VK10.vkDestroyFence(device.handle, fence.handle, null));
            this.allFences.clear();
            this.availableFences.clear();
        }
    }
}
