package com.longlinkislong.jvk;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.*;
import java.util.stream.Collector;

abstract class FloatBufferCollector implements Collector<Float, List<Float>, FloatBuffer> {
    @Override
    public Supplier<List<Float>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<Float>, Float> accumulator() {
        return List::add;
    }

    @Override
    public BinaryOperator<List<Float>> combiner() {
        return (left, right) -> {
            left.addAll(right);

            return left;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of(Characteristics.UNORDERED);
    }

    static FloatBufferCollector allocate(final IntFunction<FloatBuffer> constructor) {
        return new FloatBufferCollector() {
            @Override
            public Function<List<Float>, FloatBuffer> finisher() {
                return list -> {
                    final var out = constructor.apply(list.size());

                    list.forEach(out::put);
                    out.flip();

                    return out;
                };
            }
        };
    }
}
