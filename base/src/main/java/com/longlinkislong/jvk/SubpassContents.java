package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum SubpassContents {
    INLINE(VK10.VK_SUBPASS_CONTENTS_INLINE),
    SECONDARY_COMMAND_BUFFERS(VK10.VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

    final int value;

    SubpassContents(final int value) {
        this.value = value;
    }
}
