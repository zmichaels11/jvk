package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum SharingMode {
    EXCLUSIVE(VK10.VK_SHARING_MODE_EXCLUSIVE),
    CONCURRENT(VK10.VK_SHARING_MODE_CONCURRENT);

    final int value;

    SharingMode(final int value) {
        this.value = value;
    }
}
