package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

import java.util.Set;

public enum BufferUsageFlag {
    TRANSFER_SRC(VK10.VK_BUFFER_USAGE_TRANSFER_SRC_BIT),
    TRANSFER_DST(VK10.VK_BUFFER_USAGE_TRANSFER_DST_BIT),
    UNIFORM_TEXEL_BUFFER(VK10.VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT),
    STORAGE_TEXEL_BUFFER(VK10.VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT),
    UNIFORM_BUFFER(VK10.VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT),
    STORAGE_BUFFER(VK10.VK_BUFFER_USAGE_STORAGE_BUFFER_BIT),
    INDEX_BUFFER(VK10.VK_BUFFER_USAGE_INDEX_BUFFER_BIT),
    VERTEX_BUFFER(VK10.VK_BUFFER_USAGE_VERTEX_BUFFER_BIT),
    INDIRECT_BUFFER(VK10.VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT);

    public static final Set<BufferUsageFlag> ANY_USAGE = Set.of(BufferUsageFlag.values());
    public static final Set<BufferUsageFlag> GRAPHICS_USAGE = ANY_USAGE;
    public static final Set<BufferUsageFlag> COMPUTE_USAGE = Set.of(TRANSFER_DST, TRANSFER_SRC, UNIFORM_TEXEL_BUFFER, STORAGE_TEXEL_BUFFER, UNIFORM_BUFFER, STORAGE_BUFFER, INDIRECT_BUFFER);
    public static final Set<BufferUsageFlag> TRANSFER_USAGE = Set.of(TRANSFER_SRC, TRANSFER_DST);

    final int value;

    BufferUsageFlag(final int value) {
        this.value = value;
    }

    static int toBitfield(final Set<BufferUsageFlag> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .sum();
    }
}
