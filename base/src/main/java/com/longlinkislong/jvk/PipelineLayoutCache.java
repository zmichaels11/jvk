package com.longlinkislong.jvk;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class PipelineLayoutCache {
    private final class Layout {
        private final PipelineLayout instance;
        private int references;

        private Layout(final PipelineLayout instance) {
            this.instance = instance;
        }
    }

    private final WeakReference<Device> device;
    private final Object lock = new Object();
    private final Map<PipelineLayout.CreateInfo, Layout> layouts = new HashMap<>();

    PipelineLayoutCache(final Device device) {
        this.device = new WeakReference<>(device);
    }

    public void free() {
        synchronized (lock) {
            this.layouts.values().forEach(layout -> layout.instance.free());
            this.layouts.clear();
        }
    }

    public PipelineLayout allocatePipelineLayout(final PipelineLayout.CreateInfo createInfo) {
        synchronized (lock) {
            final var layout = this.layouts.computeIfAbsent(createInfo, info -> new Layout(new PipelineLayout(this, createInfo)));

            layout.references += 1;

            return layout.instance;
        }
    }

    public void releasePipelineLayout(final PipelineLayout layout) {
        assert layout.getPipelineLayoutCache() == this;

        synchronized (lock) {
            final var pl = this.layouts.get(layout.info);

            pl.references -= 1;

            if (0 == pl.references) {
                layout.free();
                this.layouts.remove(layout.info);
            }
        }
    }

    public Device getDevice() {
        return Objects.requireNonNull(this.device.get(), "Device was lost!");
    }
}
