package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VkRect2D;

public final class Rect2D {
    public final Offset2D offset;
    public final Extent2D extent;

    public Rect2D(final Offset2D offset, final Extent2D extent) {
        this.extent = extent;
        this.offset = offset;
    }

    VkRect2D deserialize(final VkRect2D vkobj) {
        this.offset.deserialize(vkobj.offset());
        this.extent.deserialize(vkobj.extent());

        return vkobj;
    }
}
