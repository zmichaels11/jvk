package com.longlinkislong.jvk;

import org.lwjgl.vulkan.VK10;

public enum CommandBufferLevel {
    PRIMARY(VK10.VK_COMMAND_BUFFER_LEVEL_PRIMARY),
    SECONDARY(VK10.VK_COMMAND_BUFFER_LEVEL_SECONDARY);

    final int value;

    CommandBufferLevel(final int value) {
        this.value = value;
    }
}
